<?php

require_once 'AdminController/Campaign.php';

if (isset($_POST['campin_id'])) {
    $data = [
        'campin_id' => $_POST['campin_id'],
        'service_id' => $_POST['service_id'],
        'extra_id' => $_POST['extra_id'],
        'price' => $_POST['price'],
        'qty_service' => $_POST['qty_service'],
    ];

    $camnServive = new Campaign();
    $result = $camnServive->createChildCampin( $data );
    if ( $result['tag'] == 503){
        echo json_encode(array('error' => 'something went wrong' ));
    }
    if ( $result['tag'] == 405){
        echo json_encode(array('error' => 'maximum services added' ));
    }
    else{
        echo json_encode(array('success' => 'Services added' ));
    }

}