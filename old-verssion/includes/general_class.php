<?php

class general {
    //alert properties

    
    //alert methods
    function alert ($message, $type){
    //types: success, info, warning, danger
    echo '<div class="alert alert-'
            . $type
            . ' alert-dismissible" role="alert">'
            . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            . '<span aria-hidden="true">&times;</span></button>'
            . $message
            . '</div>';
    }
    
    //Method to get users names for options
    function usersRecords(){
    $sql = mysql_query("SELECT * FROM `users`");
        while($data = mysql_fetch_array($sql)){
            echo '<option value="';
            echo $data['id'] . '"';
            echo '>' . $data['username'] . '</option>';
        }
    }
    
    //Method to get records for options
    function servicesRecords(){
        $sql = mysql_query("SELECT * FROM `services`");
        while($data = mysql_fetch_array($sql)){
            echo '<option value="';
            echo $data['id'] . '"';
            echo '>' . $data['description'] . '</option>';
        }
    }
    
    //Method to get order status for options
    function orderStatus(){
        $sql = mysql_query("SELECT * FROM `status`");
        while($data = mysql_fetch_array($sql)){
            echo '<option value="';
            echo $data['id'] . '"';
            echo '>' . $data['type'] . '</option>';
        }
    }
    
    //Method for tooltip
    function toolTip($code,$desc){
        echo '<a class="tip" data-toggle="tooltip" data-placement="top" title="' . $desc . '">' . $code . '</a>';
    }
    function toolTipClass($code,$desc,$class){
        echo '<span class="tip '. $class .'" data-toggle="tooltip" data-placement="top" title="' . $desc . '">' . $code . '</span>';
    }
    function toolTipRight($code,$desc){
        echo '<a class="tip" data-toggle="tooltip" data-placement="right" title="' . $desc . '">' . $code . '</a>';
    }
    
}
    

