<?php
require 'includes/header.php';
?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

<!-- ChartJS 1.0.1 -->

<div class="container-fluid">
  <div class="row margin-top-30">
		<div class="col-md-12">
<?php
$sql = mysql_query("SELECT
	(SELECT COUNT(*) FROM `orders`WHERE uid= $uid) AS `total-orders`,
	(SELECT SUM(`price`) FROM `orders` WHERE uid= $uid) AS `orders-income`,
	(SELECT SUM(`amount`) FROM `payment` WHERE uid= $uid)  AS `total-funds`");
$info = mysql_fetch_array($sql)
?>

      <div class="row">

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>Funds</h3>
              <p>Add Fund to your account balance</p>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
            <a href="funds-add.php" class="small-box-footer">Add Fund <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div><!-- ./col -->

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>Order</h3>
              <p>Make backlinks orders</p>
            </div>
            <div class="icon">
              <i class="fa fa-shopping-cart"></i>
            </div>
            <a href="funds-add.php" class="small-box-footer">Make Order <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div><!-- ./col -->

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Reports</h3>
              <p>Check order status and reports</p>
            </div>
            <div class="icon">
              <i class="fa fa-bars"></i>
            </div>
            <a href="reports.php" class="small-box-footer">Check Reports <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div><!-- ./col -->
      </div><!-- /.row -->
    </div>
  </div>
<?php
    $query = mysql_query("SELECT
        `o`.`id`,
        `o`.`date`,
        `o`.`service_id`,
        `o`.`extras` as `extras`,
        `o`.`quantity` as `qty`,
        `o`.`price`
        FROM `orders` as `o`
        LEFT JOIN `service` as `s` ON `o`.`service_id` = `s`.`id`
        WHERE `o`.`uid` = '$uid'
        ORDER BY `id` DESC
        limit 5
      ");
?>
  <div class="row">
    <div class="col-md-6 ">
      <div class="box box-primary">
        <div class="panel-body">
          <i class="fa fa-bars"></i> <strong>Last orders</strong>
          <hr>
          <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th>ID</th>
                <th>Created <a data-toggle="tooltip" data-placement="top" title="GMT Date"><sup class="fa fa-info-circle text-gray"></sup></a></th>
                <th>Service</th>
                <th>Qty.</th>
                <th>Charge</th>
              </tr>
            </thead>
            <tbody>
<?php 
while ($data = mysql_fetch_array($query)) {
  $id = $data['id'];
  $price = $data['price']/100;
  $quantity = $data['qty'];
  $service_id = $data['service_id'];
  $date = $data['date'];
  $extrasID = $data['extras'];
  $extrasArr = "";
if (!empty($extrasID)){
    $extrasArr=explode(",",$extrasID);
    foreach ($extrasArr as $key => $value){
        if (strpos($value, ':') !== FALSE){
            $valueArr = explode (":",$value);
            $extrasArr[$key]=$valueArr [1];
            $extraQ[$record['id']][$key] = $valueArr [0];
        }
    }
}
  $service = mysql_query("SELECT `code` FROM `service` WHERE `id` = '".$service_id."'");
  $ser = mysql_fetch_array($service);
  $serDesc = $ser['code'];
  ?>
              <tr>
                <td>
                    #<span class="oid"><?php echo $id; ?></span>
                </td>
                <td>
<?php
    if ($date == '0000-00-00 00:00:00') {
      $general ->toolTipClass('N/A', 'Date is note defined!', 'label label-gray');
    }else {
      $recordDate = strtotime($date);
      if (gmdate("Ymd") == date("Ymd", $recordDate)){
          $time =  date("h:i A", $recordDate);
          $general ->toolTipClass($time , "Today on " . $time, 'label label-gray');
      }
      else {
          $date = date("Y M", $recordDate) . " " . intval(date("d", $recordDate));
          $fullDate = date("Y/m/d h:i A", $recordDate);
          $general ->toolTipClass($date, $fullDate, 'label label-gray');
      }
    }
?>
                </td>
                <td>
                <?php
                    $general ->toolTipClass($serDesc, "Service: ".$serDesc, 'label bg-blue big');
                    if (!empty($extrasArr)) {
                        echo '<br /><span class="text-yellow">&#x21B3;</span>';
                        foreach($extrasArr as $key => $extraID){
                        $extra = mysql_fetch_assoc(mysql_query("SELECT * FROM `extras` WHERE id='$extraID' "));
                            if (!empty($extra)){
                                echo " ";
                                $extraCode = $extra["code"];
                                $general ->toolTipClass($extraCode, "Extra: ".$extra["description"], 'label label-warning big');
                            }
                        }
                    }
                    ?>
                </td>
                <td><b><?php echo $quantity ;?></b></td>
                <td><b>$<?php echo $price;?></b></td>
              </tr>
<?php } ?>
            </tbody>
          </table>
          <hr>
          <div class="text-center">
            <a class="btn btn-sm btn-default flat" href="report.php">View full order report</a>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-body">
          <i class="fa fa-credit-card"></i> <strong>Last Paymets</strong>
          <hr>
          <table class="table table-striped table-hover" >
            <thead>
              <tr>
                <th><?php $general ->toolTip('Date', 'GMT')?></th>
                <th>Method</th>
                <th>Transaction ID</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
<?php 
$query = mysql_query("SELECT `p`.`id`, `p`.`uid`, `m`.`name` as `method`, `p`.`amount`, `p`.`date` as `date`, `u`.`username` as `username`,txn_id
  FROM `payment` as `p`
  LEFT JOIN `users` as `u` on `p`.`uid` = `u`.`id`
  LEFT JOIN `payment_methods` as `m` on `p`.`method` = `m`.`id`
  WHERE `p`.`uid` = '$uid'
  ORDER BY `p`.`id` DESC LIMIT 10
  ");
while ($row = mysql_fetch_array($query)) {
?>
      <tr>
        <td>
<?php
    if ($row['date'] == '0000-00-00 00:00:00') {
      $general ->toolTipClass('N/A', 'Date is note defined!', 'label label-gray');
    }else {
      $recordDate = strtotime($row['date']);
      if (gmdate("Ymd") == date("Ymd", $recordDate)){
          $time =  date("h:i A", $recordDate);
          $general ->toolTipClass($time , "Today on " . $time, 'label label-gray');
      }
      else {
          $date = date("Y M", $recordDate) . " " . intval(date("d", $recordDate));
          $fullDate = date("Y/m/d h:i A", $recordDate);
          $general ->toolTipClass($date, $fullDate, 'label label-gray');
      }
    }
?>
        </td>
        <td><?php echo $row['method'];?></td>
        <td><?php echo $row['txn_id'];?></td>
        <td><?php echo $row['amount'];?></td>

<?php } ?>
            </tbody>
          </table>
          <hr>
          <div class="text-center">
            <a class="btn btn-sm btn-default flat" href="payments.php">View full payment report</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(function () {
    $('.table').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<?php
  include 'includes/footer.php';
?>