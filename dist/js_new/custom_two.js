$(document).ready(function() {

	if ($('.work-with').length) {
		var typed = new Typed('.work-with', {
		  strings: ["Facebook", "Twitter", "Instagram", "YouTube", "Google +", "SoundCloud", "Vine", "Pinterest", "Vimeo", "LinkedIn", "Tumblr", "VKontakte"],
		  typeSpeed: 100,
		  backSpeed: 100,
		  loop: true
		});
	}

	$('.select').material_select();

	if ($('.see-this').length > 0) {
		$('.see-this').removeAttr('hidden');
	}

	$('body').niceScroll({
		cursorwidth: "6px",
		scrollspeed: 100,
		zindex: 1000
	});

	$('.need-carousel-wrapper .card .card-reveal').niceScroll({
		cursorwidth: "6px",
		scrollspeed: 100,
		zindex: 0
	});
});

// Search top menu

$('.nav-mobile .search a').click(function() {
	var el = $(this).parents('nav');

	if (el.width() < 900) {
		$('.bannerSearch').focus();
		$('body').scrollTo($('.bannerSearch'), 500);
		return;
	}

	el.find($('.searchLi')).css('width', 400);
	el.find($('.searchLi')).removeClass('hidden').toggleClass('shown');
	el.find($('.searchLi input')).css('width', 400).focus();
});

$('.search-parent .searchbar').on('keyup focus', function() {
	var keywords = $.trim($(this).val());
	var url = $(this).attr('data-url');
	var parent = $(this).parents('.search-parent');

	if (keywords.length < 1) {
		parent.find($('.result-wrap .searchResult')).addClass('hidden');
		return;
	}

		$.ajax({
			method: 'POST',
			url: url,
			data: {'keywords' : keywords},
			success: function(data) {
				// parent.find($('.progress')).removeClass('hidden');

				if ($.trim(data) == 'false') {
					return;
				}

				var service = $.parseJSON(data);

				var replace = '';

				if (service.length == 0) {
					replace = '<a><span class="item">No Services Found.!</span></a>';
				} else {
					var i = 0;
					$.each(service, function(index, value) {
						i++;
						replace += '<a class="clickable-link" href="'+ getBaseUrl() +'/service/'+ value.slug +'"><span class="item">'+ value.service_name +' - $'+ value.price+'</span></a>';

						if (i == 5) {
							replace += '<a class="clickable-link" href="'+ getBaseUrl() +'/services/search?q='+ keywords +'"><span class="item"> More ('+ (service.length - 5) +' )</span></a>';
							return false;
						}

					});

				}

				// parent.find($('.progress')).addClass('hidden');
				parent.find('.result-wrap').css('width', parent.find('.searchInput').innerWidth()+'px');
				parent.find($('.result-wrap .searchResult')).html(replace).removeClass('hidden');

			}
		});

}).blur(function(e) {

	var parent = $(this).parents('.search-parent');

	if (e.relatedTarget == null || ($(e.relatedTarget).prop('class') != 'clickable-link')) {
		parent.find($('.result-wrap .searchResult')).html('');
	}

});

// Product tabs click
$('.product-wrapper .tabas .tab').click(function(e) {
	e.preventDefault();
	$(this).parent().find('.tab a').removeClass('active');
	$(this).find('a').addClass('active');

	$(window).scrollTo($(this).find('a').attr('href'), 1000, {
		offset: -85
	});
});

 $(document).ready(function(){

 	if(!$('.index').length) {
 		//return;
 	}

 	// Menu Height
 	var maxHeight = [];
	$('.menu-wrap-item').each(function() {

		maxHeight.push($(this).height());
	});

 	 $('.menu-wrap-item').height(Math.max.apply(null, maxHeight));

	 	Materialize.updateTextFields();

	 	$(".navigationNormal .user a, .navigationNormal .user img, .navigationTransparent .user a, .navigationTransparent .user img").sideNav();
	 	$(".navigationNormal .navigation-toggle, .navigationTransparent .navigation-toggle").sideNav();
	 	$(".toggle-category").sideNav();

		// Who need us Carousel
		 $(".carousel").owlCarousel({
		 	loop:true,
		    margin:10,
		    responsiveClass:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:3
		        },
		        1000:{
		            items:6,
		            loop:true
		        }
		    }
		 });

		var need = $(".carousel").data('owlCarousel');
		// Custom Navigation Events
		var buttons = $('.carousel-handlers button');


		buttons.on('click', function(e){
			e.preventDefault();
			if( $(this).hasClass('next') ) {
				need.next();
			} else {
				need.prev();
			}
		});


	    if ($('.navigation').length) {
	    	$('.navigation').pushpin({
		    	top: $('.navigation').offset().top,
		    	offset: 0
		    });
	    }

  });



$(document).ready(function() {

	$('.banner-button').click(function() {
		$(window).scrollTo($('.navigation'), 1000);
	});

});


// About us

$('.about-us-menu').pushpin({
	top: 80,
	offset: 0
});


// see this carousel

$(document).ready(function() {
	if(!$('.see-this')) {
		return;
	}

	$('.service-carousel').owlCarousel({
 		loop:true,
	    margin:10,
	    responsiveClass:true,
	    responsive:{
	        0:{
	            items:1,
	            loop: true
	        },
	        600:{
	            items:2,
	            loop: true
	        },
	        1000:{
	            items:3,
	            loop:true
	        }
	    }
 	});




});

// about-us

$('.about-us .header').click(function() {
	var year = $(this).attr('data-year');

	$('.about-us .header').removeClass('active');
	$(this).addClass('active');

	$('.data-wrapper .year').hide();

	$('.data-wrapper .year').each(function() {

		if($(this).hasClass(year)) {
			$(this).show().addClass('active');
		}
	});

});




$('.partners-carousel').owlCarousel({
 		loop:true,
	    margin:10,
	    responsiveClass:true,
	    responsive:{
	        0:{
	            items:1,
	            loop: true
	        },
	        600:{
	            items:3,
	            loop: true
	        },
	        1000:{
	            items:6,
	            loop:true
	        }
	    }
 	});


// Hiding Services
var rows = $('.our-services .row').length;
$('.our-services .row').slice(2-rows).hide();

$('.our-services .show-me-more').click(function() {
	$(this).toggleClass('active');

	if ($(this).hasClass('active')) {
		$('.our-services .row').slideDown(500);
		$(this).text('Hide This');
		$(window).scrollTo($('.our-services .row:eq(3)'), 700, {
			offset: -85
		});
	} else {
		var rows = $('.our-services .row').length;
		$('.our-services .row').slice(2-rows).slideUp(500);
		$(window).scrollTo($('.our-services .row:first'), 700, {
			offset: -85
		});
		$(this).text('Show Me More');
	}
});


// STAR RATING ON FORM

$(document).ready(function() {

	if(!$('.rate').length) {
		return;
	}

  $('.rate').rateYo({
	starWidth: "40px",
	rating: $('#rating').val(rating),
	normalFill: '#d8d6d6',
	precision: 1,
	rating: 0,
    numStars: 5,
    minValue: 1,
    maxValue: 5,
    retedFill: '#f9cc0d',
     onChange: function (rating, rateYoInstance) {

     	if (rating > 0) {
     		$('.rating-val').show().text(rating);
     	} else {
     		$('.rating-val').hide().text(rating);
     	}

      	$('#rating').val(rating);
    }

  });

  $('.totalRating').rateYo({
	starWidth: "25px",
	rating: $('.totalRating').text(),
	normalFill: '#d8d6d6',
	retedFill: '#f9cc0d',
	readOnly: true
  });

});


// STAR RATING

$(document).ready(function() {
	if($('.stars').length){
	    $('span.stars').stars();
	}
});


$.fn.stars = function() {
    return $(this).each(function() {
        // Get the value
        var val = parseFloat($(this).html());
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;
        // Create stars holder
        var $span = $('<span />').width(size);
        // Replace the numerical value with stars
        $(this).html($span);
    });
}

// $('form').submit(function() {
// 	$('.loader').show();
// });


// FONT RESIZING
$(window).bind('ready resize change load', function(e) {
	var width = $(window).width();

	if(!$('.resizeFont').length) {
		return;
	}

	$('.resizeFont').each(function() {
		if(width > 1024) {
			$(this).css('font-size', $(this).attr('data-max')+'px');
		}else if (width < 1024 && width > 600) {
			$(this).css('font-size', $(this).attr('data-med')+'px');
		} else if (width < 600) {
			$(this).css('font-size', $(this).attr('data-min')+'px');
		}

	});
});
$(document).ready(function() {
	var value = $('.brand-logo').attr('data-val');
	var logo = $('.brand-logo img').attr('src');

	$(document).on('scroll ready', function() {
		if ($(this).scrollTop() > 500) {
			if ($('.gtr-main-banner').length > 0) {
				$('.navbar-fixed nav').removeClass('navigationTransparent').addClass('navigationNormal');
			}
			$('.brand-logo').css('padding-top', '0px').html('<span>'+value+'</span>');
			$('.navbar-fixed nav').addClass('animated fadeInDown');
		} else {
			if ($('.gtr-main-banner').length > 0) {
				$('.navbar-fixed nav').removeClass('navigationNormal').addClass('navigationTransparent');
			}
			$('.brand-logo').css('padding-top', '10px').html('<img src="'+logo+'" height="60px">');
		}
	});
});

function getBaseUrl() {
    return window.location.protocol+'//'+window.location.host;
}
