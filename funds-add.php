<?php 
require 'includes/header.php';
?>

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='funds-add.php']" ).parent().addClass( "active" );
  
});
</script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-money"></i> <?php lang('add_fund'); ?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="dashboard.php"><i class="fa fa-dashboard"></i><?php lang('dashboard'); ?></a></li>
    <li class="active"><?php lang('add_fund'); ?></li>

  </ol>
</section><!-- /.content Header-->

<section class="content-header">
<?php
//if submit
if(isset($_POST['add_funds'])){
  
  $amount = mysqli_real_escape_string($conn ,htmlspecialchars($_POST['amount']));
  if (!preg_match("/^[0-9]*$/",$amount) || $amount <= 0 ){
      $general -> alert('Please enter a valid amount!', 'danger');
  }else{
    $_SESSION['amount']=$amount;
    //header("location:paypal.php");
    echo '<script> location.replace("paypal.php"); </script>';
  }
}

$userid = $_SESSION['userid'];
$sql=mysqli_query($conn ,"SELECT * FROM `users` WHERE id='$userid'");
$row = mysqli_fetch_assoc($sql);
?>
</section>

<!-- Content -->
<section class="content">
  <div class="row">
      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-body">
            <div class="text-center">
              <h1><?php lang('user_balance'); ?></h1>
              <p><?php lang('your_balance_1'); ?> <span class="label label-success"><b><?php echo $currency." ".$userBalance; ?></b></span> <?php lang('your_balance_2'); ?>.</p>
              <hr>
<?php
$sql= mysqli_query($conn ,"SELECT description FROM `paragraphs` WHERE `id` = 5");
$balanceNote = mysqli_fetch_array($sql);
echo $balanceNote[0];
?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
      <div class="box box-primary col-md-6">
        <div class="box-body">
          <h3><?php lang('add_fund'); ?></h3>
          <hr />
          <h4><?php lang('pay_using'); ?> <b>PayPal</b></h4>
          <form method="post" action="funds-add.php">
            <div class="form-group">
              <label for="amount"><?php lang('amount_USD'); ?></label>
              <input type="number" class="form-control" id="amount" name="amount" placeholder="<?php lang('amount'); ?> in <?php echo $currency; ?>" required>
            </div>
            <input type="submit" class="btn btn-primary flat btn-block" name="add_funds" value="<?php lang('add_fund'); ?>">
          </form>
        </div>
      </div>
    </div>
  </div>
</section><!-- /.content -->




<?php 
require 'includes/footer.php';
?>