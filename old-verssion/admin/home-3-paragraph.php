<?php 
require 'includes/header.php';
require_once 'libraries/htmlpurifier/HTMLPurifier.auto.php';
$config = HTMLPurifier_Config::createDefault();
$purifier = new HTMLPurifier($config);
?>
<link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<link href="../plugins/iconpicker/dist/css/fontawesome-iconpicker.min.css" rel="stylesheet">

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#home']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='home-3-paragraph.php']" ).parent().addClass( "active" );
});
</script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-file-text-o"></i> 3 paragraphs section
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li>Home page</li>
    <li class="active">3 paragraphs section</li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">

<?php
// Update
if(isset($_POST['submit'])) {
  // get fields
  $title1 = mysql_real_escape_string(htmlspecialchars($_POST['title1']));
  $icon1 = mysql_real_escape_string(htmlspecialchars($_POST['icon1']));

  $desc1 = $_POST['desc1'];
  $desc1 = $purifier->purify($desc1);
  $desc1 = mysql_real_escape_string($desc1);

  $title2 = mysql_real_escape_string(htmlspecialchars($_POST['title2']));
  $icon2 = mysql_real_escape_string(htmlspecialchars($_POST['icon2']));

  $desc2 = $_POST['desc2'];
  $desc2 = $purifier->purify($desc2);
  $desc2 = mysql_real_escape_string($desc2);

  $title3 = mysql_real_escape_string(htmlspecialchars($_POST['title3']));
  $icon3 = mysql_real_escape_string(htmlspecialchars($_POST['icon3']));

  $desc3 = $_POST['desc3'];
  $desc3= $purifier->purify($desc3);
  $desc3 = mysql_real_escape_string($desc3);

  //insert data
  $sql = mysql_query("INSERT INTO `paragraphs` (`id`, `title`, `icon`, `description`)
                      VALUES ('2', '$title1', '$icon1', '$desc1'),
                      ('3', '$title2', '$icon2', '$desc2'),
                      ('4', '$title3', '$icon3', '$desc3')
                      ON DUPLICATE KEY UPDATE `id`=VALUES(`id`),
                      `title`=VALUES(`title`),
                      `icon`=VALUES(`icon`),
                      `description`=VALUES(`description`)
                      ");
  //test
  if(!$sql){
      die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysql_error() . "<div>");
  }
  echo "<div class=\"alert alert-success\" role=\"alert\">Details Successfully Updated!</div>";
}

/** Reset
if(isset($_GET['reset'])) {
  $sql = mysql_query("INSERT INTO `paragraphs` (`id`, `title`, `icon`, `description`)
                      VALUES ('2', 'Our vision', 'fa-arrow-circle-o-right', 'Pellentesque ut mi cursus nisi sollicitudin tristique a a tellus. In quis nulla dignissim, blandit arcu in, dignissim tortor. Aenean fermentum, libero in condimentum ultrices, neque ipsum sollicitudin diam, a lobortis nisi velit id ligula. In velit neque, facilisis eu sem non, rhoncus ultricies ante.'),
                      ('3', 'Our Philosophy', 'fa-binoculars', 'Pellentesque ut mi cursus nisi sollicitudin tristique a a tellus. In quis nulla dignissim, blandit arcu in, dignissim tortor. Aenean fermentum, libero in condimentum ultrices, neque ipsum sollicitudin diam, a lobortis nisi velit id ligula. In velit neque, facilisis eu sem non, rhoncus ultricies ante.'),
                      ('4', 'Why Us', 'fa-bullseye', 'Pellentesque ut mi cursus nisi sollicitudin tristique a a tellus. In quis nulla dignissim, blandit arcu in, dignissim tortor. Aenean fermentum, libero in condimentum ultrices, neque ipsum sollicitudin diam, a lobortis nisi velit id ligula. In velit neque, facilisis eu sem non, rhoncus ultricies ante.')
                      ON DUPLICATE KEY UPDATE `id`=VALUES(`id`),
                      `title`=VALUES(`title`),
                      `icon`=VALUES(`icon`),
                      `description`=VALUES(`description`)
                      ");
  //test
  if(!$sql){
      die("<div class=\"alert alert-danger\" role=\"alert\">Could not reset data: " . mysql_error() . "<div>");
  }
  echo "<div class=\"alert alert-success\" role=\"alert\">Details Successfully Reset!</div>";
}
**/

// Get data
$data = mysql_query("SELECT * FROM `paragraphs` WHERE `id`='2'");
$row = mysql_fetch_array($data);
$title1 = $row['title'];
$icon1 = $row['icon'];
$desc1 = $row['description'];

$data = mysql_query("SELECT * FROM `paragraphs` WHERE `id`='3'");
$row = mysql_fetch_array($data);
$title2 = $row['title'];
$icon2 = $row['icon'];
$desc2 = $row['description'];

$data = mysql_query("SELECT * FROM `paragraphs` WHERE `id`='4'");
$row = mysql_fetch_array($data);
$title3 = $row['title'];
$icon3 = $row['icon'];
$desc3 = $row['description'];
?>

  <div class="box box-primary">
    <div class="box-body">
      <form method="post" action="home-3-paragraph.php">
        <div class="form-group">
          <h3><i class="fa fa-file-text-o"></i> Paragraph <b>#1</b></h3>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label for="title1">Title</label>
            <input type="text" class="form-control" value="<?php echo $title1 ?>" id="title1" name="title1" placeholder="Title...">
          </div>
          <div class="form-group">
            <label for="icon3">Icon</label>
            <div class="input-group">
              <span class="input-group-addon"></span>
              <input type="text" data-placement="bottomLeft" class="form-control icp icp-auto" value="<?php echo $icon1 ?>" id="icon1" name="icon1" placeholder="Icon code...">
            </div>
          </div>
          <div class="form-group">
            <label for="desc1">Description</label>
            <textarea class="form-control" id="desc1" name="desc1" rows="4" placeholder="Description..."><?php echo $desc1 ?></textarea>
          </div>
        </div>
        <div class="clearfix"></div>

        <div class="form-group">
          <h3><i class="fa fa-file-text-o"></i> Paragraph <b>#2</b></h3>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label for="title2">Title</label>
            <input type="text" class="form-control" value="<?php echo $title2 ?>" id="title2" name="title2" placeholder="Title...">
          </div>
          <div class="form-group">
            <label for="icon3">Icon</label>
            <div class="input-group">
              <span class="input-group-addon"></span>
              <input type="text" data-placement="bottomLeft" class="form-control icp icp-auto" value="<?php echo $icon2 ?>" id="icon2" name="icon2" placeholder="Icon code...">
            </div>
          </div>
          <div class="form-group">
            <label for="desc2">Description</label>
            <textarea class="form-control" id="desc2" name="desc2" rows="4" placeholder="Description..."><?php echo $desc2 ?></textarea>
          </div>
        </div>
        <div class="clearfix"></div>

        <div class="form-group">
          <h3><i class="fa fa-file-text-o"></i> Paragraph <b>#3</b></h3>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label for="title3">Title</label>
            <input type="text" class="form-control" value="<?php echo $title3 ?>" id="title3" name="title3" placeholder="Title...">
          </div>
          <div class="form-group">
            <label for="icon3">Icon</label>
            <div class="input-group">
              <span class="input-group-addon"></span>
              <input type="text" data-placement="topLeft" class="form-control icp icp-auto" value="<?php echo $icon3 ?>" id="icon3" name="icon3" placeholder="Icon code...">
            </div>
          </div>
          <div class="form-group">
            <label for="desc3">Description</label>
            <textarea class="form-control" id="desc3" name="desc3" rows="4" placeholder="Description..."><?php echo $desc3 ?></textarea>
          </div>
        </div>
        <div class="clearfix"></div>
              
              
        <input type="submit" class="btn btn-primary flat margin-top-20" name="submit" value="UPDATE">
      </form>
  	</div>
  </div>
</section><!-- /.content -->

<script src="../plugins/iconpicker/dist/js/fontawesome-iconpicker.js"></script>
<script>
  $(function() {
    $('.icp-auto').iconpicker();
  });
</script>

<script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script>
  $(function () {
    $("textarea").wysihtml5();
  });
</script>

<?php 
require 'includes/footer.php';
?>