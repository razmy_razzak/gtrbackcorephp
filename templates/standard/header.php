<!DOCTYPE html>
<!--[if IE 7]> <html lang="en" class="ie7"> <![endif]-->  
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title><?php echo $title ?></title>

    <!-- Meta -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="<?php echo $description ?>" />
<?php
    if (isset($kw)) {
        echo '<meta name="keywords" content="'.$kw.'" />';
    }
?>
    <meta name="author" content="SEOeStore" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />

    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

    <!-- font-awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">    

    <!-- custom css -->
    <link rel="stylesheet" href="dist/css/style1.css?v=3" />
    <link rel="stylesheet" href="dist/css/themes/color.php?v=2" id="style_color?v=2" />
    <!-- JS Global Compulsory -->     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- JS Implementing Plugins -->
    <script type="text/javascript" src="plugins/back-to-top.js"></script>
    <script type="text/javascript" src="plugins/jquery.parallax.js"></script>
    <!-- JS Customization -->
    <script type="text/javascript" src="dist/js/custom.js"></script>
    <!--[if lt IE 9]>
        <script src="assets/js/respond.js"></script>
    <![endif]-->
<?php
if ($dir == 'rtl') {
    ?>
    <link rel="stylesheet" href="dist-rtl/css/rtl.css" />
    <style type="text/css">

        .social-icons li , .service i{
            float:right !important;
        }

    </style>

<?php } ?>
</head> 
<body>
    <!--=== Header ===-->
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="navbar-header margin-top-20 maggin-bottom-30">
                <!-- Logo -->
                <a href="index.php">
                    <?php if(empty($logo)){
                             echo '<h1>'.$site_name.'</h1>';
                        }else{
                             echo '<img class="img-responsive" src="'. $logo . '" alt="Logo" />';
                        }?>
                </a>
            </div>

            <div class="collapse navbar-collapse menu" id="navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <?php
            if(mysqli_num_rows($custom_pages) > 0 ){
            while ($data= mysqli_fetch_array($custom_pages)) {
                               ?>
                        <li>
                            <a href='custom-page.php?id=<?php echo $data['id']; ?>'>
                            <?php echo $data['link_text']?>
                            </a>
                        </li>
                        <?php
                            }
                            }
                           if(isset($_SESSION['username'])) {
                               ?>

                            <li>
                                <a class="btn btn-custom hover-effect" href="dashboard.php"><?php lang('dashboard'); ?></a>
                            </li>
                            <?php
                                }else{
                                ?>

                                <li>
                                    <a class="btn btn-default hover-effect" href="login.php"><?php lang('sign'); ?></a>
                                </li>
                                <li>
                                    <a class="btn btn-custom hover-effect" href="register.php"><?php lang('create'); ?></a>
                                </li>
                                <?php    
                                }
                                    ?>
                </ul>
            </div>
            <!-- /navbar -->
        </nav>
    </div>
    <!-- /container -->

    <!--/header -->
    <!--=== End Header ===-->