<header class="s-header">

    <div class="header-logo">
        <a class="site-logo" href="index.html">
            <p class="logo_one"><strong class="logo_two">G</strong><strong class="logo_three">TR</strong>   Backlinks</p>
        </a>
    </div> <!-- end header-logo -->

    <nav class="header-nav">

        <a href="#0" class="header-nav__close" title="close"><span>Close</span></a>

        <div class="header-nav__content">
            <h3>GTR backlinks</h3>

            <?php if(isset($_SESSION['id']) && $_SESSION['id'] != '' ){?>
                <p class="username">Username: <strong><?php echo  $_SESSION['username']?></strong> </p>
            <?php } ?>

            <ul class="header-nav__list">
                <li class="current"><a class="smoothscroll"  href="#home" title="home">Home</a></li>
                <li><a class="smoothscroll"  href="#about" title="about">About</a></li>
                <li><a class="smoothscroll"  href="#services" title="about">What we do</a></li>
                <?php
                if(mysqli_num_rows($custom_pages) > 0 ){
                while ($data= mysqli_fetch_array($custom_pages)) {
                    if($data['id'] != 2){
                ?>
                <li><a class=""  href="custom-page.php?id=<?php echo $data['id']; ?>" title="services"><?php echo $data['link_text']?></a></li>
                <?php } } } ?>
                <li><a class="smoothscroll"  href="user_pass_rest.php" title="contact">Password Reset</a></li>
            </ul>

            <p>Perspiciatis hic praesentium nesciunt. Et neque a dolorum <a href='#0'>voluptatem</a> porro iusto sequi veritatis libero enim. Iusto id suscipit veritatis neque reprehenderit.</p>

            <ul class="header-nav__social">
                <li>
                    <a href="#0"><i class="fab fa-facebook"></i></a>
                </li>
                <li>
                    <a href="#0"><i class="fab fa-twitter"></i></a>
                </li>
                <li>
                    <a href="#0"><i class="fab fa-instagram"></i></a>
                </li>
                <li>
                    <a href="#0"><i class="fab fa-behance"></i></a>
                </li>
                <li>
                    <a href="#0"><i class="fab fa-dribbble"></i></a>
                </li>
            </ul>

        </div> <!-- end header-nav__content -->

    </nav> <!-- end header-nav -->

    <a class="header-menu-toggle" href="#0">
        <span class="header-menu-icon"></span>
    </a>

</header> <!-- end s-header -->