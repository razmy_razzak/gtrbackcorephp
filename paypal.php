<?php
require 'includes/header.php';
require 'includes/paypal_class.php';
require 'includes/paypal_config.php';

?>

<section class="content">
    <div class="box box-info">
      <div class="box-body">
        <?php
        $p = new paypal_class;       
        $p->paypal_url = PAYPAL_URL;
        $this_script = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
        if (empty($_GET['action'])) $_GET['action'] = 'process';

        switch ($_GET['action']) {
           case 'process':
                        $p->add_field('business', BUSINESS_URL);
                        $p->add_field('return', "http://" . $_SERVER['HTTP_HOST'] . '/paypal.php?action=success');
                        $p->add_field('cancel_return', "http://" . $_SERVER['HTTP_HOST'] . '/paypal.php?action=cancel');
                        $p->add_field('notify_url', "http://" . $_SERVER['HTTP_HOST'] . '/paypal_ipn.php');
                        $p->add_field('item_name', ITEM_NAME);
                        $p->add_field('on0', 'username');
                        $p->add_field('os0', $_SESSION["username"]);
                        $p->add_field('currency_code',$currency);
                        $p->add_field('amount', $_SESSION['amount']);
                        $p->add_field('custom', $_SESSION['userid']);
                        $p->submit_paypal_post();
                  break;

           case 'success':
                        echo "<center><h1>Payment Transaction Done Successfully</h1>";
                        echo '<h4>Your payment will add to your balance soon!</h4></center>';
                break;

           case 'cancel': 
                        echo "<center><h1>Transaction Canceled!</center>";
                break;
         }
        ?>
       </div>
    </div>
</section>

<?php include 'includes/footer.php';?>