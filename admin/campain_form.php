<?php
require_once 'includes/header.php';
require_once 'libraries/htmlpurifier/HTMLPurifier.auto.php';
require_once 'AdminController/Campaign.php';
$config = HTMLPurifier_Config::createDefault();
$purifier = new HTMLPurifier($config);
$custom_css = fopen("../dist/css/custom.css", "r+") or die("Unable to open file!");
?>
    <script>
        $(document).ready(function(){
            $( ".sidebar-menu li a[href^='custom-css-manage.php']" ).parent().addClass( "active" );
        });
    </script>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> gdgfds
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="../index.php">
                    <i class="fa fa-dashboard"></i>Dashboard</a>
            </li>
            <li>Campaign Pages</li>
            <li class="active">Manage Campaign pages</li>
        </ol>
    </section>
    <!-- /.content Header-->
    <!-- Content -->
    <section class="content">
        <?php
        if (isset($_POST['price_range'])) {
            $order = mysqli_real_escape_string($conn , htmlspecialchars($_POST['order']));
            $price_range = mysqli_real_escape_string($conn , htmlspecialchars($_POST['price_range']));
            if( $order && $price_range ){
                $data =[
                    'order' => $order,
                    'price_range' => $price_range
                ];
                $campain = new Campaign();
                $result = $campain->createCampaignPrice( $data );
                if ($result['tag'] == 503) {
                    echo "<div class=\"alert alert-danger\" role=\"alert\">Error!</div>";
                } else {
                    echo "<div class=\"alert alert-success\" role=\"alert\">Services Successfully Updated!</div>";
                }
            }
        }
        ?>
        <div class="box box-primary">
            <div class="box-body">
                <form  action="" method="post">
                    <div class="form-group">
                        <span>Set price range and products for Campaign pages</span>
                    </div>
                    <div class="form-group">
                        <label class="" for="amount">Order:</label>
                        <div class="input-group">
                            <input type="number"  class="form-control"  name="order" placeholder="order this price tag" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="" for="amount">Price range:</label>
                        <div class="input-group">
                            <input type="number"  class="form-control"  name="price_range" placeholder=" Enter price tag" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>
                <hr>
            </div>
        </div>
    </section>

<?php
require 'includes/footer.php';
?>