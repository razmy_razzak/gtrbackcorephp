<?php 
require 'includes/header.php';
require_once 'libraries/htmlpurifier/HTMLPurifier.auto.php';
$config = HTMLPurifier_Config::createDefault();
$purifier = new HTMLPurifier($config);
?>
<link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<link href="../plugins/iconpicker/dist/css/fontawesome-iconpicker.min.css" rel="stylesheet">

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='main-lang.php']" ).parent().addClass( "active" );
});
</script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-language" aria-hidden="true"></i> Change Language
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li class="active">Change language</li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">
<?php
// Update
if(isset($_POST['submit'])) { 
  // get fields
  for ($i=0; $i < count($_POST['id']); $i++) {
    $id = mysqli_real_escape_string($conn , htmlspecialchars($_POST['id'][$i]));
    $value = mysqli_real_escape_string($conn , htmlspecialchars($_POST['value'][$i]));
    $mysql = mysqli_query($conn , "UPDATE `lang` SET `updated` = '$value' WHERE `id` = '$id'");
  }
  if($mysql != ''){
    echo "<div class=\"alert alert-success\" role=\"alert\">successfully updated!</div>";
  }else{
    echo "<div class=\"alert alert-danger\" role=\"alert\"> error, there was an error in your data</div>";
  }
}
?>
<div class="box box-primary">
  <div class="box-body">
    <form method="post"   >
      <div class="col-md-12">
<?php
// Get data
$data = mysqli_query($conn , "SELECT * FROM `lang`");
while($row = mysqli_fetch_array($data)){

  $id = $row['id'];
  $var = $row['var'];
  $original = $row['original'];
  $updated = $row['updated'];
?>
<div class="form-group">
  <label for="title1"><?php echo $original; ?></label>
  <input type="hidden" name="id[]" value="<?php echo $id; ?>">
  <input type="text" class="form-control" value="<?php if($updated != ''){
        echo $updated;
      }else{
        echo $original;
      } ?>" id="title1" name="value[]" placeholder="Title...">
</div>

<?php
}
?>
        </div>
        <input type="submit" class="btn btn-primary flat margin-top-20" name="submit" value="UPDATE">
      </form>
    </div>
  </div>

</section><!-- /.content -->
<?php 
require 'includes/footer.php';
?>
