<?php 
require 'includes/header.php';
$sql = mysqli_query($conn , "SELECT * FROM `admins`");
$admin = mysqli_fetch_array($sql);
$email = $admin['email'];
$api = $admin['api_key'];
$paypal = $admin['paypal'];
?>

<script>
$(document).ready(function(){
    $( ".navbar-custom-menu li a[href^='settings.php']" ).parent().addClass( "active" );
});
</script>

<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-cogs"></i> Settings
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Settings</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<?php
if(isset($_POST['submit'])) {
    // get fields
    $email = mysqli_real_escape_string($conn , htmlspecialchars($_POST['email']));
    $api = mysqli_real_escape_string($conn , htmlspecialchars($_POST['api']));
    $paypal = mysqli_real_escape_string($conn , htmlspecialchars($_POST['paypal']));
    
    //insert data
    $sql = mysqli_query($conn , " UPDATE `admins` SET `email`='$email', `api_key`='$api', `paypal`='$paypal' WHERE id =1 ");

    //test
    if(!$sql){
        die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysqli_error($conn) . "<div>");
    }
    echo "<div class=\"alert alert-success\" role=\"alert\">Password Successfully Updated!</div>";
}
?>


        <div class="box box-primary">
            <div class="box-body">
                <form action="settings.php" method="post">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="emai" >E-mail:</label>
                            <input class="form-control" type="text" id="password" name="email" placeholder="Insert new E-mail..." value="<?php echo $email ;?>" />
                        </div>                       
                        
                        <div class="form-group">
                            <label for="api" >API:</label>
                            <input class="form-control" type="text" id="password" name="api" placeholder="Insert new API..." value="<?php echo $api ;?>" />
                        </div>                       
                        <div class="form-group">
                            <label for="paypal" >Paypal Email:</label>
                            <input class="form-control" type="text" id="password" name="paypal" placeholder="Insert new API..." value="<?php echo $paypal ;?>" />
                        </div>                       
                                             
                        
                        <input class="btn btn-primary flat" type="submit" name="submit" value="Update " />
                    </div>
                </form>
    </section><!-- /.content -->




<?php 
require 'includes/footer.php';
?>