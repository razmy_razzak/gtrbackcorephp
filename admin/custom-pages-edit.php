<?php
require_once './includes/connect.php';
require_once 'includes/header.php';
require_once 'libraries/htmlpurifier/HTMLPurifier.auto.php';
$config = HTMLPurifier_Config::createDefault();
$purifier = new HTMLPurifier($config);

?>
<link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#custom_pages']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='custom-pages-edit.php']" ).parent().addClass( "active" );
});
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-users"></i> Edit Custom Page
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="index.php">
                <i class="fa fa-dashboard"></i>Dashboard</a>
        </li>
        <li class="active">Edit Custom Page</li>
    </ol>
</section>
<!-- /.content Header-->
<!-- Content -->
<section class="content">
<?php
function getPages(){
  $sql = mysqli_query($conn , "SELECT `id`,`link_text` FROM `custom_pages`");
  while($record = mysqli_fetch_array($sql)){
    echo '<option value="' . $record['id'] . '">' . $record['link_text'] . '</option>'; 
  }
}
if(isset($_POST['edit'])){
    //Get data
    $id = mysqli_real_escape_string($conn , htmlspecialchars($_POST['id']));
    $text_link = mysqli_real_escape_string($conn , htmlspecialchars($_POST['text_link']));
    $title = mysqli_real_escape_string($conn , htmlspecialchars($_POST['title']));
    $content = mysqli_real_escape_string($conn , $_POST['content']);
      $content = $purifier->purify($content);
    $status = mysqli_real_escape_string($conn , htmlspecialchars($_POST['status']));
    $sql = mysqli_query($conn , "UPDATE `custom_pages` SET `link_text`= '$text_link',`title`='$title',`content`='$content',`status`='$status' WHERE id = $id");
    if(!$sql){
          die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysqli_error($conn) . "<div>");
      }
    echo "<div class=\"alert alert-success\" role=\"alert\">Custom Page  Successfully Updated!</div>";
}
if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
?>
  <div class="box box-primary">
    <div class="box-body">
      <form method="get" action="">

        <div class="form-group">
          <label for="user">Choose user to edit</label>
            <select class="form-control" name="id" required>
            <option value="">- Select Custom Page  -</option>
            <?php getPages(); ?>
            </select>
        </div>
        <input type="submit" class="btn btn-primary flat" value="Edit">
      </form>
    </div>
  </div>
  <?php
} else {
  $id = mysqli_real_escape_string($conn , htmlspecialchars($_GET['id']));
$custom_pages = mysqli_query($conn , "SELECT * FROM `custom_pages` where id = $id");
while ($data= mysqli_fetch_array($custom_pages)) {
$text_link=$data['link_text'];
$title=$data['title'];
$content=$data['content'];
$status=$data['status'];
}
?>
    <div class="box box-primary">
        <div class="box-body">
                <form  method="post">
                    <div class="form-group ">
                        <label for="ID" class="form-label">ID</label>
                        <input class="form-control" type= "text" id="id" name="id" value="<?php echo $id; ?>" readonly>
                    </div>
                    <div class="form-group ">
                        <label for="text_link" class="form-label">TEXT Link</label>
                        <input type="text" class="form-control" name="text_link"   value='<?php echo $text_link;  ?>'>
                    </div>
                    <div class="form-group ">
                        <label for="title" class="form-label">Title</label>
                        <input type="text" class="form-control" name="title"  value='<?php echo $title;  ?>'>
                    </div>
                    <div class="form-group ">
                        <label for="content" class="form-label">Content</label>
                        <textarea type="text" class="form-control" id="editor1" name="content"  cols="30" rows="10"><?php echo $content;?></textarea> 
                    </div>
                    <div class="form-group ">
                        <label for="content" class="form-label">Status</label>
                        <select class="form-control" name="status">
                            <option value="enable" <?php if ($status == "enable"){
                                echo "selected";}?>>Enable</option>
                            <option value="disable" <?php if ($status == "disable"){
                                echo "selected";}?>>Disable</option>
                        </select>
                    </div>                    

                    <div class="form-group ">
                        <div class="offset-sm-2 col-sm-10">
                            <button type="submit" name='edit' class="btn btn-primary">Edit</button>
                        </div>
                        

                   
                </form>
            </div>
    </div>
</section>
<script src="../plugins/ckeditor/ckeditor.js"></script>
<script>
      $(function () {
        CKEDITOR.replace('editor1');
      });
</script>
<?php 
}
require 'includes/footer.php';
?>