<?php
require_once 'includes/connect.php';
$data = mysql_query("SELECT * FROM `main` WHERE `id` = '1'");
$row = mysql_fetch_array($data);
  $site_name = $row['site_name'];
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login to <?php echo $site_name;?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/style.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        Login to <?php echo $site_name;?>
      </div><!-- /.login-logo -->
      <p class="text-center">Login to your account. Manage your orders & reports.</p>


<?php
session_start();
if (isset($_SESSION['username'])) {
    $user_check = $_SESSION['username'];
    // SQL Query To Fetch Complete Information Of User
    $ses_sql=mysql_query("SELECT `username` FROM `users` WHERE username='$user_check'");
    $row = mysql_fetch_assoc($ses_sql);
    $login_session =$row['username'];
};
if(isset($login_session)){
    echo "<script type='text/javascript'> document.location = 'dashboard.php'; </script>";
}
?>



<?php

if (isset ($_POST['submit'])){

$username = mysql_real_escape_string(htmlspecialchars($_POST['user_name']));
$password = mysql_real_escape_string(htmlspecialchars($_POST['password']));


    $sql="SELECT * FROM `users` WHERE username='$username' AND password='$password'";
    $result=mysql_query($sql);
    // Mysql_num_row is counting table row
    $count=mysql_num_rows($result);


    //$user_id = mysql_result($result, 0, "id");
    //$user_status = mysql_result($result, 0, "status");

    //$user_record = mysql_fetch_assoc ($result , 0);

    while ($record = mysql_fetch_array($result)){
        $user_id = $record['id'];
    }

    // If result matched $myusername and $mypassword, table row must be 1 row
    if($count==1){

        // Register $myusername, $mypassword and redirect to file "login_success.php"
        //session_register("username");
        $_SESSION['username']= $username;
        $_SESSION['userid']= $user_id;

        echo '<div class="alert alert-success">Login OK!</div>';
        //header("Location: order.php");
        echo "<script type='text/javascript'> document.location = 'dashboard.php'; </script>";
        //echo $_SESSION['username'];
    }
    else {
        echo '<div class="alert alert-danger">Wrong Username or Password!</div>';
    }


    mysql_close($conn);
}

?>





      <div class="login-box-body">
        <p class="login-box-msg">Sign in to access</p>
        <form action="login.php" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Username" name="user_name" required />
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password" required/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>

          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </form>

        <div class="form-group text-right small margin-top-20">
            <a href="reset.php?username">Forget username</a>
             - 
            <a href="reset.php?password">Forget password</a>
        </div>
        <hr>
        Not a member? <a href="register.php">Register Now</a>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <div class="container text-center">
      <a href="index.php"><i class="fa fa-home" aria-hidden="true"></i> Back to home</a>
    </div>


  </body>
</html>
