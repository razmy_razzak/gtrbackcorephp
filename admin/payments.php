<?php
require 'includes/header.php';
?>
<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#reports']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='payments.php']" ).parent().addClass( "active" );
  
});
</script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-money"></i> Payment records

  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li>Reports</li>
    <li class="active">Payment records</li>
  </ol>
</section><!-- /.content Header-->

<!-- DataTables -->
<link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-default">
        <div class="panel-body">

          <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th>ID</th>
                <th><?php $general ->toolTip('Date', 'GMT')?></th>
                <th>Username</th>
                <th>Method</th>
                <th>Transaction ID</th>
                <th>Amount in <?php echo $currency ?></th>
              </tr>
            </thead>
            <tbody>
<?php
$query = mysqli_query($conn , "SELECT `p`.`id`, `p`.`uid`, `m`.`name` as `method`, `p`.`amount`, `p`.`date` as `date`, `u`.`username` as `username`,txn_id
  FROM `payment` as `p`
  LEFT JOIN `users` as `u` on `p`.`uid` = `u`.`id`
  LEFT JOIN `payment_methods` as `m` on `p`.`method` = `m`.`id`
  ORDER BY `p`.`id` DESC
  ");

while ($row = mysqli_fetch_array($query)) {

?>

  <tr>
    <td><?php echo $row['id'];?></td>
    <td>
      <?php
          if ($row['date'] == '0000-00-00 00:00:00') {
            $general ->toolTipClass('N/A', 'Date is note defined!', 'label label-gray');
          }else {
            $recordDate = strtotime($row['date']);
            if (gmdate("Ymd") == date("Ymd", $recordDate)){
                $time =  date("h:i A", $recordDate);
                $general ->toolTipClass($time , "Today on " . $time, 'label label-gray');
            }
            else {
                $date = date("Y M", $recordDate) . " " . intval(date("d", $recordDate));
                $fullDate = date("Y/m/d h:i A", $recordDate);
                $general ->toolTipClass($date, $fullDate, 'label label-gray');
            }
          }
      ?>
    </td>
    <td><?php echo $row['username'];?></td>
    <td><?php echo $row['method'];?></td>
    <td><?php echo $row['txn_id'];?></td>
    <td><?php echo $currency.' '.$row['amount'];?></td>

<?php } ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<script>
  $(function () {
    $('.table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<?php
  include 'includes/footer.php';
?>