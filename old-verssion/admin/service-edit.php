<?php 
require 'includes/header.php';
function getSerOptions(){
    $sql = mysql_query("SELECT * FROM `service` ORDER BY `ordering`");
    while($record = mysql_fetch_array($sql)){
        echo '<option value="' . $record['id'] . '">' . $record['description'] . '</option>'; 
    }
}
?>
<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#service']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='service-edit.php']" ).parent().addClass( "active" );
});
</script>



<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="ion ion-edit"></i> Edit Service
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li>Services</li>
    <li class="active">Edit service</li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">
<?php
if(isset($_POST['submit'])) {
  // get fields
  $id = mysql_real_escape_string(htmlspecialchars($_POST['id']));
  $price = mysql_real_escape_string(htmlspecialchars($_POST['price']));
  $status = mysql_real_escape_string(htmlspecialchars($_POST['status']));
  //$wcolor = mysql_real_escape_string(htmlspecialchars($_POST['wcolor']));
  //insert data
  $sql = mysql_query("UPDATE `service` SET `price`='$price',`status`='$status' WHERE `id`='$id'");
  //test
  if(!$sql){
      die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysql_error() . "<div>");
  }
  echo "<div class=\"alert alert-success\" role=\"alert\">Service Successfully Updated!</div>";
}

if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
?>
  <div class="box box-primary">
    <div class="box-body">
      <form method="get" action="">

        <div class="form-group">
          <label for="cat">Choose Service to edit</label>
            <select class="form-control select2" id="cat" name="id" required>
              <option value="">- select service -</option>
            <?php getSerOptions(); ?>
            </select>
        </div>
        <input type="submit" class="btn btn-primary flat" value="Edit">
      </form>
    </div>
  </div>
  <?php
} else {
  $id = mysql_real_escape_string(htmlspecialchars($_GET['id']));
  $sql = mysql_query("SELECT * FROM `service` WHERE `id`='$id'");
  if(mysql_num_rows($sql) > 0) {
    $data = mysql_fetch_array($sql);
    $id = $data['id'];
    $price = $data['price'];
    $sstatus = $data['status'];
?>

    <div class="box box-primary">
        <div class="box-body">
			<form action="service-edit.php" method="post">
			    <div class="box-body">
					<div class="form-group">
						<label for="name">Service id:*</label>
						<input class="form-control" type= "text" id="id" name="id" value="<?php echo $id; ?>" placeholder="Service name..." readonly>
					</div>
					
					<div class="form-group">
						<label for="price">Service price in cent:*</label><small> Leave the price empty to apply the percentage</small>
						<input class="form-control" type= "text" id="price" name="price" value="<?php echo $price; ?>" placeholder="Service price...">
          </div>

          <div class="form-group ">
          <label>Service status :</label>
            <select class="form-control select2" id="cat" name="status" required>
<?php
  if ( $sstatus != 0) {
  echo '<option value="0" >Disactive</option>';
  echo '<option value="1" selected>Active</option>';
  }else{
  echo '<option value="0" selected >Disactive</option>';
  echo '<option value="1">Active</option>';
  }
?>
            </select>
					</div>
					<input class="btn btn-primary flat" type="submit" name="submit" value="Update" />
					<br /><small>All fields with * are required!</small>
				</div>
			</form>
		</div>
    </div>
<?php
  } else {
    echo "<script type='text/javascript'> document.location = 'Service-edit.php'; </script>";
  }
}
?>
</section><!-- /.content -->
<script>
  $(function () {
  $(".select2").select2();
});
</script>
<?php 
require 'includes/footer.php';
?>