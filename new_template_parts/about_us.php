<!-- about
================================================== -->
<section id='about' class="s-about">

    <div class="row section-header" data-aos="fade-up">
        <div class="col-full">
            <h3 class="subhead">Who We Are</h3>
            <h1 class="display-1">GTR is a social media and Search Engine Optimization
                Company. They started out as GTR Socials, a social media marketing website. They
                have since branched out to provide SEO Services</h1>
        </div>
    </div> <!-- end section-header -->

    <div class="row" data-aos="fade-up">
        <div class="col-full">
        </div>
    </div> <!-- end about-desc -->

    <div class="row">

        <div class="about-process process block-1-2 block-tab-full">

            <div class="process__vline-left"></div>
            <div class="process__vline-right"></div>

            <div class="col-block process__col" data-item="1" data-aos="fade-up">
                <div class="process__text">
                    <h4>How it works</h4>

                    <p>
                        There is nothing to this process. Just create an account
                        with GTR Backlinks—all you need is a working email address. Customize your
                        account to your needs, browse the services on offer, deposit as much money as
                        you want and select the services you desire.
                    </p>
                </div>
            </div>
            <div class="col-block process__col" data-item="2" data-aos="fade-up">
                <div class="process__text">
                    <h4>How to rank</h4>

                    <p>
                        It is really up to you how you choose to use GTR to build
                        your website’s ranking. GTR’s primary goal is to give you access to
                        authoritative backlinks that will build your visibility online.<br>

                        After that, you can do as you please. Whether you choose to
                        deploy the premium indexing services on offer or you attempt to make use of the
                        article submission features to make your backlinks more powerful, know that
                        there is a multitude of options available
                        to you.
                    </p>
                </div>
            </div>
            <div class="col-block process__col" data-item="3" data-aos="fade-up">
                <div class="process__text">
                    <h4>Why Us</h4>

                    <p>
                        Sure, the internet is chockfull of websites selling
                        backlinks. But this is the only one that will help you manage and improve your
                        SEO campaign even if you are an amateur that knows absolutely nothing about
                        Search Engine Optimization.<br>

                        This website has you covered.
                    </p>
                </div>
            </div>
        </div> <!-- end process -->

    </div> <!-- end about-stats -->

</section> <!-- end s-about -->