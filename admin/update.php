<?php 
require 'includes/header.php';
$sql = mysqli_query($conn , "SELECT * FROM `main` WHERE `id`='1'");
$data = mysqli_fetch_array($sql);
$version = $data['version'];
?>

<script>
$(document).ready(function(){
            $(".sidebar-menu li a[href^='update.php']").parent().addClass("active");
});
</script>

<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-refresh"></i> Update
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Update</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<?php
if(isset($_POST['check'])) {
    $url = 'http://updater.seopanel.xyz/index.php?version';

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, '');
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
    $newVersion = curl_exec($ch);
    curl_close($ch);

    if ($newVersion > $version){
        mysqli_query($conn , "UPDATE `main` SET `updates` = '1' WHERE `id` = '1';");
        echo '<div class="alert alert-warning" role="alert">New Update available!</div>';
    }else{
        mysqli_query($conn , "UPDATE `main` SET `updates` = '0' WHERE `id` = '1';");
        echo '<div class="alert alert-success" role="alert">You already have the latest version!</div>';
    }
}

if(isset($_POST['update'])) {
    $url = 'http://updater.seopanel.xyz/index.php';
    $udata = Array(
        'u'=>'',
        'v'=>$version,
        'k'=>'21o51ib8tayxl7ip'
    );
    if (is_array($udata)) {
        $dataString = http_build_query($udata, '', '&');
    }
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
    $result = curl_exec($ch);
    curl_close($ch);

    if (strpos($result, 'http://') !== false) {
        $fileName = pathinfo($result);
        $fileName = $fileName['filename'];
        $localFile = 'tmp/'.$fileName.'.zip';
        if (file_exists($localFile)) {
            unlink($localFile);
        }
        file_put_contents($localFile, file_get_contents($result));
        if (file_exists($localFile)) {
            $zip = new ZipArchive;
            $res = $zip->open($localFile);
            if ($res === TRUE) {
                $zip->extractTo('tmp/'.$fileName);
                $zip->close();

                require 'tmp/'.$fileName.'/router.php';
                router($fileName, $routerArr);
                require 'tmp/'.$fileName.'/db.php';
                insertDb($fileName, $dbArr);

                $updateVersion = explode("_v", $fileName);
                $updateVersion = $updateVersion [1];
                mysqli_query($conn , "UPDATE `main` SET `updates` = '0' WHERE `id` = '1';");
                mysqli_query($conn , "UPDATE `main` SET `version` = '$updateVersion' WHERE `id` = '1';");

                echo '<div class="alert alert-success" role="alert">Update successfully done!</div>';
            }
        }

    }
}

/** //Example router.php file
 * $routerArr = array
 *         (
 *         array('type'=>'folder' , 'file'=>'abc.zip', 'path'=>'/'),
 *         array('type'=>'folder' , 'file'=>'abc2.zip', 'path'=>'/admin/'),
 *         array('type'=>'file' , 'file'=>'efg.txt', 'path'=>'/'),
 *         array('type'=>'file' , 'file'=>'efg2.txt', 'path'=>'/admin/')
 *         );
 */

function router($fileName, $routerArr){
    if (is_array($routerArr)){
        foreach ($routerArr as $fileArr) {
            $fileBath = 'tmp/'.$fileName.'/'.$fileArr['file'];
            $destFileBath = '..'.$fileArr['path'].$fileArr['file'];
            $destBath = '..'.$fileArr['path'];
            if($fileArr['type']=='file'){
                copy($fileBath, $destFileBath);
            }elseif($fileArr['type']=='folder'){
                $zip = new ZipArchive;
                $res = $zip->open($fileBath);
                if ($res === TRUE) {
                    $zip->extractTo($destBath);
                    $zip->close();
                }
            }
        }
    }
}

/** //Example db.php file
 * $dbArr = array
 *         (
 *         "INSERT INTO `users` (`id`, `username`, `email`, `password`, `status`, `balance`, `date`) VALUES ('', 'tst', 'tst@abmegypt.com', 'tst', '1', '0', '2017-12-24 00:00:00');",
 *         "INSERT INTO `users` (`id`, `username`, `email`, `password`, `status`, `balance`, `date`) VALUES ('', 'tst', 'tst@abmegypt.com', 'tst', '1', '0', '2017-12-24 00:00:00');",
 *         "INSERT INTO `users` (`id`, `username`, `email`, `password`, `status`, `balance`, `date`) VALUES ('', 'tst', 'tst@abmegypt.com', 'tst', '1', '0', '2017-12-24 00:00:00');",
 *         );
 */

function insertDb($fileName, $dbArr){
    if (is_array($dbArr)){
        foreach ($dbArr as $db) {
            mysqli_query($conn , $db);
        }
    }
}


$sql = mysqli_query($conn , "SELECT * FROM `main` WHERE `id`='1'");
$data = mysqli_fetch_array($sql);
$version = $data['version'];
if ($data['updates']==1){
    $btnStatus = '';
}else{
    $btnStatus = 'disabled=""';
}
?>


        <div class="box box-primary">
            <div class="box-body">
                <h3 class="text-center">Current version is: V<?php echo $version ?></h3>
                <div class="row margin-top-40 margin-bottom-40">
                    <form action="update.php" method="post">
                        <div class="col-md-3 col-md-offset-3">
                            <input class="btn btn-warning btn-block flat" type="submit" name="check" value="Chekc for update!" />
                        </div>
                        <div class="col-md-3">
                            <input class="btn btn-success btn-block flat" type="submit" name="update" value="Update!"  <?php echo  $btnStatus; ?> />
                        </div>
                    </form>
                </div>
    </section><!-- /.content -->




<?php 
require 'includes/footer.php';
?>