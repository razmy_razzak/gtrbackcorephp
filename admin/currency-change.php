<?php
require 'includes/header.php';
?>
<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='dashboard.php']" ).parent().addClass( "active" );
});
</script>
<section class="content-header">
  <h1>
    <i class="ion ion-edit"></i> Change Currency
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li class="active">Main Information</li>
    <li class="active">Change Currency</li>
  </ol>
</section><!-- /.content Header-->
<section class="content">
  <div class="box box-primary">
    <div class="box-body text-center">
<?php
if(isset($_GET['currency_id'])&&isset($_GET['new_currency_id'])&& is_numeric( $_GET['currency_id'])&& is_numeric( $_GET['new_currency_id'])){
    $currency_id = mysqli_real_escape_string($conn , htmlspecialchars($_GET['currency_id']));
    $new_currency_id = mysqli_real_escape_string($conn , htmlspecialchars($_GET['new_currency_id']));

    $data = mysqli_query($conn , "SELECT code FROM `paypal_currencies` where id = $currency_id");
    $row = mysqli_fetch_array($data);
    $currency=$row['code'];

    $data = mysqli_query($conn , "SELECT code FROM `paypal_currencies` where id = $new_currency_id");
    $row = mysqli_fetch_array($data);
    $new_currency=$row['code'];

    $get = file_get_contents("https://finance.google.com/finance/converter?a=1&from=$currency&to=$new_currency");
    $get = explode("<span class=bld>",$get);
    $get = explode("</span>",$get[1]);
    $rate = preg_replace("/[^0-9\.]/", null, $get[0]);

    $get = file_get_contents("https://finance.google.com/finance/converter?a=1&from=USD&to=$new_currency");
    $get = explode("<span class=bld>",$get);
    $get = explode("</span>",$get[1]);
    $usdrate = preg_replace("/[^0-9\.]/", null, $get[0]);
}
if (isset($_POST)){
if(isset($_POST['yes'])){

$percentagedata = mysqli_query($conn , "SELECT `extra_price`,`service_price` FROM `main` ");
$percentagerow= mysqli_fetch_array($percentagedata);
   $extra_price = $percentagerow['extra_price']/100;
   $service_price = $percentagerow['service_price']/100;

$sql = "SELECT `id`,`panel_price`,`price` FROM `extras` ";
$data = mysqli_query($conn , $sql);
while ($row= mysqli_fetch_array($data)) {
    if ($row['price'] == "" OR $row['price'] == "0.00") {
        $price = $row['panel_price'] * $usdrate * (1+$extra_price) ;
    }else{
        $price = $row['price'] * $rate;
    }

    $sql = mysqli_query($conn , "UPDATE `extras` SET `price`='$price' WHERE `id`='".$row['id']."'");
    //test
    if(!$sql){
        die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysqli_error($conn) . "<div>");
    }
  
}

$sql = "SELECT `id`,`panel_price`,`price` FROM `service` ";
$data = mysqli_query($conn , $sql);
while ($row= mysqli_fetch_array($data)) {
    if ($row['price'] == "" || $row['price'] == "0.00") {
        $price = $row['panel_price'] * $usdrate * (1+$service_price) ;
    }else{
        $price = $row['price'] * $rate;
    }

    $sql = mysqli_query($conn , "UPDATE `service` SET `price`='$price' WHERE `id`='".$row['id']."'");
    //test
    if(!$sql){
        die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysqli_error($conn) . "<div>");
    }
  
}
echo "<div class=\"alert alert-success\" role=\"alert\">Services And Extras Successfully Updated!</div> </section>";
require 'includes/footer.php';
exit;
}elseif(isset($_POST['no'])){
    echo "<script type='text/javascript'> document.location = 'main.php'; </script>";

}
} 
?>

<?php
$ordersql = mysqli_query($conn , "SELECT * FROM `orders`");
$balancesql = mysqli_query($conn , "SELECT SUM(balance) as balance FROM `users`");
$row = mysqli_fetch_array($balancesql);
$balance = $row['balance'];
if (mysqli_num_rows($ordersql) > 0 || $balance > 0 ) {
?>
        <h3> You need to change service prices and extras prices manually</h3>  
        <a href="main.php"> << Back To Main Infromation </a>
<?php
}else{
?>
        <h3> WE WILL CHANGE THE PRICES IN THE DATABASE FROM 
            <span class="label label-info"><?php echo $currency; ?></span> TO 
            <span class="label label-info"><?php echo $new_currency; ?></span><br>
            WITH RATE = <b><span class="label label-primary"><?php echo $rate; ?></span></b>
        </h3>

            <h3>DO WANT TO CONTINUE OR YOU WILL KEEP THE SAME PIECES ??</h3>
        <br><br>
        <form action='' method="post">
          <div class="row">
            <div class="col-md-3 col-md-offset-3">
                <button type="submit" name='yes' class="btn btn-danger btn-block">YES</button>   
            </div>
            <div class="col-md-3">
                <button type="submit" name='no' class="btn btn-success  btn-block">NO</button>   
            </div>
          </div>
        </form>
        <br><br>
<?php } ?>
    </div>
   </div>
</section>

 <?php 
require 'includes/footer.php';
?>