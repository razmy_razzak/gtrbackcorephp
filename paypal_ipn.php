<?php
session_start();

require 'includes/connect.php';
require 'includes/paypal_class.php';
require 'includes/paypal_config.php';

$p = new paypal_class;
$p->paypal_url = PAYPAL_URL;

if ($p->validate_ipn()) {
    $to = ADMIN_EMAIL;
    $transaction_id = $p->ipn_data['txn_id'];
    $ipnData = $p->ipn_data;

    $uid =  $p->ipn_data['custom'];
    $amount = $p->ipn_data['mc_gross'];
    $fullData = '';

    /*
    foreach ($ipnData as $key => $value) {
            $fullData.= $key;
            $fullData.= "->";
            $fullData.= $value;
            $fullData.= " - ";
    }
    */
    $sql = mysqli_query($conn , "SELECT `username` FROM `users` WHERE `id` = '$uid' ");
    $userName = mysqli_fetch_array($sql);
    $userName = $userName[0];
    if (is_numeric($amount) && $amount > 0) {
        $subject = "SEO panel new payment";
        $body ="$userName ID: $uid just made a payment, \n- Transaction ID :$transaction_id \n- Amount: $amount";
        $dateTime = gmdate('ymdHis');
        mail($to, $subject, $body);
        
        mysqli_query($conn , "INSERT INTO `payment` (`uid`, `txn_id`, `amount`, `method`, `date`) VALUES ('$uid', '$transaction_id', '$amount', '1', $dateTime)");
        }
    else {
        $subject = "SEO panel error payment";
        $body ="$userName ID: $uid made wrong payment, \n- Transaction ID :$transaction_id \n- Amount: $amount";
        mail($to, $subject, $body);
    }
}
