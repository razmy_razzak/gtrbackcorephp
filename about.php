<?php

?>

<!-- services
    ================================================== -->
<section id='services' class="s-services light-gray">

    <div class="row section-header" data-aos="fade-up">
        <div class="col-full">
            <h1 class="display-1"><?php echo $title ?></h1>
        </div>
    </div> <!-- end section-header -->

    <div class="row" data-aos="fade-up">
        <div class="col-full">
            <p class="lead">
                <?php  echo $content?>
            </p>
        </div>
    </div> <!-- end about-desc -->

</section> <!-- end s-services -->