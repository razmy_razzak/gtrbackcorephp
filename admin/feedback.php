<?php
require_once 'includes/header.php';
require_once './includes/connect.php';
?>
    <script>
        $(document).ready(function () {
            $(".sidebar-menu li a[href^='feedback.php']").parent().addClass("active");
        });
    </script>
<section class="content-header">
    <h1>
        <i class="fa fa-users"></i> Send Email
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="index.php">
                <i class="fa fa-dashboard"></i>Dashboard</a>
        </li>
        <li class="active">Send Feedback</li>
    </ol>
</section>
<!-- /.content Header-->
<!-- Content -->
<section class="content">
        <?php
if(isset($_POST['send'])) {

    $type = mysqli_real_escape_string($conn , htmlspecialchars($_POST['type']));
    $message = mysqli_real_escape_string($conn , htmlspecialchars($_POST['message'])); // required


    $to = "sps-feedback@seoestore.net";//email to send to 
  $subject = 'message from sps - ' . $site_name;
  $message = "Sender Name: " . $site_name . "<br>for: " . $type . "<br><br>Message:<br>" . $message;
  $headers = 'From: ' . $email . "\r\n" .
      'Reply-To: ' . $email . "\r\n".
      'Content-Type: text/html; charset=UTF-8';

  $send = mail($to, $subject, $message, $headers);
  if($send){
    echo "<div class=\"alert alert-success\" role=\"alert\">Thank you, your feedback successfully sent!</div>";
  }
  else {
    echo "<div class=\"alert alert-danger\" role=\"alert\">Error while sending the message, try again!</div>";
  }

}
?>
        <div class="box box-primary">
            <div class="box-body">
                <form action="" method="post">
                    <div class="form-group ">
                        <label for="message-type" class="form-label">Select The Type Of The Email</label>
                        <select class="form-control" name="type" required>
                            <option value="" selected>Select The Type Of The Email</option>
                            <option value="report_bug">Report Bug</option>
                            <option value="send_feedback">Send Feedback</option>
                            <option value="request_features">request Features</option>
                        </select>
                    </div>
                    <div class="form-group ">
                        <label for="message" class="form-label">Message</label>
                        <textarea type="text" class="form-control" id="editor1" name="message" cols="30" rows="10" required></textarea>
                    </div>
                    <div class="form-group ">
                        <div class="offset-sm-2 col-sm-10">
                            <button type="submit" name='send' class="btn btn-primary">Send</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
</section>
    <?php 
require 'includes/footer.php';
?>