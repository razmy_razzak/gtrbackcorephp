<?php
require"connect.php";
$query = mysqli_query($conn ,"SELECT * FROM `lang` WHERE 1");
while($data =mysqli_fetch_array($query)){
	$var = $data['var'];
	if($data['updated']){
		$lang[$var] = $data['updated'];
	}else{
		$lang[$var] = $data['original'];
	}
}

function lang($x,$echo=1){
	global $lang;
	if($echo==1){
		echo $lang[$x];
	}else{
		return $lang[$x];
	}
}
