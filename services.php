<?php
require_once 'controllers/Services.php';
$test = new Services();
$data = $test->getAllServices();
$title
?>

<!-- services
    ================================================== -->
<section id='services' class="s-services light-gray">

    <div class="row section-header" data-aos="fade-up">
        <div class="col-full">
            <h3 class="subhead">services page</h3>
            <h1 class="display-1"><?php echo $title ?></h1>
        </div>
    </div> <!-- end section-header -->

    <div class="row" data-aos="fade-up">
        <div class="col-full">
            <p class="lead">
                <?php  echo $content?>
            </p>
        </div>
    </div> <!-- end about-desc -->

    <div class="row services-list block-1-3 block-m-1-2 block-tab-full">
        <?php foreach ($data as $product) {?>
        <div class="hidden-md hidden-sm hidden-xs col-block service-item " data-aos="fade-up">
            <a href="custom-page.php?id=2&productId=<?php echo $product['id'] ?>">
                <div class="service-text">
                    <h3 class="h4 product_heading"><?php echo $product['description'] ?></h3>
                    <div class="product" style="background: url(images/products/product_bg.png) no-repeat;  height: 150px">
                        <div class="left">
                            <i class="<?php echo $product['icon'] ?>"></i>
                        </div>
                        <div class="right">
                            <p class="product_title"><?php echo strlen($product['description']) > 40 ? substr($product['description'],0,40)."..." : $product['description'];?></p>
                        </div>
                    </div>
                    <p class="price">Price: $<?php echo  ($product['price'] != 0 )? $product['price']/100: $product['panel_price'] ?></p>
                </div>
            </a>
        </div>
        <?php }?>



    </div> <!-- end services-list -->

</section> <!-- end s-services -->
