<?php $user_id = isset($_SESSION['id'])? $_SESSION['id'] : 0; ?>

<!-- home
================================================== -->
<section id="home" class="s-home target-section" data-parallax="scroll" data-image-src="images/hero-bg.jpg" data-natural-width=3000 data-natural-height=2000 data-position-y=top>

    <div class="shadow-overlay"></div>

    <div class="home-content">

        <div class="row home-content__main">
            <h1>
                Search Engine <br>
               Optimization.
            </h1>

            <p>
                Rank your website #1 in Google
            </p>

        </div> <!-- end home-content__main -->

    </div> <!-- end home-content -->
    <ul class="home-sidelinks">
        <?php if(isset($_SESSION['id']) && $_SESSION['id'] != '' ){?>
        <li class="username_home" ><a >Username: <?php echo  $_SESSION['username'];?></a></li>
        <?php }?>

        <li><a class="smoothscroll" href="#about">About<span>who we are</span></a></li>
        <li><a class="smoothscroll" href="#services">Our Services<span>what we do</span></a></li>
        <li><a  class="smoothscroll" href="#contact">Contact<span>get in touch</span></a></li>
        <li><a  class="" href="custom-page.php?id=1">Service<span>buy services</span></a></li>
        <?php if(isset($_SESSION['id']) && $_SESSION['id'] != '' ){?>
        <li><a  class="" href="logout.php">Logout<span>logout here</span></a></li>
        <?php } else { ?>
        <li><a  class="" id="login_home" href="#">Login<span>login here</span></a></li>
        <li class=""><a href="register.php" title="home">Register<span>Register here</span></a></li>
        <?php } ?>
    </ul> <!-- end home-sidelinks -->

    <ul class="home-social">
        <li class="home-social-title">Follow Us</li>
        <li><a href="#0">
                <i class="fab fa-facebook"></i>
                <span class="home-social-text">Facebook</span>
            </a></li>
        <li><a href="#0">
                <i class="fab fa-twitter"></i>
                <span class="home-social-text">Twitter</span>
            </a></li>
        <li><a href="#0">
                <i class="fab fa-linkedin"></i>
                <span class="home-social-text">LinkedIn</span>
            </a></li>
    </ul> <!-- end home-social -->

    <a href="#about" class="home-scroll smoothscroll">
        <span class="home-scroll__text">Scroll Down</span>
        <span class="home-scroll__icon"></span>
    </a> <!-- end home-scroll -->

</section> <!-- end s-home -->

<?php include 'login_model.php'?>

<script>
    var user_id='<?php echo $user_id; ?>';
    console.log(user_id);
</script>
