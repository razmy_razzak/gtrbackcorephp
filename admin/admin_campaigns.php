<?php
require_once 'includes/header.php';
require_once 'AdminController/Campaign.php';
$campaign = new Campaign();
$allCamps = $campaign->getAllCampapins();
$allServices = $campaign->getAllServices();
?>
    <script>
        $(document).ready(function(){
            $( ".sidebar-menu li a[href^='custom-css-manage.php']" ).parent().addClass( "active" );
        });
    </script>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> gdgfds
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="index.php">
                    <i class="fa fa-dashboard"></i>Dashboard</a>
            </li>
            <li>Custom CSS</li>
            <li class="active">Manage Custom CSS</li>
        </ol>
    </section>
    <!-- /.content Header-->
    <!-- Content -->
    <section class="content">
    <div class="box box-primary">
        <div class="box-body">
            <a class="btn btn-primary" href="campain_form.php" target="_blank">Add</a>
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>List order</th>
                    <th>Price tag</th>
                    <th>Services and Extras</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                   foreach ( $allCamps as $camp ) {
                       $children = $campaign->getChildServicesById( $camp['id'] );
                    ?>
                    <tr>
                    <td><?php echo $camp['id']?></td>
                    <td><?php echo $camp['ordernum']?></td>
                    <td>$<?php echo $camp['pricerang'] ?></td>
                    <td>
                        <ul>
                            <?php if($children){foreach ( $children as $child){
                                $service = $campaign->getServiceDataById( $child['service_id']);
                                $extras = $campaign->getExtrasById( $child['extra_id']);
                                ?>
                            <li>
                                <?php echo $service['description']?>
                            <ul><li><?php echo $extras['description']?></li></ul>
                            </li>
                            <?php }} ?>
                        </ul>
                    </td>
                    <td>
                        <button type="button" class="btn btn-primary"  data-toggle="modal" data-target="#exampleModal<?php echo $camp['id'] ?>">
                            <i class="fa fa-plus"></i>
                        </button>
                        <?php include 'add_cmapd_model.php'?>
                        <a href="doDeleteCmps.php?service_id=<?php echo $camp['id']?>" class="btn btn-danger"><i class="fa fa-recycle"></i></a>
                    </td>
                    </tr>
                <?php  } ?>

                </tbody>
            </table>
            <hr>
        </div>
    </div>
    </section>
<?php
require 'includes/footer.php';
?>