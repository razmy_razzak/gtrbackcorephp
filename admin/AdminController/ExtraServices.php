<?php

/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 02/07/2018
 * Time: 13:27
 */
class ExtraServices
{
    private $conn;
    private $serPricePlus;
    private $extraPricePlus;
    public function __construct()
    {
        require 'includes/DbConnect.php';
        $DBCon = new DbConnect();
        $this->conn = $DBCon->getdbconnect();
        $data = mysqli_fetch_array(mysqli_query($this->conn,"SELECT * FROM `main` WHERE `id`='1'"));
        $this->serPricePlus = 1 +$data['service_price']/100;
        $this->extraPricePlus = 1 + $data['extra_price']/100;
    }

    public function getExtrServById( array $ids ){
        if($ids){
            $data = [];
            foreach ($ids as $id){
                $result= mysqli_query($this->conn , "SELECT * FROM `extras` WHERE `id`='$id'");
                $row = mysqli_fetch_assoc($result);
                $panel_price = $row['panel_price']/100;
                $row['panel_price'] = ($row['price'] !=0 )? $row['price']/100 : $panel_price*$this->extraPricePlus;
                $data[] = $row;
            }
            return $data;
        }
        return 503;
    }


    public function getServiceById( $id ){
        $result= mysqli_query($this->conn , "SELECT * FROM `service` WHERE `id` = $id");
        $row = mysqli_fetch_assoc($result);
        $panel_price = $row['panel_price']/100;
        $row['panel_price'] = ($row['price'] !=0 )? $row['price']/100 : $panel_price*$this->serPricePlus;
        return $row;
    }



}