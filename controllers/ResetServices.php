<?php

/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 28/06/2018
 * Time: 12:27
 */
class ResetServices
{
    private $conn;
    public function __construct()
    {
        require 'includes/DbConnect.php';
        $DBCon = new DbConnect();
        $this->conn = $DBCon->getdbconnect();

    }

    public function createPasswordReset( $data ){
        if($data){
            $reset_hash = $data['hash'];
            $email = $data['email'];
            $sql = "INSERT INTO `user_reset`(`hash`,`email`)VALUES ( '$reset_hash','$email')";
            $retval = mysqli_query($this->conn, $sql );
            return $retval;
        }
    }

    public function getUserDataByHash( $hash ){
        if( $hash ){
            $data = mysqli_query($this->conn , "SELECT * FROM `user_reset` WHERE `hash` ='$hash'");
            if($data){
                $row = mysqli_fetch_array($data);
                if( $row ){
                    $user_email = $row['email'];
                    $data_user = mysqli_query($this->conn , "SELECT * FROM `users` WHERE `email` ='$user_email'");
                    $row_user = mysqli_fetch_array($data_user);
                    return $row_user;
                }
                else{
                    return null;
                }
            }
            else{
                return false;
            }
        }
    }

    public function updatePassword( $user_id, $password ){
        if($user_id){
            $sql =  "UPDATE users SET passsword='$password' WHERE id='$user_id'";
            $retval = mysqli_query($this->conn, $sql );
            return $retval;
        }
    }

    public function deleteHash( $hash ){
        if($hash){
            $sql =  "DELETE FROM user_reset WHERE hash='$hash'";
            $retval = mysqli_query($this->conn, $sql );
            return $retval;
        }
    }
}