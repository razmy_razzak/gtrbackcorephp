<?php
require_once 'controllers/RatingServices.php';

$data = [
    'uid' => $_POST['uid'],
    'reviewRating' => $_POST['reviewRating'],
];

$rating = new RatingServices();
$result = $rating->createRating( $data );
if( $result['tag'] == 100) {
    //not enough fund
    echo json_encode(array('success' => true ));
}
else{
    echo json_encode(array('error' => 'Something went wrong' ));
}