<?php
require_once 'header.php';
$custom_pages = mysqli_query($conn ,"SELECT * FROM `custom_pages` WHERE id = $id");
while ($data= mysqli_fetch_array($custom_pages)) {
$title=$data['title'];
$content =$data['content'];
}
?>
<div class="container">
    <div class="row">
        <div class='headline'>
            <h3>
                <?php echo $title; ?>
            </h3>
        </div>
        <p>
            <?php echo $content; ?>
        </p>
    </div>
</div>

<?php
require_once 'footer.php';
?>