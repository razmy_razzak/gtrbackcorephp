<?php 
require 'includes/header.php';
?>

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#home']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='home-seo.php']" ).parent().addClass( "active" );
});
</script>


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-line-chart"></i> SEO details
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li>Home page</li>
    <li class="active">SEO details</li>

  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">

<?php
// Update
if(isset($_POST['submit'])) {
  // get fields
  $title = mysql_real_escape_string(htmlspecialchars($_POST['title']));
  $kw = mysql_real_escape_string(htmlspecialchars($_POST['kw']));
  $description = mysql_real_escape_string(htmlspecialchars($_POST['description']));

  //insert data
  $sql = mysql_query("UPDATE `seo` SET
                      `title`='$title',
                      `kw`='$kw',
                      `description`='$description'
                      WHERE `page`='home'");
  //test
  if(!$sql){
      die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysql_error() . "<div>");
  }
  echo "<div class=\"alert alert-success\" role=\"alert\">SEO Details Successfully Updated!</div>";
}

/** Reset
if(isset($_GET['reset'])) {
  $sql = mysql_query("UPDATE `seo` SET 
                    `title`='Welcome to Website EG',
                    `kw`='Website EG, website-eg',
                    `description`='Nulla facilisi. Donec eget velit quis elit eleifend faucibus. Nulla sit amet ex vitae erat consequat tincidunt. Donec eget felis ac justo consectetur congue mollis in dolor. Sed aliquet est nec egestas mollis. Vivamus euismod nisi quis dapibus laoreet. Integer vestibulum neque a iaculis sodales. Proin dui nisl, finibus eu sodales sed, eleifend quis justo.'
                    WHERE `page`='home'");
  //test
  if(!$sql){
      die("<div class=\"alert alert-danger\" role=\"alert\">Could not reset data: " . mysql_error() . "<div>");
  }
  echo "<div class=\"alert alert-success\" role=\"alert\">SEO Details Successfully Reset!</div>";
}
**/
// Get data
$data = mysql_query("SELECT * FROM `seo` WHERE `page`='home'");
$row = mysql_fetch_array($data);
  $title = $row['title'];
  $kw = $row['kw'];
  $description = $row['description'];
?>

  <div class="box box-primary">
    <div class="box-body">

      <form method="post" action="home-seo.php">
        <div class="form-group">
          <label for="title">Page title</label>
          <input type="text" class="form-control" id="title" name="title" value="<?php echo $title ?>" placeholder="Page title...">
        </div>

        <div class="form-group">
          <label for="kw">Meta keywords</label> <span><small>(Comma separated)</small></span>
          <input type="text" class="form-control" id="kw" name="kw" value="<?php echo $kw ?>" placeholder="Meta keywords...">
        </div>

        <div class="form-group">
          <label for="description">Meta description</label>
          <textarea class="form-control" type="text" rows="4" id="description" name="description" placeholder="Meta description..." ><?php echo $description ?></textarea>

        </div>
            
        <input type="submit" class="btn btn-primary flat margin-top-20" name="submit" value="UPDATE">
        
        </form>
  	</div>
  </div>
</section><!-- /.content -->



<?php 
require 'includes/footer.php';
?>