<?php
require_once 'controllers/Services.php';
$service = new Services();
$user_id = isset($_SESSION['id'])? $_SESSION['id'] : 0;
$artical_cats = $service->getArtical_cats();
$pricetags = $service->getCampspriceTags();



?>

<!-- services
    ================================================== -->
<section id='services' class="s-services light-gray">
    <div class="row section-header" data-aos="fade-up">
        <div class="col-full">
            <h1 class="display-1"><?php echo $title ?></h1>
        </div>
    </div> <!-- end section-header -->

    <div class="row" data-aos="fade-up">
        <div class="col-full">
            <p class="lead">
                <?php  echo $content?>
            </p>

        </div>
    </div> <!-- end about-desc -->

    <div class="row services-list block-1-2 block-m-1-2 ">

        <div class="col-2-3 service-item " data-aos="fade-up">
            <div id="order" class="service-text form_card" style="display: block" >
                <h3 class="h4 product_heading">ORDER</h3>
                <form id="campain_form" class="product_form" action="" >
                    <input name="uid" type="hidden" value="<?php echo $user_id;?>">
                    <label>Select your Budget</label>
                    <div class="range_slector">
                        <input name="cmap_range" id="ex19" type="text"
                               data-provide="slider"
                               data-slider-ticks="<?php echo json_encode($pricetags['data']); ?>"
                               data-slider-ticks-labels='<?php echo json_encode($pricetags['label']); ?>'
                               data-slider-min="1"
                               data-slider-value="1"
                               data-slider-tooltip="hide" />
                    </div>
                        <span class="sample"> eg: www.link.com, www.link2.com</span>
                        <textarea name="links" placeholder="Enter Links with comma separated "
                                  rows="3" cols="33"
                                  wrap="hard"></textarea>
                        <span class="sample"> eg: seo, google, etc</span>
                        <textarea name="keywords" placeholder="Enter Keyword with comma separated "
                                  rows="3" cols="33"
                                  wrap="hard"></textarea>
                        <span class="sample"> Optional</span>
                        <select class="form-control select2" id="article" name="article" data-placeholder="Select Article Category">
                            <option value="">- Select Article Category -</option>
                            <option value="0">- Auto detect article category -</option>
                            <?php if($artical_cats){
                                foreach ( $artical_cats as $artical_cat){?>
                                    <option value="<?php echo $artical_cat['code']?>"><?php echo $artical_cat['name']?></option>
                                <?php } } ?>
                        </select>
                    <input type="submit" class="order_btn" name="subscribe" value="Submit">
                </form>
            </div>
        </div>
        <div class="col-1-3 service-item " data-aos="fade-up">
            <div id="campain" class="service-text form_card" style="display: none" >
                <ul id="cmp_services" class="camp_liststyle">

                </ul>
            </div>
        </div>

    </div>




</section> <!-- end s-services -->

<?php include 'login_model.php'?>
<?php include 'success_msg.php'?>




<script>
    var user_id='<?php echo $user_id; ?>';

</script>

