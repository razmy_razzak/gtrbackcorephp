<?php 
require 'includes/header.php';


// Get data
$data = mysqli_query($conn , "SELECT * FROM `main` WHERE `id`='1'");
$row = mysqli_fetch_array($data);
  $site_name = $row['site_name'];
  $service_price = $row['service_price'];
  $extra_price = $row['extra_price'];
  $color = $row['color'];
  $analytics = $row['analytics'];
  $currency_id=$row['currency_id'];

  $data = mysqli_query($conn , "SELECT * FROM `paypal_currencies`");
?>

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='main.php']" ).parent().addClass( "active" );
});
</script>

<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="../plugins/colorpicker/bootstrap-colorpicker.min.css">

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-list-alt"></i> Main Information
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li class="active">Main Information</li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">

<?php
// Update
if(isset($_POST['submit'])) {
  // get fields
  $site_name = mysqli_real_escape_string($conn , htmlspecialchars($_POST['site_name']));
  $color = mysqli_real_escape_string($conn , htmlspecialchars($_POST['color']));
  $analytics = mysqli_real_escape_string($conn , htmlspecialchars($_POST['analytics']));
  $service_price = mysqli_real_escape_string($conn , htmlspecialchars($_POST['service']));
  $extra_price = mysqli_real_escape_string($conn , htmlspecialchars($_POST['extra']));
  $new_currency_id = mysqli_real_escape_string($conn , htmlspecialchars($_POST['currency']));
  //insert data
  $sql = mysqli_query($conn , "UPDATE `main` SET
                      `site_name`='$site_name',
                      `service_price`='$service_price',
                      `extra_price`='$extra_price',
                      `color`='$color',
                      `currency_id`='$new_currency_id',
                      `analytics`='$analytics'
                      WHERE `id`='1'");
  //test
  if(!$sql){
      die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysqli_error($conn) . "<div>");
  }
  echo "<div class=\"alert alert-success\" role=\"alert\">Main Information Successfully Updated!</div>";

  if($currency_id != $new_currency_id){
    echo "<script type='text/javascript'> document.location = 'currency-change.php?currency_id=$currency_id&new_currency_id=$new_currency_id'; </script>";
  }

}
?>

  <div class="box box-primary">
    <div class="box-body">
      <form method="post" action="main.php">
        <div class="form-group">
          <label for="site_name">Site Name</label>
          <input type="text" class="form-control" id="site_name" name="site_name" value="<?php echo $site_name ?>" placeholder="Site Name...">
        </div>
        <div class="form-group">
            <label for="color">Main color</label>
            <div class="input-group my-colorpicker colorpicker-element">
              <div class="input-group-addon">
                <i style="background-color: rgb(255, 0, 0);"></i>
              </div>
              <input type="text" class="form-control" id="color" placeholder="Color" name="color" value="<?php echo $color; ?>" />
            </div><!-- /.input group -->
        </div>
        <div class="form-group">
          <label for="analytics">Google analytics ID</label>
          <input type="text" class="form-control" id="analytics" name="analytics" value="<?php echo $analytics; ?>" placeholder="UA-XXXX-Y">
        </div>
        <div class="form-group">
          <label for="service_price">Services profit (percentage)</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-percent"></i></span>
            <input type="text" class="form-control" id="service_price" name="service" value="<?php echo $service_price ?>" placeholder="Service percentage add...">
          </div>
        </div>
        <div class="form-group">
          <label for="extra_price">Extra profit (percentage)</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-percent"></i></span>
            <input type="text" class="form-control" id="extra_price" name="extra" value="<?php echo $extra_price ?>" placeholder="Extra percentage add...">
          </div>
        </div>
        <div class="form-group">
          <label for="currency">Currency</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-money"></i></span>
            <select name='currency' id='currency'  class="form-control">
              <?php  
                  while ($row= mysqli_fetch_array($data)) {
                ?>
              <option value="<?php echo $row['id']?>"<?php if ($currency_id == $row['id']) echo "selected"; ?> ><?php echo $row['name']." (".$row['code'].")" ?></option>
                  <?php }?>
            </select>
          </div>
        </div>

        <input class="btn btn-primary flat" type="submit" name="submit" value="UPDATE">
      </form>
    </div>
  </div>
</section><!-- /.content -->

<!-- bootstrap color picker -->
<script src="../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<script>
  $(function () {
    $(".my-colorpicker").colorpicker();
  });
</script>

<?php 
require 'includes/footer.php';
?>