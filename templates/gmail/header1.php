
<!DOCTYPE html>
<!--[if IE 7]> <html lang="en" class="ie7"> <![endif]-->  
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title><?php echo $title ?></title>

    <!-- Meta -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="<?php echo $description ?>" />
<?php
    if (isset($kw)) {
        echo '<meta name="keywords" content="'.$kw.'" />';
    }
?>
    <meta name="author" content="SEOeStore" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />

    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

    <!-- font-awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">    

    <!-- custom css -->
    <link rel="stylesheet" href="dist/css/style-gmail.css" />
    <link rel="stylesheet" href="dist/css/themes/color.php" id="style_color?v=0" />
    <!-- JS Global Compulsory -->     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- JS Implementing Plugins -->
    <script type="text/javascript" src="plugins/back-to-top.js"></script>
    <script type="text/javascript" src="plugins/jquery.parallax.js"></script>
    <!-- JS Customization -->
    <script type="text/javascript" src="dist/js/custom.js"></script>
    <!-- JS Page Level -->           
    <!--[if lt IE 9]>
        <script src="assets/js/respond.js"></script>
    <![endif]-->
<?php
if ($dir == 'rtl') {
?>
<style type="text/css">

* {
  direction: rtl !important;
}
.social-icons li , .service i{
  float:right !important;
}
    .right-top {
        left:0 !important;
    }

    .header-logo {
        float: right !important;
    }

</style>
<?php } ?></head> 
<body>
<!--=== Header ===-->
<div class="header">               
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-xs-2 margin-bottom-10 margin-top-20 header-logo" >
            <!-- Logo -->
            <a class="logo" href="index.php" >
            <?php if(empty($logo)){
                echo '<h1>'.$site_name.'</h1>';
            }else{
                echo '<img class="img-responsive" src="'. $logo . '" alt="Logo" />';
            }?>
            </a>
            </div>
            
            <div class="col-xs-10">
                <div class="right-top">
                    <a class="btn-top" href='#'>Services2</a>

                <?php
                    if(mysqli_num_rows($custom_pages) > 0 ){
                    while ($data= mysqli_fetch_array($custom_pages)) {
               ?>
                            <a class="btn-top" href='custom-page.php?id=<?php echo $data['id']; ?>'>
                                <?php echo $data['link_text']?>
                            </a>
                <?php
                        }
                    }
                ?>
            <?php if(isset($_SESSION['username'])) {
            ?>
                    <a class="btn-top btn-register" href="dashboard.php"><?php lang('dashboard'); ?></a>
            <?php
            }else{
            ?>
                    <a class="btn-top btn-defualt" href="login.php"><?php lang('sign'); ?></a>
                    <a class="btn-top btn-register" href="register.php"><?php lang('create'); ?></a>
            <?php    
            }
            ?>
                </div>
            </div><!-- /navbar -->
        </div>

    </div><!-- /container -->               
</div><!--/header -->      
<!--=== End Header ===-->
