<?php


class Services
{
    private $conn;
    public $serverError;
    private $serPricePlus;
    private $extraPricePlus;
    public function __construct()
    {
        require 'includes/DbConnect.php';
        $DBCon = new DbConnect();
        $this->conn = $DBCon->getdbconnect();
        $this->serverError = json_encode(array("error"=>'Something Went Wrong Please Contact admin'));
        $data = mysqli_fetch_array(mysqli_query($this->conn,"SELECT * FROM `main` WHERE `id`='1'"));
        $this->serPricePlus = 1 +$data['service_price']/100;
        $this->extraPricePlus = 1 + $data['extra_price']/100;

    }

    public function getAllServices(){
        $data = mysqli_fetch_array(mysqli_query($this->conn,"SELECT * FROM `main` WHERE `id`='1'"));
        $serPricePlus = $data['service_price'];
        $extraPricePlus = $data['extra_price'];
        $sPercentage = 1 + $serPricePlus/100;
        $ePercentage = 1 + $extraPricePlus/100;
        $result= mysqli_query($this->conn , "SELECT * FROM `service` WHERE `status` ='1'");
        $data = array();
        while ($row = mysqli_fetch_assoc($result)){
            $panel_price = $row['panel_price']/100;
            $row['panel_price'] = ($row['price'] !=0 )? $row['price']/100:$panel_price*$sPercentage;
            $data[] = $row;
        }
        return $data;
    }

    public function getServiceById( $id ){
        $data = mysqli_fetch_array(mysqli_query($this->conn,"SELECT * FROM `main` WHERE `id`='1'"));
        $serPricePlus = $data['service_price'];
        $extraPricePlus = $data['extra_price'];
        $sPercentage = 1 + $serPricePlus/100;
        $result= mysqli_query($this->conn , "SELECT * FROM `service` WHERE `id` = $id");
        $row = mysqli_fetch_assoc($result);
        $panel_price = $row['panel_price']/100;
        $row['panel_price'] = ($row['price'] !=0 )? $row['price']/100 : $panel_price*$sPercentage;
        return $row;
    }

    public function getServicesMatches( $word ){
        $data = mysqli_fetch_array(mysqli_query($this->conn,"SELECT * FROM `main` WHERE `id`='1'"));
        $serPricePlus = $data['service_price'];
        $result= mysqli_query($this->conn , "SELECT * FROM `service` WHERE `code` LIKE '".$word."%'");
        $data = array();
        while ($row = mysqli_fetch_assoc($result)){
            $panel_price = $row['panel_price']/100;
            $row['panel_price'] = ($row['price'] !=0 )? $row['price']/100 : $panel_price*$serPricePlus;
            $data[] = $row;
        }
        return $data;
    }

    public function apiOrder( $data_one ){
        $admin = mysqli_query($this->conn,"SELECT `email`,`api_key` FROM `admins` WHERE `id` = 1");
        $data = mysqli_fetch_array($admin);
        $url = 'http://panel.seoestore.net/action/api.php';
        $api_key = $data['api_key'];
        $email = $data['email'];
        $url = 'http://panel.seoestore.net/action/api.php';
        unset($orde);
        $orde = Array(
            'api_key'=>$api_key,
            'email'=>$email,
            'action'=>'order',
            'service'=>$data_one['service_id'],
            'quantity'=>$data_one['quantity'],
            'extras'=>$data_one['extra'],
            'links'=>$data_one['links'],
            'keywords'=>$data_one['keywords'],
            'article'=>$data_one['article'],
        );
        //handle Data for API
        if (is_array($orde)) {
            if(empty($data_one['extra'])){
                unset($orde['extras']);
            }
            if (is_array($orde)) {
                $dataString = http_build_query($orde, '', '&');
            }
        }
        //submit API order
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        $result = curl_exec($ch);

        curl_close($ch);
        //getting PHP result array
        $result = json_decode($result, true);
        //check if order success
        if ( ! isset($result['order'])) {
            $result['order'] = null;
            return 000;

        }else {
            $oid = $result['order'];
            return $oid;
        }

    }

    public function createOrder( $data ){
        $oid = 0;
        $price = 0;
        if($data){
            $service_id = $data['service_id'];
            $check_services = mysqli_fetch_array(mysqli_query($this->conn,"SELECT * FROM `service` WHERE `id`=$service_id"));
            if($data['quantity'] < $check_services['min_qty']){
                return ['tag' => 300, 'qtys' =>$check_services['min_qty'] ];
            }
            $price = ($data['price'] *$data['quantity']);
            if($data['extra']){
                $extras_ids = explode(",", $data['extra']);
                // we have to recalculate the extra price, coz we only getting ids from the form post
                foreach ( $extras_ids as  $extras_id){
                    $extra_price = mysqli_fetch_array(mysqli_query($this->conn,"SELECT `panel_price`,`price` FROM `extras` WHERE `id`=$extras_id"));
                    $ext_price = ($extra_price['price'] !=0 )? $extra_price['price']/100 : $extra_price['panel_price']/100*$this->extraPricePlus;
                    $price = (float)$price + (float)$ext_price;
                }

            }
            $amount  =(float) $this->checkUserBalance( $data['uid'] );
            if( $amount >= $price ){
                $result_api = $this->apiOrder( $data );
                if($result_api != 000){
                    $oid = $result_api;
                }
                else{
                    //api error
                    return ['tag' => 409];
                }
            }
            else{
                return ['tag' => 219];
            }
            if($oid != 0){
                $uid = $data['uid'];
                $date = $data['date'];
                $keywords =  $data['keywords'];
                $links = $data['links'];
                $service = $data['service_id'];
                $extras = $data['extra'];
                $quantity = $data['quantity'];
                $refId = $data['refId'];
                $price = $price*100;
                $sql = "INSERT INTO `orders`".
                    "(`oid`,`uid`,`price`,`date`,`keywords`,`links`,`service_id`,`extras`,`quantity`,`refId`)".
                    "VALUES ( '$oid','$uid','$price','$date','$keywords','$links','$service','$extras','$quantity','$refId')";
                $retval = mysqli_query($this->conn, $sql );
                return ['tag' => 200,'oid'=>$oid];
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }

    }
    public function checkUserBalance($user_id ){
        if( $user_id ){
            $data = mysqli_query($this->conn ,"SELECT SUM(amount) as amt FROM `payment` WHERE uid='$user_id'");
            $row2 = mysqli_fetch_assoc(mysqli_query($this->conn,"SELECT SUM(price) as price FROM `orders` WHERE uid='$user_id'"));
            $row = mysqli_fetch_assoc($data);
            $price = (isset($row2['price'])?$row2['price'] :0)/100;
            $amount = isset($row['amt'])?$row['amt'] :0;
            $balence = $amount - $price;
            if($balence > 0){
                return $balence;
            }
            else{
                return -1;
            }
        }else{
            return $this->serverError;
        }
    }

    public function getExtras( array $ids ){
        $data = mysqli_fetch_array(mysqli_query($this->conn,"SELECT * FROM `main` WHERE `id`='1'"));
        $serPricePlus = $data['service_price'];
        $extraPricePlus = $data['extra_price'];
        $sPercentage = 1 + $serPricePlus/100;
        if($ids){
            $data = [];
            foreach ($ids as $id){
                $result= mysqli_query($this->conn , "SELECT * FROM `extras` WHERE `id`='$id'");
                $row = mysqli_fetch_assoc($result);
                $panel_price = $row['panel_price']/100;
                $row['panel_price'] = ($row['price'] !=0 )? $row['price']/100 : $panel_price*$extraPricePlus;
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getArtical_cats(){
        $result= mysqli_query($this->conn , "SELECT * FROM `article_cats`");
        $data = array();
        while ($row = mysqli_fetch_assoc($result)){
            $data[] = $row;
        }
        return $data;
    }

    public function getCampspriceTags(){
        $result= mysqli_query($this->conn , "SELECT * FROM `campain` ");
        $data = array();
        $label = [];
        while ($row = mysqli_fetch_assoc($result)){
            $label[] = '$'.(int)$row['pricerang'];
            $data[]= (int)$row['id'];
        }
        return ['label'=>$label, 'data' =>$data ];
    }

    public function getCampinDataById( $camp_id ){
        if( $camp_id ){
            $result = mysqli_query($this->conn,"SELECT * FROM `campn_services` WHERE `campin_id`=$camp_id");
            if( $result ){
                $data =[];
                while ($row = mysqli_fetch_assoc($result)){
                    $row['service_name'] ='';
                    $row['extra_name']='';
                    if( $row['service_id'] ){
                        $data_one = $this->getServiceById( $row['service_id'] );
                        $row['service_name'] = $data_one['description'];
                    }
                    if( $row['extra_id'] ){
                        $data_two =  $this->getExtrasById( $row['extra_id'] );
                        $row['extra_name'] = $data_two['description'];
                    }
                    $data[]= $row;
                }
                return $data;
            }
            else {
                return 404;
            }
        }

        return 503;
    }

    public function getExtrasById( $extra_id ){
        if($extra_id){
            $result= mysqli_query($this->conn , "SELECT * FROM `extras` WHERE `id` = $extra_id");
            $row = mysqli_fetch_assoc($result);
            return $row;
        }
    }


    public function multipleServices( $data ){
        if( $data ){
            $products = [];
            $order_ids = [];
            $price_count = 0;
            $user_blc  =(int) $this->checkUserBalance( $data['uid'] );
            $campain_services = $this->getCampinDataById( $data['campain_id'] );
            if( $campain_services != 404 || $campain_services != 503 ){
                foreach ($campain_services as $camps ){
                    $services = $this->getServiceById($camps['service_id']);
                    $extra = $this->getExtrasById( $camps['extra_id']);
                    $price_count = $price_count + (float)$camps['price'];
                    $item=[
                        'uid' => $data['uid'],
                        'service_id' => $services['id'],
                        'extra' => $extra['id'],
                        'quantity' => $camps['qty_service'],
                        'links' => $data['links'],
                        'keywords' => $data['keywords'],
                        'article' => $data['article'],
                        'price' => (float)$camps['price'],
                        'date' => $data['date'],
                        'refId' => $data['refId'],
                    ];
                    $products[] = $item;
                }
                if( $products){
                    //bussiness logic where it check the price and available balance
                    $admin_blc = $this->checkAdminBalaence();
                    $oids = '';
                    if($user_blc >= $price_count && $admin_blc >=$price_count ){
                        foreach ( $products as $product ){
                                $result = $this->apiOrder( $product );
                                if($result != 000 ){
                                    $oids[]= $result;
                                    $product['oid'] = $result;
                                    $this->createMultiServices($product);
                                }
                                else{
                                    $oids[]= $result;
                                }
                            }
                            return $oids;
                    }
                    return 111; //not enough money to buy

                }
                else{
                    return 404;
                }
            }
        }
        else{
            return 404;
        }
    }


    public function checkAdminBalaence(){
        $admin = mysqli_query($this->conn , "SELECT `email`,`api_key` FROM `admins` WHERE `id` = 1");
        $data = mysqli_fetch_array($admin);
        $url = 'http://panel.seoestore.net/action/api.php';
        $api_key = $data['api_key'];
        $email = $data['email'];
        $data = Array(
            'api_key'=>$data['api_key'],//must included in every request
            'email'=>$data['email'],//must included in every request
            'action'=>'balance',//action must included in every request
        );

        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $_post[] = $key.'='.$value;
            }
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, join('&', $_post));
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        $result = curl_exec($ch);
        curl_close($ch);
        $balance = json_decode($result,true);
        if(isset($balance['error'])){
            $balance = 'N/A';
        }elseif(isset($balance['balance'])){
            $balance = $balance['balance']/100;
        }else{
            $balance = 0;
        }
        return $balance;
    }

    public function createMultiServices( $data ){
        if($data){
            $uid = $data['uid'];
            $date = $data['date'];
            $keywords =  $data['keywords'];
            $links = $data['links'];
            $service = $data['service_id'];
            $extras = $data['extra'];
            $quantity = $data['quantity'];
            $refId = $data['refId'];
            $price = $data['price'];
            $oid = $data['oid'];
            $price = $price*100;
            $sql = "INSERT INTO `orders`".
                "(`oid`,`uid`,`price`,`date`,`keywords`,`links`,`service_id`,`extras`,`quantity`,`refId`)".
                "VALUES ( '$oid','$uid','$price','$date','$keywords','$links','$service','$extras','$quantity','$refId')";
            $retval = mysqli_query($this->conn, $sql );
            return ['tag' => 200,'oid'=>$oid];
        }
        else{
            return false;
        }
    }

}