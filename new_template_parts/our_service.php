<!-- services
================================================== -->
<section id='services' class="s-services light-gray">

    <div class="row section-header" data-aos="fade-up">
        <div class="col-full">
            <h3 class="subhead">What We Do</h3>
            <h1 class="display-1">Rank your website #1 in Google.</h1>
        </div>
    </div> <!-- end section-header -->

    <div class="row" data-aos="fade-up">
        <div class="col-full">
            <p class="lead">
                you’re here because you know that Search Engine Optimization is the backbone of success on the internet and that Backlinks are an essential component of a successful search engine optimization strategy.
                You also realize that it takes time to accrue effective backlinks, which is why you want to know why GTR Backlinks is the best option available to you.
                GTR will do this by delivering high-quality backlinks and unique articles at the cheapest prices on the internet.
                Order as many backlinks as you want in any format of your choosing and then sit back and watch the magic happen.<br>
                Check out the website for a better understanding of the services on offer.<br>
                <br>
                Well, there are a few things you will immediately appreciate.GTR will get your website to the top position in the Google rankings. And You should care about that because the higher the Google bots rank you, the more traffic your website will attract and the more money you can expect to make in the long run.
            </p>
            <a href="custom-page.php?id=1" class="btn btn-primary service_more">Buy services</a>
        </div>
    </div> <!-- end about-desc -->

</section> <!-- end s-services -->