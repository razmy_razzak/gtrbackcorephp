$( document ).ready(function() {
    var order = $("#order"), readmore= $("#read_more"), review = $("#review")
    var ids = {
        'order' :order,
        'review':review,
        'readmore' :readmore
    }


    $('#order_btn').click(function(e) {
        $.each(ids, function( index, value ) {
            if(index != 'order'){
                value.fadeOut(100);
            }
            else{
                value.delay(100).fadeIn(100);
            }
        });
        e.preventDefault();
    });
    $('#review_btn').click(function(e) {
        $.each(ids, function( index, value ) {
            if(index != 'review'){
                value.fadeOut(100);
            }
            else{
                value.delay(100).fadeIn(100);
            }
        });
        e.preventDefault();
    });
    $('#readmore_btn').click(function(e) {

        $.each(ids, function( index, value ) {
            if(index != 'readmore'){
                value.fadeOut(100);
            }
            else{
                value.delay(100).fadeIn(100);
            }
        });
        e.preventDefault();

    });

    $("#review_form").submit(function ( event ) {
        event.preventDefault();
        if(user_id == 0){
            $('#id01').css('display','block');
        }
        else {
            var formData = $("#review_form").serializeArray();
            $.post("doRating.php",
                formData,
                function(data, status){
                    data = jQuery.parseJSON(data);
                    if(data.error){

                    }
                    // if success we show loder and message
                    else if (data.success){
                        $('#id02').css('display','block');
                        $('#id02 .mainpage').html("");
                        $('#id02 .mainpage').append('<div class="alert-box--success">Thank you. Your review submitted successfully  </div>');
                    }
                });
        }
    });

    $('input[name=username]').keypress(function(){
        $( "#error_msg" ).remove();
    });

    $('input[name=password]').keypress(function(){
        $( "#error_msg" ).remove();
    });

    $("#login_form").submit(function (e) {
        e.preventDefault();
        var formData = $("#login_form").serializeArray();
        $.post("dologin.php",
            formData,
            function(data, status){
                data = jQuery.parseJSON(data);
                if(data.error){
                    if( $('#error_msg').length == 0 ){
                        $( "#error" ).append( '<strong id="error_msg">'+data.error+'</strong>' );
                    }
                }
                // if success we show loder and message
                else if (data.msg){
                    var url = window.location.href.split('#')[0];
                    user_id = data.id;
                    $( "#success" ).append( '<strong id="success_msg">'+data.msg+', Now You will be redirect to product page</strong>' );
                    setTimeout(function () {

                        window.location = url;
                    }, 1000)
                }

            });
    });
    $( "#login_home" ).on('click',function( event ) {
        if(user_id == 0){
            $('#id01').css('display','block');
        }
    });

    $( "#product_form" ).submit(function( event ) {
        event.preventDefault();
        if(user_id == 0){
            $('#id01').css('display','block');
        }
        else {
            $('#preloader').show();
            $('#loader').show();
            var formData = $("#product_form").serializeArray();
            $(this).find('.form_error').remove();
            if(validate( formData )){
                $.post("doService.php",
                    formData,
                    function(data, status){
                        $('#id02 .mainpage').html("");
                        $('#preloader').hide();
                        $('#loader').hide();
                        data = jQuery.parseJSON(data);
                        if(data.success){
                            $('#id02').css('display','block');
                            $('#id02 .mainpage').append('<div class="alert-box--success">Order Created successfully </div>');
                            $('#id02 .mainpage').append('<h2>You Order Details</h2>');
                            $('#id02 .mainpage').append('<h3>Your Order Id :' +data.oid+'</h3>');
                            $.each(formData, function (index , value) {
                                $('#id02 .mainpage').append('<div class="items"> <label class="dispaly_inline">'+value.name+'</label> : <span>'+value.value+'</span></div>');
                            });
                            $("#product_form").trigger("reset");
                        }
                        else if(data.fund){
                            $('#id02').css('display','block');
                            $('#id02 .mainpage').append('<div class="alert-box--error"><p>Sorry Please add more fund and try again</p></div>');
                            $('#id02 .mainpage').append('<div class="alert-box--error"><p>Please wait redirecting to fund page</p></div>');
                            setTimeout(function () {
                                window.location = window.location.origin+'/funds-add.php';
                            }, 1500)
                        }
                        else if(data.qtys){
                            $('#id02').css('display','block');
                            $('#id02 .mainpage').append('<div class="alert-box--error"><p>Sorry minimum quantity is : '+data.qtys+'</p></div>');
                        }
                    });
            }
            return false;
        }
        return false;

    });

    $("input").keypress(function(){
        $(this).next("span").hide();
        $(this).css('border-bottom', '1px solid #9e9e9e');
    });
    $("textarea").keypress(function(){
        $(this).next("span").hide();
        $(this).css('border-bottom', '1px solid #9e9e9e');
        $(this).find('.form_error').remove();
        $('#campain_form').find('.form_error').remove();
    });
    
    function ajaxCall() {
        $.get(url, function(data, status){
            alert("Data: " + data + "\nStatus: " + status);
        });
    }

    function validate( formData ) {
        var error_list = [];
        var empty_check = false;
        var pattern_check = false;
        var url_check = false;
        var number_check = false;
        $.each(formData, function( index, value ) {
            if( value.value == '' && value.name != 'article'){
                empty_check = true;
                findValidation('empty')
                $('input[name='+value.name+']').css('border-bottom', '1px solid red');
                $('input[name='+value.name+']').after('<span class="form_error">'+findValidation('empty')+'</span>');

                if(value.name == 'keywords' || value.name == 'links'){
                    $('textarea[name='+value.name+']').css('border-bottom', '1px solid red');
                    $('textarea[name='+value.name+']').after('<span class="form_error">'+findValidation('empty')+'</span>');
                }
            }

            if( value.name == 'quantity' && !empty_check){
                if( !isNumber(value.value)){
                    $('input[name='+value.name+']').css('border-bottom', '1px solid red');
                    $('input[name='+value.name+']').after('<span class="form_error">Please enter numbers only</span>');
                    number_check = true
                }
            }

            switch(value.name) {
                case 'url':
                    return 'Please enter valid url';
                    break;
                case 'number':
                    return 'Please enter numbers only';
                    break;
                default:
                    return 'Filed required';
            }

        });
        if( empty_check || pattern_check || url_check || number_check ){
            return false;

        }else {
            return true;
        }
    }

    function isNumber(str) {
        var pattern = /^\d+$/;
        return pattern.test(str);
    }

    function findValidation( expression ) {
        switch (expression) {
            case 'empty':
                return 'Field required';
                break;
            case 'url':
                return 'Please enter valid url';
                break;
            case 'number':
                return 'Please enter numbers only';
                break;
            default:
                return 'Filed required';
        }
    }

    function urlcheck( url ) {
        var pattern = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;
        return  pattern.test(url);
    }

    // With JQuery slider range budjet
    $('#ex20a')
        .parent()
        .find(' >.well')
        .toggle()
        .find('input')
        .slider('relayout');

    $( ".slider-horizontal" ).click(function() {
        $("#cmp_services").html("");
        var value = $('#ex19').val();
        getCampsData(value);

    });

    function getCampsData( value ) {
        $.get('doCampain.php?campin_id='+value, function(data, status){
            data = jQuery.parseJSON(data);
            if(data.error){
                $('#cmp_services').append('<stong style="color: red" >Something went wrong, please contact Admin</stong>');
            }
            else if(data.success){
                $('#campain').show();
                $.each( data.success, function (index, value) {
                    $('#cmp_services').append(
                        '<li>'+
                        value.service_name+
                        (value.extra_id > 0? '<ul><li>'+value.extra_name+'</li></ul>' : '') +
                        '</li>'
                    );
                } );
            }
        });
    }
    var value = $('#ex19').val();
    if(value > 0){
        getCampsData(value);
    }

    $('#campain_form').submit(function (e) {
        e.preventDefault();
        if(user_id == 0){
            $('#id01').css('display','block');
        }
        else {
            var links = $('textarea[name=links]').val(),
                keywords =$('textarea[name=keywords]').val(),
                budget_id = $('#ex19').val();
            if( links == ''){
                $('textarea[name=links]').css('border-bottom', '1px solid red');
                $('textarea[name=links]').before('<span class="form_error">'+findValidation('empty')+'</span>');
            }
            else if( keywords == ''){
                $('textarea[name=keywords]').css('border-bottom', '1px solid red');
                $('textarea[name=keywords]').before('<span class="form_error">'+findValidation('empty')+'</span>');
            }
            else{
                var formData = $("#campain_form").serializeArray();

                $.post("doMultipleServices.php",
                    formData,
                    function(data, status){
                        data = jQuery.parseJSON(data);
                        if(data.error){
                            $('#cmp_services').append('<stong style="color: red" >Something went wrong, please contact Admin</stong>');
                        }
                        else if(data.success){
                            if( data.success == 111 ){
                                $('#id02').css('display','block');
                                $('#id02 .mainpage').append('<div class="alert-box--error"><p>Sorry Please add more fund and try again or If you have sufficient balance contact admin </p></div>');
                                $('#id02 .mainpage').append('<div class="alert-box--error"><p>Please wait redirecting to your dashboard</p></div>');
                                setTimeout(function () {
                                    window.location = window.location.origin+'/funds-add.php';
                                }, 1500)
                            }
                            else if( data.success == 000){
                                console.log(data.success);
                            }
                            else {
                                $('#id02 .mainpage').html('');
                                $('#id02').css('display','block');
                                $('#id02 .mainpage').append('<div class="alert-box--success"><p>Services Created</p></div>');
                                console.log(data.success);
                            }
                        }
                    });

            }
        }




    });


});
