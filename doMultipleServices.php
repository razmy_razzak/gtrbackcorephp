<?php
require_once 'controllers/Services.php';

if($_POST['uid']){
    $data= [
        'uid'  => $_POST['uid'],
        'campain_id' => $_POST['cmap_range'], //get services and extras from campai_service table
        'links' => $_POST['links'],
        'keywords' => $_POST['keywords'],
        'article' =>$_POST['article'],
        'date'         =>  gmdate('ymdHis'),
        'refId'         =>  isset($_POST['refId'])? $_POST['refId']:'',
    ];
    $service = new Services();
    $result = $service->multipleServices($data);
    if( $result == 503 || $result == 404 ){
        echo json_encode(array('error' => 'Not found'));
    }
    else{
        echo json_encode(array('success' => $result));
    }

}