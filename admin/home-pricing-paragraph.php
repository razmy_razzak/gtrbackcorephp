<?php 
require 'includes/header.php';
require_once 'libraries/htmlpurifier/HTMLPurifier.auto.php';
$config = HTMLPurifier_Config::createDefault();
$purifier = new HTMLPurifier($config);
?>
<link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#home']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='home-pricing-paragraph.php']" ).parent().addClass( "active" );
});
</script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="ion ion-drag"></i> Main paragraph
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li>Home page</li>
    <li class="active">Main paragraph </li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">

<?php
// Update
if(isset($_POST['submit'])) {
  // get fields
  $title = mysqli_real_escape_string($conn , htmlspecialchars($_POST['title']));
  
  $desc = $_POST['description'];
  $desc = $purifier->purify($desc);
  $desc = mysqli_real_escape_string($conn , $desc);
  

  //insert data
  $sql = mysqli_query($conn , "UPDATE `paragraphs` SET
                      `title`='$title',
                      `description`='$desc'
                      WHERE `id`='7'");
  //test
  if(!$sql){
      die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysqli_error($conn) . "<div>");
  }
  echo "<div class=\"alert alert-success\" role=\"alert\">Details Successfully Updated!</div>";
}


// Get data
$data = mysqli_query($conn , "SELECT * FROM `paragraphs` WHERE `id`='7'");
$row = mysqli_fetch_array($data);
  $title = $row['title'];
  $description = $row['description'];
  $button = $row['button'];
?>

  <div class="box box-primary">
    <div class="box-body">
      <form method="post" action="">
        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" class="form-control" id="title" name="title" value="<?php echo $title ?>" placeholder="Title...">
        </div>

        <div class="form-group">
          <label for="description">Description</label>
          <textarea class="form-control" rows="4" id="description" name="description" placeholder="Description..."><?php echo $description ?></textarea>
        </div>
                      
        <input type="submit" class="btn btn-primary flat margin-top-20" name="submit" value="UPDATE">
      </form>
  	</div>
  </div>
</section><!-- /.content -->

<script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script>
  $(function () {
    $("textarea").wysihtml5();
  });
</script>

<?php 
require 'includes/footer.php';
?>