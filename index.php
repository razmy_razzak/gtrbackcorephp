<?php
session_start();
require_once 'includes/connect.php';
require 'includes/lang.php';
// Get data
$data = mysqli_query($conn , "SELECT * FROM `seo` WHERE `page`='home'");
$row = mysqli_fetch_array($data);
  $title = $row['title'];
  $kw = $row['kw'];
  $description = $row['description'];

$tplQuery = mysqli_query($conn , "SELECT * FROM `tpl` WHERE `active`='1'");
if(mysqli_num_rows($tplQuery)!=1){
    $tpl = 'standard';
}else{
    $tplData = mysqli_fetch_array($tplQuery);
    $tpl = strtolower($tplData['tpl']);
}

//Get data
$query = mysqli_query($conn , "SELECT * FROM `logo` WHERE `id`='1'");
$data = mysqli_fetch_array($query);
$logo = $data[1];
$data = mysqli_query($conn ,"SELECT * FROM `main` WHERE `id` = '1'");
$row = mysqli_fetch_array($data);
  $site_name = $row['site_name'];
  $dir = $row['dir'];
  $analytics = $row['analytics'];
  $serPricePlus = $row['service_price'];
  $extraPricePlus = $row['extra_price'];
  $sPercentage = 1 + $serPricePlus/100;
  $ePercentage = 1 + $extraPricePlus/100;

$data = mysqli_query($conn ,"SELECT * FROM `footer` WHERE `id` = '1'");
$row = mysqli_fetch_array($data);
  $footer1_title = $row['footer1_title'];
  $footer1_desc = $row['footer1_desc'];
  $footer2_title = $row['footer2_title'];
  $fb_link = $row['fb_link'];
  $tw_link = $row['tw_link'];
  $gplus_link = $row['gplus_link'];
  $linkedin_link = $row['linkedin_link'];
  $footer3_title = $row['footer3_title'];
  $address = $row['address'];
  $phone = $row['phone'];
  $fax = $row['fax'];
  $mobile = $row['mobile'];
  $email = $row['email'];



$query = mysqli_query($conn ,"SELECT `image_link` FROM `slideshow`");
$images = array();

$i = 0;
while($row=mysqli_fetch_array($query)){
  if(!empty($row[0])){
    $images[$i] = $row[0];
    $i++;
  }
}



$query = mysqli_query($conn , "SELECT * FROM `paragraphs`");
while($row=mysqli_fetch_array($query)){
  $id = $row['id'];
  if($id==1){
    $title1 = $row['title'];
    $description1 = $row['description'];
    $button1 = $row['button'];
    $link1 = 'register.php';
    if(isset($_SESSION['username'])) {
      $button1 = "Go to Dashboard";
      $link1 = 'dashboard.php';
    }
  }elseif($id==2){
    $title2 = $row['title'];
    $icon2 = $row['icon'];
    $description2 = $row['description'];
  }elseif ($id==3) {
    $title3 = $row['title'];
    $icon3 = $row['icon'];
    $description3 = nl2br($row['description']);
  }elseif ($id==4) {
    $title4 = $row['title'];
    $icon4 = $row['icon'];
    $description4 = $row['description'];
  }elseif ($id==6) {
    $title6 = $row['title'];
    $description6 = $row['description'];
  }elseif ($id==7) {
    $title7 = $row['title'];
    $description7 = $row['description'];
  }
}

$custom_pages = mysqli_query($conn , "SELECT * FROM `custom_pages` where status ='enable'");
require_once 'templates/'.$tpl.'/main.php';
