<!DOCTYPE html>
<html class="no-js" lang="en">
<head>

    <!--- basic page needs
    ================================================== -->
    <meta charset="utf-8">
    <title><?php if( isset($title)){echo $title;} ?></title>
    <meta name="description" content="<?php if( isset($description)){echo $description;} ?>">
    <?php
    if (isset($kw)) {
        echo '<meta name="keywords" content="'.$kw.'" />';
    }
    ?>
    <meta name="author" content="SEOeStore" />

    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="dist/css_new/base.css">
    <link rel="stylesheet" href="dist/css_new/vendor.css">
    <link rel="stylesheet" href="dist/css_new/main.css">

    <!--    custom-->
    <link rel="stylesheet" href="dist/css_new/custom.css">
    <link rel="stylesheet" href="dist/css_new/jquery.rateyo.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.2/css/bootstrap-slider.min.css">

    <!-- script
    ================================================== -->
    <script src="dist/js_new/modernizr.js"></script>
    <script src="dist/js_new/pace.min.js"></script>
    <script src="dist/js_new/jquery.rateyo.min.js"></script>

    <!-- favicons
    ================================================== -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
</head>

<body id="top">