
<?php 
require 'includes/header.php';
$general = new general();
$sPercentage = 1 + $serPricePlus/100;
$ePercentage = 1 + $extraPricePlus/100;
function getArticleCats(){
    $sql = mysql_query("SELECT * FROM `article_cats` ORDER BY `name` ASC");
    while($record = mysql_fetch_array($sql)){
        echo '<option value="' . $record['code'] . '">' . $record['name']. '</option>'; 
    }
}

?>

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='order.php']" ).parent().addClass( "active" );
  
});
</script>

<script>
$(function(){
    $('#service').on('change', function(){
        var ser = $(this).val();
        $("#extras").removeClass('hide');
        $("#extras").load('extras.php?ser='+ser);
    })
})
</script>
<script>
function checkFunc(extraID){
    document.getElementById("extra"+extraID).checked = true;
}

function checkQty() {
    var ser = $('#service').val();
    var qty = $('#quantity').val();
    if((ser)!='' && (qty)!='') {
        $.ajax({
            url: 'validation.php',
            data: { 
                "ser": ser, 
                "qty": qty, 
            },
            type: 'GET',
            success: function(result){
                if (result !== 'ok') {
                    alert('Error: minimum quantity for the choosen service is: ' + result);
                }
            }
        })
    }
}

$(function(){
    $('#quantity').change(function (e){
        checkQty();
    })
    $('#service').change(function (e){
        checkQty();
    })
})

function validateForm(){
    $('.btn-load').show();

    var service = document.forms["orderForm"]["service"].value;
    var quantity = document.forms["orderForm"]["quantity"].value;
    var links = document.forms["orderForm"]["links"].value;
    var keywords = document.forms["orderForm"]["keywords"].value;
    
    if (service == null || service == "") {
        $('.btn-load').hide(200);
        alert("Error: Please choose a service.");
        return false;
    }
    if (quantity == null || quantity == "") {
        alert("Please enter quantity!");
        $('.btn-load').hide(200);
        return false;
    }
    if (links == null || links == "") {
        alert("Error: Please enter links.");
        $('.btn-load').hide(200);
        return false;
    }
    if (keywords == null || keywords == "") {
        alert("Error: Please enter keywords.");
        $('.btn-load').hide(200);
        return false;
    }
}

</script>


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-rocket"></i> Make order
  </h1>
  <ol class="breadcrumb">
    <li><a href="dashboard.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li class="active">Make order</li>

  </ol>
</section><!-- /.content Header-->
<!-- Content -->
<section class="content">
<?php
if (isset($_POST['order'])){
    //error handler
    $err = '';
    // check if user not login
    if (!isset($_SESSION['userid'])) {
        $err = 'Please <a href="login.php">Login</a> or <a href="register.php">Create account</a> to be able to submit orders!';
    }
    //submit orders
    if($err == '') {
        $error = '';
        //Get data
        $service = mysql_real_escape_string(htmlspecialchars($_POST['service']));
        $quantity = mysql_real_escape_string(htmlspecialchars($_POST['quantity']));
        $links0 = $_POST['links'];
        $links = mysql_real_escape_string(htmlspecialchars($_POST['links']));
        $keywords0 = $_POST['keywords'];
        $keywords = mysql_real_escape_string(htmlspecialchars($_POST['keywords']));
        $rtcl = mysql_real_escape_string(htmlspecialchars($_POST['article']));
        if ($rtcl==''){
            $rtcl = '0';
        }
        $uname = $_SESSION['username'];
        if ($service==''){
            $error.= '<div><i class="fa-inline fa fa-minus-circle" aria-hidden="true"></i> Error: Please choose a service!</div>';
        }else{
            $minQty = mysql_fetch_array(mysql_query("SELECT `min_qty` FROM `service` WHERE `id`='$service'"))[0];
        }
        if ($quantity==''){
            $error.= '<div><i class="fa-inline fa fa-minus-circle" aria-hidden="true"></i> Error: Quantity is required!</div>';
        }elseif (!preg_match("/^[0-9]*$/",$quantity)){
            $error.= '<div><i class="fa-inline fa fa-minus-circle" aria-hidden="true"></i> Error: Quantity must be a valid number!</div>';
        }elseif ($quantity==0) {
            $error.= '<div><i class="fa-inline fa fa-minus-circle" aria-hidden="true"></i> Error: Please inter a valid quantity!</div>';
        }else {
            if (isset($minQty)){
                if ($quantity<$minQty) {
                    $error.= '<div><i class="fa-inline fa fa-minus-circle" aria-hidden="true"></i> Error: Minimum quantity for the chosen services is:'.$minQty.'</div>';
                }
            }
        }
        if ($links==''){
            $error.= '<div><i class="fa-inline fa fa-minus-circle" aria-hidden="true"></i> Error: You must submit links!</div>';
        }
        if ($keywords==''){
            $error.= '<div><i class="fa-inline fa fa-minus-circle" aria-hidden="true"></i> Error: You must submit Keywords!</div>';
        }
        if ($error == ''){
            $servicePrice = mysql_fetch_array(mysql_query("SELECT `price` FROM `service` WHERE id='$service'"))[0];
            if ($servicePrice !=0) {
              $price = $servicePrice * $quantity;
            }else{
            $servicePrice = mysql_fetch_array(mysql_query("SELECT `panel_price` FROM `service` WHERE id='$service'"))[0];
              $price = $servicePrice * $quantity * $sPercentage;
            }

            $extrasArr = array();
            if (isset($_POST['extra'])) {
              $eIds = $_POST['extra'];
              $extrasArr = $eIds;
              $query = mysql_query("SELECT * FROM `extras`");
              while ($row = mysql_fetch_array($query)) {
                $eId = $row['id'];
                //if extra id on the extras array sent by POST
                if (in_array($eId, $eIds)){
                  //Getting price
                  if ($row['price'] > 0){
                    $ePrice = $row['price'];
                  }else{
                    $ePrice = $row['panel_price'] * $ePercentage;
                  }
                  //check if extra should multiply on quantity
                  if($row['multiple'] == 1){
                    $ePrice = $ePrice * $quantity;
                  }
                  //Getting total order price by adding extras price
                  $price = $price + $ePrice;
                }
              }
            }

            $price = round($price);
            $date = gmdate('ymdHis');
            $extras = implode(',', $extrasArr);
            $uBalance = $userBalance * 100;
            if ($uBalance >= $price){
              //insert data
              unset($orde);
              $orde = Array(
                  'api_key'=>$api_key,
                  'email'=>$email,
                  'action'=>'order',
                  'service'=>$service,
                  'quantity'=>$quantity,
                  'extras'=>$extras,
                  'links'=>$links0,
                  'keywords'=>$keywords0,
                  'article'=>$rtcl,
                  );
                  //handle Data for API
                  if (is_array($orde)) {
                    if(empty($extras)){
                      unset($orde['extras']);
                    }
                    if (is_array($orde)) {
                      $dataString = http_build_query($orde, '', '&');
                    }
                  }
                  //submit API order
                  $ch = curl_init($url);
                  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                  curl_setopt($ch, CURLOPT_POST, 1);
                  curl_setopt($ch, CURLOPT_HEADER, 0);
                  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                  curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
                  curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
                  $result = curl_exec($ch);

                  curl_close($ch);
                  //getting PHP result array
                  $result = json_decode($result, true);
                    //check if order success
                    if ( ! isset($result['order'])) {
                       $result['order'] = null;
                      $general -> alert('Orders not submitted! <b>Unknown Error</b>', 'danger');
                    }else{
                      $oid= $result['order'];
                        //End send API request
                      $sql = "INSERT INTO `orders`".
                     "(`oid`,`uid`,`price`,`date`,`keywords`,`links`,`service_id`,`extras`,`quantity`)".
                     "VALUES ( '$oid','$uid','$price','$date','$keywords','$links','$service','$extras','$quantity')";
                      $retval = mysql_query( $sql, $conn );
                      if(! $retval )
                      {
                        die('Could not enter data: ' . mysql_error());
                      }
                      //submit message
                      $general -> alert('Orders submitted! order ID: <b>'.$oid . '</b> Check <a href="reports.php">Reports page</a>', 'success');
                      //getting new balance
                    }
              }else {
                $general -> alert('<div><i class="fa-inline fa fa-minus-circle" aria-hidden="true"></i> Not enough balance! You should have <b>$' . $price/100 . '</b> in your balance. <a href="funds-add.php">Add balance</a></div>', 'danger');
            }
    }else {
        //if error submit the error message
        $general -> alert($err, 'danger');
    }
}}
?>
  <div class="box box-primary">
    <div class="box-body">
    <form class="form-order" name="orderForm" action="" onsubmit="return validateForm()" method="post">
        <div class="form-group">
            <label for="service">Service:</label>
            <select class="form-control select2" id="service" name="service" data-placeholder="Select Service" required>
              <option value="">- select service -</option>

<?php 
  $sprice = mysql_query("SELECT `id`,`price`,`description`,`panel_price` FROM `service` WHERE status = 1 ORDER BY `ordering`");
  if(mysql_num_rows($sprice) > 0 ){
  while ($data = mysql_fetch_array($sprice)) {
    $id = $data['id'];
    $description = $data['description'];
    $price = $data['price']/100;
    $panel = $data['panel_price']/100;
    $panel_price = $panel*$sPercentage;

    if ($price != 0) {
      echo '<option value="'.$id.'">'.$description.'[price:$'.$price.']</option>';
    }
    else{
      echo '<option value="'.$id.'">'.$description.'[price:$'.$panel_price.']</option>';
    }
  }
}
?>
</select>
          </div>
          <div id="extras" class="hide">
              <img src="dist/img/loading.gif">
          </div>
          <div class="form-group">
              <label for="quantity">Quantity:</label><input class="form-control" type="number" id="quantity" placeholder="e.g. 1000" name="quantity" required/>
          </div>

          <div class="tier hide"><input type="checkbox" name="tier" value="1" <?php if($tier == 1) echo 'checked="checked"';?>> Tier project</div>

          <div class="form-group">
              <label id="links-label" for="links">Links:</label> <a id="links-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="One link/URL per line."><sup class="fa fa-info-circle text-gray"></sup></a>
              <textarea class="form-control" id="links" rows="4" placeholder="http://example.com/page-one.html
http://example.com/page-two.html
http://example.com/page-three.html" type="text" name="links" required/></textarea>
          </div>

          <div class="show-kw <?php if($tier != 1) echo 'hide';?>"><a><i class="fa fa-eye" aria-hidden="true"></i></a> Show keywords</div>

          <div id="keywords-block" class="form-group">
              <label for="keywords">Keywords:</label> <a data-toggle="tooltip" data-placement="top" title="" data-original-title="One Keyword per line or comma separated."><sup class="fa fa-info-circle text-gray"></sup></a>
              <textarea class="form-control" id="keywords" rows="4" placeholder="Keyword Example One
Keyword Example Two
Keyword Example Three" type="text" name="keywords" required/></textarea>
          </div>

          <div class="form-group hide" id="article-category">
              <label for="article">Article Category:</label> <i>optional</i> <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Select article category for your keywords, keep empty or choose auto detect to get a proper article."><sup class="fa fa-info-circle text-gray"></sup></a>
              <select class="form-control select2" id="article" name="article" data-placeholder="Select Article Category">
                  <option value="">- Select Article Category -</option>
                  <option value="0">- Auto detect article category -</option>
                  <?php getArticleCats(); ?>
              </select>
          </div>

          <button class="btn btn-primary flat" type="submit" name="order">Submit <i class="fa fa-circle-o-notch fa-spin btn-load" style="display: none;"></i></button>
          <!--<div class="price-button bg-light-gray">Total price: <b>$<span id="usd-price">0</span></b></div>-->
                       
      </form>
    </div>
  </div>
</section><!-- /.content -->
<script>
$(function(){
  $("document").ready(function(){
    $.ajax({
        url: 'prices.php',
        type: 'GET',
        dataType: 'json',  // Let jQuery know returned data is json.
        success: function(result){
          var services=[];
          $.each(result, function(id, price) {
              services[id]=price;
          });
        $.ajax({
            url: 'extras-prices.php',
            type: 'GET',
            dataType: 'json',  // Let jQuery know returned data is json.
            success: function(result){
                var extras=[];
                $.each(result, function(id, price) {
                    extras[id]=price;
                });
                function updatePrice(){
                    var id = $('#service').val();
                    var qty = $('#quantity').val();
                    var sum = 0;
                    $('#extras input').each(function(){    
                        if($(this).is(':checked')) {
                          switch($(this).val()){
                            case '1':
                                sum+=extras[1]*qty/100;
                            break;
                            case '2':
                                sum+=extras[2]*qty/100;
                            break;
                            case '3':
                                sum+=extras[3]*qty/100;
                            break;
                            case '4':
                                var abqty = $('.extraq').val();
                                sum+=extras[4]*abqty/100;
                            break;
                            case '5':
                                sum+=extras[5]*qty/100;
                            break;
                            case '6':
                                sum+=extras[6]*qty/100;
                            break;
                            case '7':
                                sum+=extras[7]*qty/100;
                            break;
                            case '8':
                                sum+=extras[8]*qty/100;
                            break;
                            case '9':
                                sum+=extras[9]*qty/100;
                            break;
                            case '10':
                                sum+=extras[10]*qty/100;
                            break;
                            case '11':
                                sum+=extras[11]/100;
                            break;
                            case '12':
                                sum+=extras[12]/100;
                            break;
                            case '13':
                                sum+=extras[13]/100;
                            break;
                            case '14':
                                sum+=extras[14]/100;
                            break;
                          }

                        }
                    });
                    sum+=services[id]*100*qty/10000;
                    $('#usd-price').html((sum).toFixed(2));
                }
                $(document).on('change', '#service', function (){
                    $('#extras input').each(function(){
                        $(this).attr('checked', false);
                    })
                    updatePrice();
                })
                $(document).on('input', '#quantity', function (){
                    updatePrice();
                })
                $(document).on('change', '#extras input', function (){
                    updatePrice();
                })
                $(document).on('input', '.extraq', function (){
                    updatePrice();
                })
            }
        });
      }
    });
  });
})
</script>
<script>
$(function(){

    if($(".tier input").is(':checked')) {        
        $('#keywords').val('Click here');
        $('#links-label').html('Orders ID');
        $('#links').attr("placeholder", "#1234\n#1235");
        $('#links-tooltip').attr("data-original-title", "One ID per line");
        $('#keywords-block').addClass('hide');
    }
    $('.show-kw').css('cursor','pointer');
    $('.show-kw').click(function(){
        $(this).hide(100);
        $('#keywords-block').removeClass('hide');
    })

})
</script>
<script>
  $(function () {
  $(".select2").select2();
});
</script>
<?php 
require 'includes/footer.php';
?>