<?php 
require 'includes/header.php';
function getPaymentMethod(){
    $sql = mysql_query("SELECT * FROM `payment_methods` ORDER BY `name` ASC");
        echo '<option value="">Choose payment method</option>'; 
    while($record = mysql_fetch_array($sql)){
        echo '<option value="' . $record['id'] . '">' . $record['name']. '</option>'; 
    }
}

?>

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#payment']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='funds-add.php']" ).parent().addClass( "active" );
  
});
</script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-money"></i> Add user funds
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li>Payment</li>
    <li class="active">Add user funds</li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">
<?php
if (isset($_POST['submit'])) {
  $userID = mysql_real_escape_string(htmlspecialchars($_POST['username']));
  $amount = mysql_real_escape_string(htmlspecialchars($_POST['amount']));
  $transID = mysql_real_escape_string(htmlspecialchars($_POST['transID']));
    $dateTime = gmdate('ymdHis');
    $sql1 = "INSERT INTO `payment` (`uid`, `txn_id`, `amount`, `date`) VALUES ('$userID', '$transID', '$amount', '$dateTime')";
    $retval = mysql_query($sql1, $conn);
    if (!$retval) {
        die('Error1: ' . mysql_error());
    }else{
       $general -> alert('Payment record added!', 'success');
    }

}
?>
  <div class="box box-primary">
    <div class="box-body">

      <form method="post" action="">
        <div class="form-group">
            <label class="" for="username">User:</label>
            <select class="form-control input-sm select2" id="username" name="username" data-placeholder="Select username" required>
            <option value="">- Select User -</option>
            <?php $general -> usersRecords(); ?>
            </select>
        </div>
        <div class="form-group">
          <label class="" for="amount">Amount:</label>
          <div class="input-group">
            <span class="input-group-addon no-radius">$</span>
            <input type="number" step="any" class="form-control" id="amount" name="amount" placeholder="Amount in USD" required>
          </div>
        </div>

        <div class="form-group">
          <label class="" for="transID">Method:</label>
          <select list="txn-list" type="text" class="form-control" id="transID" name="transID" placeholder="Transaction ID" required>
          <?php getPaymentMethod()?>
          </select>
          <small><i class="fa fa-info-circle"></i> Add more payment method from <a href="payment-method.php">here</a></small>
        </div>
        <div class="form-group">
          <input type="submit" name="submit" value="Submit" class="btn btn-primary flat">
        </div>

        </form>
    </div>
  </div>
</section><!-- /.content -->




<?php 
require 'includes/footer.php';
?>