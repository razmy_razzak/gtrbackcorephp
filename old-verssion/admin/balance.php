<?php 
require 'includes/header.php';
$balance = mysql_query("SELECT 
    0.00 AS `pay`, `price`, `orders`.`date`, `oid`, `users`.`username` AS uid
FROM
    `orders`
        LEFT JOIN
    `users` ON `uid` = `users`.`id` 
UNION SELECT 
    `amount`,
    0.00,
    `payment`.`date`,
    `payment_methods`.`name`,
    `users`.`username` AS uid
FROM
    `payment`
        LEFT JOIN
    `payment_methods` ON `method` = `payment_methods`.`id`
        LEFT JOIN
    `users` ON `uid` = `users`.`id`
ORDER BY `date`")
?>

<script>
$(document).ready(function(){
  $( ".sidebar-menu li a[href^='#seo']" ).parent().addClass( "active" );
  $( ".sidebar-menu li a[href^='balance.php']" ).parent().addClass( "active" ); 
});
</script>
<link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-info-circle"></i> Balance reports
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li class="active">Balance.</li>

  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">
  <div class="box box-primary">
    <div class="box-body">
      <table class="table table-striped table-hover" >
        <thead>
          <tr>
            <th>ID</th>
            <th>Deposits</th> 
            <th>Orders</th> 
            <th>Balance</th> 
            <th>Note</th>
            <th>User</th>
            <th>Date</th>
          </tr>
        </thead>
        <tbody>
<?php 
$x = 1;
$y = 0;
while ($data = mysql_fetch_array($balance)) {
?>
          <tr><td>
          <?php echo $x;?>
          </td><td> 
          <?php echo $data['pay'];?>
          </td><td> 
          <?php echo $data['price']/100;?>
          </td><td> 
          <?php
          $y = $y + $data['pay'] - $data['price']/100;
          echo $y;?>
          </td><td> 
          <?php echo $data['oid'];?>
          </td><td> 
          <?php echo $data['uid'];?>
          </td><td> 
          <?php echo $data['date'];?>
          </td></tr>
<?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</section>
<script>
  $(function () {
    $('.table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<?php 
require 'includes/footer.php';
?>