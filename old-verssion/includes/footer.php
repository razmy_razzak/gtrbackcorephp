      </div><!-- /.content-wrapper -->
      
      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
        <!--<small>Powered by</small> <a href="http://abmegypt.net" target="_blank"><b>ABMEGYPT</b></a>-->
        </div>
        <!-- Default to the left -->
        <?php require_once'version.php';?>
        <span>&copy;<?php echo date("Y") ?>.</span> <b>SEO Panel <small>v <?php echo $version; ?></b></small>
      </footer>


    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->


    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- Select2 -->
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->
  </body>
</html>
