<?php


class Campaign
{
    private $conn;
    private $serPricePlus;
    private $extraPricePlus;
    public function __construct()
    {
        require 'includes/DbConnect.php';
        $DBCon = new DbConnect();
        $this->conn = $DBCon->getdbconnect();
        $data = mysqli_fetch_array(mysqli_query($this->conn,"SELECT * FROM `main` WHERE `id`='1'"));
        $this->serPricePlus = 1 +$data['service_price']/100;
        $this->extraPricePlus = 1 + $data['extra_price']/100;

    }

    public function createCampaignPrice( $data ){
        if($data){
            try{
                $order_num = $data['order'];
                $price_rang = $data['price_range'];
                $sql = "INSERT INTO `campain` (`ordernum`,`pricerang`) VALUES ( '$order_num','$price_rang')";
                $result = mysqli_query($this->conn, $sql );
                return ['tag' => 200 ];

            }catch (\Exception $e){
                return ['tag' => 503 ];
            }
        }
    }

    public function getAllCampapins(){
        $result= mysqli_query($this->conn , "SELECT * FROM `campain` ORDER BY `ordernum` ASC ");
        $data = array();
        while ($row = mysqli_fetch_assoc($result)){
            $data[] = $row;
        }
        return $data;

    }

    public function getAllServices(){
        $result= mysqli_query($this->conn , "SELECT * FROM `service` WHERE `status` ='1'");
        $data = array();
        while ($row = mysqli_fetch_assoc($result)){
            $panel_price = $row['panel_price']/100;
            $row['panel_price'] = ($row['price'] !=0 )? $row['price']/100:$panel_price*$this->serPricePlus;
            $data[] = $row;
        }
        return $data;
    }

    public function createChildCampin( $data ){
        if($data){
            $campi_id = $data['campin_id'];
            $service_id = $data['service_id'];
            $extra_id =  $data['extra_id'];
            $price =  $data['price'];
            $qty =  $data['qty_service'];

            $data_one = mysqli_query($this->conn ,"SELECT SUM(price) as amt FROM `campn_services` WHERE `campin_id`='$campi_id'");
            $row = mysqli_fetch_assoc($data_one);
            $child_price = $row['amt'] + $price;

            $data_two = mysqli_fetch_array(mysqli_query($this->conn,"SELECT `pricerang` FROM `campain` WHERE `id`='$campi_id'"));
            $limi = $data_two['pricerang'];

            if($child_price <= $limi ){
                $sql = "INSERT INTO `campn_services` (`campin_id`,`service_id`,`extra_id`,`price`, `qty_service`) VALUES ( '$campi_id','$service_id','$extra_id','$price','$qty')";
                $result = mysqli_query($this->conn, $sql );
                return ['tag' => 200 ];
            }
            else{
                return ['tag' => 405 ];
            }
        }
        else{
            return ['tag' => 503 ];
        }
    }

    public function deletecCampinByID( $id ){
        if( $id ){
            $sql1 =  "DELETE FROM `campn_services` WHERE `campin_id`='$id'";
            $retval1 = mysqli_query($this->conn, $sql1 );
            if( $retval1 ){
                $sql2 =  "DELETE FROM `campain` WHERE `id`='$id'";
                $retval2 = mysqli_query($this->conn, $sql2 );
            }
            return true;
        }
        else{
            return false;
        }
    }

    public function getChildServicesById( $camp_id ){
        if( $camp_id ){
            $result= mysqli_query($this->conn , "SELECT * FROM `campn_services` WHERE `campin_id`='$camp_id'");
            $data = array();
            while ($row = mysqli_fetch_assoc($result)){
                  $data[] = $row;
            }
            return $data;
        }
    }

    public function getServiceDataById( $service_id){
        if($service_id){
            $data_two = mysqli_fetch_array(mysqli_query($this->conn,"SELECT * FROM `service` WHERE `id`='$service_id'"));
            return $data_two;

        }
    }

    public function getExtrasById( $extra_id ){
        if( $extra_id ){
            $data_two = mysqli_fetch_array(mysqli_query($this->conn,"SELECT * FROM `extras` WHERE `id`='$extra_id'"));
            return $data_two;
        }
    }

}