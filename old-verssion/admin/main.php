<?php 
require 'includes/header.php';
?>

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='main.php']" ).parent().addClass( "active" );
});
</script>

<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="../plugins/colorpicker/bootstrap-colorpicker.min.css">

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-list-alt"></i> Main Information
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li class="active">Main Information</li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">

<?php
// Update
if(isset($_POST['submit'])) {
  // get fields
  $site_name = mysql_real_escape_string(htmlspecialchars($_POST['site_name']));
  $color = mysql_real_escape_string(htmlspecialchars($_POST['color']));
  $analytics = mysql_real_escape_string(htmlspecialchars($_POST['analytics']));
  $service_price = mysql_real_escape_string(htmlspecialchars($_POST['service']));
  $extra_price = mysql_real_escape_string(htmlspecialchars($_POST['extra']));

  //insert data
  $sql = mysql_query("UPDATE `main` SET
                      `site_name`='$site_name',
                      `service_price`='$service_price',
                      `extra_price`='$extra_price',
                      `color`='$color',
                      `analytics`='$analytics'
                      WHERE `id`='1'");
  //test
  if(!$sql){
      die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysql_error() . "<div>");
  }
  echo "<div class=\"alert alert-success\" role=\"alert\">Main Information Successfully Updated!</div>";
}

// Get data
$data = mysql_query("SELECT * FROM `main` WHERE `id`='1'");
$row = mysql_fetch_array($data);
  $site_name = $row['site_name'];
  $service_price = $row['service_price'];
  $extra_price = $row['extra_price'];
  $color = $row['color'];
  $analytics = $row['analytics'];
?>

  <div class="box box-primary">
    <div class="box-body">
      <form method="post" action="main.php">
        <div class="form-group">
          <label for="site_name">Site Name</label>
          <input type="text" class="form-control" id="site_name" name="site_name" value="<?php echo $site_name ?>" placeholder="Site Name...">
        </div>
        <div class="form-group">
            <label for="color">Main color</label>
            <div class="input-group my-colorpicker colorpicker-element">
              <div class="input-group-addon">
                <i style="background-color: rgb(255, 0, 0);"></i>
              </div>
              <input type="text" class="form-control" id="color" placeholder="Color" name="color" value="<?php echo $color; ?>" />
            </div><!-- /.input group -->
        </div>
        <div class="form-group">
          <label for="analytics">Google analytics ID</label>
          <input type="text" class="form-control" id="analytics" name="analytics" value="<?php echo $analytics; ?>" placeholder="UA-XXXX-Y">
        </div>
        <div class="form-group">
          <label for="service_price">Services profit (percentage)</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-percent"></i></span>
            <input type="text" class="form-control" id="service_price" name="service" value="<?php echo $service_price ?>" placeholder="Service percentage add...">
          </div>
        </div>
        <div class="form-group">
          <label for="extra_price">Extra profit (percentage)</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-percent"></i></span>
            <input type="text" class="form-control" id="extra_price" name="extra" value="<?php echo $extra_price ?>" placeholder="Extra percentage add...">
          </div>
        </div>

        <input class="btn btn-primary flat" type="submit" name="submit" value="UPDATE">
      </form>
    </div>
  </div>
</section><!-- /.content -->

<!-- bootstrap color picker -->
<script src="../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<script>
  $(function () {
    $(".my-colorpicker").colorpicker();
  });
</script>

<?php 
require 'includes/footer.php';
?>