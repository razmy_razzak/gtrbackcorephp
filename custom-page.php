<?php
session_start();
require_once 'includes/connect.php';
require_once 'includes/lang.php';
// Get data
$data = mysqli_query($conn ,"SELECT * FROM `seo` WHERE `page`='home'");
$row = mysqli_fetch_array($data);
  $title = $row['title'];
  $kw = $row['kw'];
  $description = $row['description'];

$tplQuery = mysqli_query($conn ,"SELECT * FROM `tpl` WHERE `active`='1'");
if(mysqli_num_rows($tplQuery)!=1){
    $tpl = 'standard';
}else{
    $tplData = mysqli_fetch_array($tplQuery);
    $tpl = strtolower($tplData['tpl']);
}

//Get data
$data = mysqli_fetch_array(mysqli_query($conn ,"SELECT * FROM `logo` WHERE `id`='1'"));
$logo = $data[1];
$data = mysqli_query($conn ,"SELECT * FROM `main` WHERE `id` = '1'");
$row = mysqli_fetch_array($data);
  $site_name = $row['site_name'];
  $dir = $row['dir'];
  $analytics = $row['analytics'];
  $serPricePlus = $row['service_price'];
  $extraPricePlus = $row['extra_price'];
  $sPercentage = 1 + $serPricePlus/100;
  $ePercentage = 1 + $extraPricePlus/100;

$data = mysqli_query($conn ,"SELECT * FROM `footer` WHERE `id` = '1'");
$row = mysqli_fetch_array($data);
  $footer1_title = $row['footer1_title'];
  $footer1_desc = $row['footer1_desc'];
  $footer2_title = $row['footer2_title'];
  $fb_link = $row['fb_link'];
  $tw_link = $row['tw_link'];
  $gplus_link = $row['gplus_link'];
  $linkedin_link = $row['linkedin_link'];
  $footer3_title = $row['footer3_title'];
  $address = $row['address'];
  $phone = $row['phone'];
  $fax = $row['fax'];
  $mobile = $row['mobile'];
  $email = $row['email'];

$custom_pages = mysqli_query($conn ,"SELECT * FROM `custom_pages` where status ='enable'");

if(isset($_GET['id'])&& is_numeric( $_GET['id'])){
    $id = mysqli_real_escape_string($conn , htmlspecialchars($_GET['id']));
}else{
  echo "<script type='text/javascript'> document.location = 'index.php'; </script>";
}

require_once 'templates/'.$tpl.'/custom-page.php';
