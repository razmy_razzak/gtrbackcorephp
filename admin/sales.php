<?php
require 'includes/header.php';
?>

<!-- DataTables -->
<link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#reports']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='sales.php']" ).parent().addClass( "active" );
  
});
</script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-pie-chart"></i> Daily sales reports
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li>Reprots</li>
    <li class="active">Daily sales reports</li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">  <div class="row">
    <div class="col-md-12">
      <div class="box box-default">
        <div class="panel-body">
          <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th>Date</th>
                <th>Orders amount</th>
                <th>Orders income</th>
                <th>Total charge</th>
              </tr>
            </thead>
            <tbody>
<?php
$sql = mysqli_query($conn , "
SELECT  DATE(`orders`.`date`) AS `dater`,
COUNT(*) AS `orders-amount`,
SUM(`orders`.`price`) AS `orders-income`,
(SELECT SUM(`amount`) FROM `payment` WHERE DATE(`dater`)=DATE(`payment`.`date`)) AS `total_charge`
FROM `orders`
GROUP BY `dater`
ORDER BY `id` DESC
");

while ($row = mysqli_fetch_array($sql)) {
  echo '<tr><td>';
  echo $row['dater'];
  echo '</td><td>';
  echo $row['orders-amount'];
  echo '</td><td>';
  echo $currency." ".$row['orders-income']/100;
  echo '</td><td>';
  echo $currency." ".$row['total_charge']*1;
  echo '</td></tr>';
}
?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<script>
  $(function () {
    $('.table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<?php
  include 'includes/footer.php';
?>