<!-- contact
================================================== -->
<section id="contact" class="s-contact">

    <div class="row section-header" data-aos="fade-up">
        <div class="col-full">
            <h3 class="subhead subhead--light">Contact Us</h3>
            <h1 class="display-1 display-1--light">Get in touch and let's make something great together. Let's turn your idea on an even greater product.</h1>
        </div>
    </div> <!-- end section-header -->

    <div class="row">

        <div class="col-full contact-main" data-aos="fade-up">
            <p>
                <a href="mailto:#0" class="contact-email">info@gtrsocials.com</a>
                <span class="contact-number">785317107  /  7853171073 / 7853171073</span>
            </p>
        </div> <!-- end contact-main -->

    </div> <!-- end row -->

    <div class="row">

        <div class="col-five tab-full contact-secondary" data-aos="fade-up">
            <h3 class="subhead subhead--light">Where To Find Us</h3>
            35 woodrose lane, 2nd floor Staten Island NY
            <p class="contact-address">
                35 woodrose lane,<br>
                2nd floor Staten, CA<br>
                Island NY
            </p>
        </div> <!-- end contact-secondary -->

        <div class="col-five tab-full contact-secondary" data-aos="fade-up">
            <h3 class="subhead subhead--light">Follow Us</h3>

            <ul class="contact-social">
                <li>
                    <a href="#0"><i class="fab fa-facebook"></i></a>
                </li>
                <li>
                    <a href="#0"><i class="fab fa-twitter"></i></a>
                </li>
                <li>
                    <a href="#0"><i class="fab fa-instagram"></i></a>
                </li>
                <li>
                    <a href="#0"><i class="fab fa-behance"></i></a>
                </li>
                <li>
                    <a href="#0"><i class="fab fa-dribbble"></i></a>
                </li>
            </ul> <!-- end contact-social -->

            <div class="contact-subscribe">
                <form id="mc-form" class="group mc-form" novalidate="true">
                    <input type="email" value="" name="EMAIL" class="email" id="mc-email" placeholder="Email Address" required="">
                    <input type="submit" name="subscribe" value="Subscribe">
                    <label for="mc-email" class="subscribe-message"></label>
                </form>
            </div> <!-- end contact-subscribe -->
        </div> <!-- end contact-secondary -->

    </div> <!-- end row -->

<?php include ('copyrights.php')?>

    <div class="cl-go-top">
        <a class="smoothscroll" title="Back to Top" href="#top"><i class="icon-arrow-up" aria-hidden="true"></i></a>
    </div>

</section> <!-- end s-contact -->