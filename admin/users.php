<?php 
require 'includes/header.php';
$user = mysqli_query($conn , "SELECT * FROM `users`")
?>
<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#seo']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='reports.php']" ).parent().addClass( "active" );
  
});
</script>
<link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-info-circle"></i> User report
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li class="active">Users</li>
  </ol>
</section><!-- /.content Header-->
<!-- Content -->
<section class="content">
  <div class="box box-primary">
    <div class="box-body">
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th> 
            <th>Email</th> 
            <th>Balance</th>
          </tr>
        </thead>
        <tbody>
<?php
while ($data= mysqli_fetch_array($user)) {
  $id =$data['id'];
?>
          <tr><td>
            <?php echo $id ?>
          </td><td>
            <?php echo $data['username']; ?>
          </td><td>
            <?php echo $data['email']; ?>
          </td><td>
            <?php
            $userFunds = mysqli_fetch_array(mysqli_query($conn , "SELECT SUM(amount) FROM `payment` WHERE uid='$id'"))[0];
            $userOrders = mysqli_fetch_array(mysqli_query($conn , "SELECT SUM(price) FROM `orders` WHERE uid='$id'"))[0];
            $userBalance = $userFunds -$userOrders/100;
            echo $userBalance; 
            ?>
          </td></tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</section>
<script>
  $(function () {
    $('.table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<?php 
require 'includes/footer.php';
?>