<?php
require_once 'includes/connect.php';
require_once 'controllers/EmailClass.php';
require"includes/lang.php";
$data = mysqli_query($conn , "SELECT * FROM `main` WHERE `id` = '1'");
$row = mysqli_fetch_array($data);
  $site_name = $row['site_name'];
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Create new account - <?php echo $site_name;?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <?php lang('create_new'); ?>
      </div><!-- /.login-logo -->
      <p class="text-center"><?php lang('gain_instant'); echo $site_name;?>.</p>

<?php
session_start();
if (isset($_SESSION['username'])) {
    $user_check = $_SESSION['username'];
    // SQL Query To Fetch Complete Information Of User
    $ses_sql=mysqli_query($conn , "SELECT `username` FROM `users` WHERE username='$user_check'");
    $row = mysqli_fetch_assoc($ses_sql);
    $login_session =$row['username'];
};
if(isset($login_session)){
    echo "<script type='text/javascript'> document.location = 'dashboard/index.php'; </script>";
}
?>



<?php

if (isset ($_POST['submit'])){

$user_name = mysqli_real_escape_string($conn , htmlspecialchars($_POST['user_name']));
$email = mysqli_real_escape_string($conn , htmlspecialchars($_POST['email']));
$password = mysqli_real_escape_string($conn , htmlspecialchars($_POST['password']));
$existUser= mysqli_fetch_array(mysqli_query($conn , "SELECT `username` from `users` WHERE `username` ='$user_name'"));
$existEmail=mysqli_fetch_array(mysqli_query($conn , "SELECT `email` from `users` WHERE `email` ='$email'"));
$emailValidator = new EmailClass();
$emailverify =  $emailValidator->verifyEmail( $_POST['email'] );
$dateTime = gmdate('ymdHis');

    if ($user_name==''){
      echo '<div class="alert alert-danger">Please enter username!</div>';
    }
    elseif ($user_name==$existUser[0]) {
        echo '<div class="alert alert-danger">Username already exists, please choose another one!</div>';
    }
    elseif (!preg_match("/^[0-9A-Za-z._-]*$/",$user_name)) {
        echo '<div class="alert alert-danger">Please enter a valid username, available characters: a-z A-Z 0-9 . _ and -</div>';
    }
    elseif (strlen($user_name) < 3) {
        echo '<div class="alert alert-danger">Username must be at least 3 characters!</div>';
    }
    elseif (strlen($user_name) > 15) {
        echo '<div class="alert alert-danger">Username must be less than 15 characters!</div>';
    }
    elseif ($email==''){
        echo '<div class="alert alert-danger">Please enter email!</div>';
    }
    elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo '<div class="alert alert-danger">Please enter a valid email!</div>';
    }
    elseif ($email==$existEmail[0]) {
      echo '<div class="alert alert-danger">Email already exists, please choose another one!</div>';
    }
    elseif ($password==''){
      echo '<div class="alert alert-danger">Please enter password!</div>';
    }
    elseif ($emailverify=='Bad'){
      echo '<div class="alert alert-danger">We are unable to verify your email Address, please choose another one</div>';
    }
    else {

      //insert data
      $sql = "INSERT INTO `users` ".
                 "(`username`, `email` ,`password` ,`status` , `date`) ".
                 "VALUES ( '$user_name','$email','$password','1', '$dateTime' )";

      //test
      $retval = mysqli_query($conn ,  $sql);
      if(! $retval )
      {
        die('<div class="alert alert-danger">'.mysqli_error($conn).'</div>');
      }
      echo '<div class="alert alert-success">Account successfully created!</div>';
      echo "<script type='text/javascript'> document.location = 'login.php'; </script>";
    }


    mysqli_close($conn);
}

?>


      <div class="login-box-body">
        <p class="login-box-msg">Gain instant access</p>
        <form action="register.php" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="<?php lang('username'); ?>" name="user_name" required />
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
              <input class="form-control" placeholder="<?php lang('email'); ?>" type="email" name="email" required />
              <span class="fa fa-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="<?php lang('password'); ?>" name="password" required />
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">

            <div class="col-xs-4 col-xs-offset-8">
              <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat"><?php lang('register'); ?></button>
            </div><!-- /.col -->
          </div>
        </form>
        <?php lang('already_member'); ?>Already a member? <a href="login.php"><?php lang('login'); ?></a>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    <div class="container text-center">
      <a href="index.php"><i class="fa fa-home" aria-hidden="true"></i> <?php lang('back_to_home'); ?>Back to home</a>
    </div>
  </body>
</html>
