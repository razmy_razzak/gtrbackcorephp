<?php 
require 'includes/header.php';
?>

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#home']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='home-slideshow.php']" ).parent().addClass( "active" );
});
</script>


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="ion ion-images"></i> Slidshow
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
     <li>Home page</li>
    <li class="active">Slideshow</li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">

<?php


function imagehandler($x) {
  $path_parts = pathinfo($_FILES['image'.$x]["name"]);
  $db_file = "dist/img/sliders/" . $x . "_" . time() . "." . $path_parts['extension'];
  $target_file = "../" . $db_file;
  $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
  $uploadOk = 1;

  $check = getimagesize($_FILES['image'.$x]["tmp_name"]);
  if($check == false) {
    echo "<div class=\"alert alert-danger\" role=\"alert\"><b>File#" . $x . "</b> is not an image.</div>";
    $uploadOk = 0;
  }

  // Check file size
  if ($_FILES['image'.$x]["size"] > 3145728) {
    echo "<div class=\"alert alert-danger\" role=\"alert\"><b>Image#" . $x . "</b> error, file is too large.</div>";
    $uploadOk = 0;
  }
  // Allow certain file formats
  if($imageFileType != "jpg" && $imageFileType != "jpeg") {
    echo "<div class=\"alert alert-danger\" role=\"alert\"><b>Image#" . $x . "</b> error, only JPG, JPEG, PNG & GIF files are allowed.</div>";
    $uploadOk = 0;
  }
  // Check if $uploadOk is set to 0 by an error
  if ($uploadOk == 0) {
      echo "<div class=\"alert alert-danger\" role=\"alert\">Sorry, <b>Image#" . $x . "</b>  was not uploaded.</div>";
  // if everything is ok, try to upload file
  } else {
      if (move_uploaded_file($_FILES['image'.$x]["tmp_name"], $target_file)) {
        //insert data
        $sql = mysql_query("UPDATE `slideshow` SET `image_link`='$db_file' WHERE `id`='$x'");
        //test
        if(!$sql){
          die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysql_error() . "<div>");
        }
        echo "<div class=\"alert alert-success\" role=\"alert\"><b>Image#" . $x . "</b> successfully updated!</div>";

      } else {
        echo "<div class=\"alert alert-danger\" role=\"alert\"><b>Image#" . $x . "</b> error, there was an error uploading your file.</div>";
      }
  }

}

if(isset($_POST["submit"])) {
  if (isset($_POST["del1"])) {
    $sql = mysql_query("UPDATE `slideshow` SET `image_link`='' WHERE `id`='1'");
    echo "<div class=\"alert alert-success\" role=\"alert\"><b> Image#1 </b> successfully deleted!</div>";
  } else {
    if(file_exists($_FILES['image1']['tmp_name'])) {
      imagehandler('1');
    }
  }

  if (isset($_POST["del2"])) {
    $sql = mysql_query("UPDATE `slideshow` SET `image_link`='' WHERE `id`='2'");
    echo "<div class=\"alert alert-success\" role=\"alert\"><b> Image#2 </b> successfully deleted!</div>";
  } else {
    if(file_exists($_FILES['image2']['tmp_name'])) {
      imagehandler('2');
    }
  }

  if (isset($_POST["del3"])) {
    $sql = mysql_query("UPDATE `slideshow` SET `image_link`='' WHERE `id`='3'");
    echo "<div class=\"alert alert-success\" role=\"alert\"><b> Image#3 </b> successfully deleted!</div>";
  } else {
    if(file_exists($_FILES['image3']['tmp_name'])) {
      imagehandler('3');
    }
  }
}


// Reset
if(isset($_GET['reset'])) {
  $sql = mysql_query("INSERT INTO `slideshow` (`id`, `image_link`)
                      VALUES ('1', 'dist/img/sliders/img1.jpg'),
                      ('2', 'dist/img/sliders/img2.jpg'),
                      ('3', 'dist/img/sliders/img3.jpg')
                      ON DUPLICATE KEY UPDATE `id`=VALUES(`id`),
                      `image_link`=VALUES(`image_link`);
                      ");
  //test
  if(!$sql){
    die("<div class=\"alert alert-danger\" role=\"alert\">Could not reset data: " . mysql_error() . "<div>");
  }
  echo "<div class=\"alert alert-success\" role=\"alert\">Slideshow Successfully Reset!</div>";
}

// Get data
$data = mysql_query("SELECT * FROM `slideshow` WHERE `id`='1'");
$row = mysql_fetch_array($data);
$link1 = $row[1];

$data = mysql_query("SELECT * FROM `slideshow` WHERE `id`='2'");
$row = mysql_fetch_array($data);
$link2 = $row[1];

$data = mysql_query("SELECT * FROM `slideshow` WHERE `id`='3'");
$row = mysql_fetch_array($data);
$link3 = $row[1];

?>

  <div class="box box-primary">
    <div class="box-body">

      <form method="post" action="home-slideshow.php" enctype="multipart/form-data">
        <div class="form-group margin-top-40">
          <h3><li class="ion ion-image"></li> Image <b>#1</b></h3>
        </div>
        <div class="form-group col-md-12">
          <?php if(!empty($link1)) { ?>
          <img class="img-responsive" src="../<?php echo $link1 ?>" width="200px">
          <div class="checkbox">
            <label>
              <input name="del1" type="checkbox" /> <b class="text-red">Delete Image#1</b>
            </label>
          </div>
          <?php } ?>
          <label for="image1">Choose image</label>
          <input type="file" accept="image/jpeg" class="form-control" name="image1" id="image1" />
          <span class="label label-info">Best dimensions: 1600 px * 525 px</span><br>
          <span class="label label-warning">Only JPG/JPEG files & size less than 3 MB</span>
        </div>
        <div class="clearfix"></div>

        <div class="form-group margin-top-40">
          <h3><li class="ion ion-image"></li> Image <b>#2</b></h3>
        </div>
        <div class="form-group col-md-12">
          <?php if(!empty($link2)) { ?>
          <img class="img-responsive" src="../<?php echo $link2 ?>" width="200px">
          <div class="checkbox">
            <label>
              <input name="del2" type="checkbox"> <b class="text-red">Delete Image#2</b>
            </label>
          </div>
          <?php } ?>
          <label for="image2">Choose image</label>
          <input type="file" accept="image/jpeg" class="form-control" name="image2" id="image2">
          <span class="label label-info">Best dimensions: 1600 px * 525 px</span><br>
          <span class="label label-warning">Only JPG/JPEG files & size less than 3 MB</span>
        </div>
        <div class="clearfix"></div>

        <div class="form-group margin-top-40">
          <h3><li class="ion ion-image"></li> Image <b>#3</b></h3>
        </div>
        <div class="form-group col-md-12">
          <?php if(!empty($link3)) { ?>
          <img class="img-responsive" src="../<?php echo $link3 ?>" width="200px">
          <div class="checkbox">
            <label>
              <input name="del3" type="checkbox"> <b class="text-red">Delete Image#3</b>
            </label>
          </div>
          <?php } ?>
          <label for="image3">Choose image</label> <span><small class="text-blue">(Only JPG/JPEG files & size less than 3 MB)</small></span>
          <input type="file" accept="image/jpeg" class="form-control" name="image3" id="image3">
          <span class="label label-info">Best dimensions: 1600 px * 525 px</span><br>
          <span class="label label-warning">Only JPG/JPEG files & size less than 3 MB</span>
        </div>
        <div class="clearfix"></div>

        <input type="submit" class="btn btn-primary flat margin-top-20" name="submit" value="UPDATE">
        <a class="btn btn-warning flat margin-top-20" href="home-slideshow.php?reset">RESET DATA</a>

      </form>
    </div>
  </div>
</section><!-- /.content -->



<?php 
require 'includes/footer.php';
?>