<?php
require_once 'controllers/Services.php';
if($_GET['campin_id']){

    $services = new Services();
    $result = $services->getCampinDataById( $_GET['campin_id'] );
    if( $result == 503 || $result == 404 ){
        echo json_encode(array('error' => 'Not found'));
    }
    else{
        echo json_encode(array('success' => $result));
    }


}