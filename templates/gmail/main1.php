<?php
require_once 'header.php';
?>
<!-- revolution slider css-->
<link rel="stylesheet" href="plugins/revolution-slider/rs-plugin/css/settings.css" type="text/css" media="screen">
<!--[if lt IE 9]><link rel="stylesheet" href="plugins/revolution-slider/rs-plugin/css/settings-ie8.css" type="text/css" media="screen"><![endif]-->

<script type="text/javascript" src="plugins/app.js"></script>
<!-- revolution slider js-->
<script type="text/javascript" src="plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="dist/js/revolution-slider.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        App.init();
        RevolutionSlider.initRSfullWidth();
    });
</script>



<!--=== Slider ===-->
<?php if(!empty($images)) { ?>
<div class="fullwidthbanner-container tp-banner-container">
    <div class="fullwidthabnner tp-banner">
        <ul>
            <?php
                foreach($images as $image){
                    ?>
                    <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="300">
                        <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                        <img src="<?php echo $image ?>" />
                    </li>
                <?php    
                }?>
            
        </ul>
        <div class="tp-bannertimer tp-bottom"></div>
    </div>
</div>
<?php } ?>
<!--=== End Slider ===-->

<!--=== Purchase Block ===-->
<div class="margin-bottom-50 margin-top-100">
    <div class="container-fluid text-center">
        <p class="main-title"><?php echo $title1; ?></p>
        <p class="main-desc"><?php echo $description1; ?></p>
        <a class="btn-main" href="<?php echo $link1; ?>"><?php echo $button1; ?></a>
    </div>
</div><!--/row-->
<!-- End Purchase Block -->

<div class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-6 margin-top-100 text-center">
                <h3><?php echo $title6; ?></h3>
                <p><?php echo $description6; ?></p>
            </div>
            <div class="col-md-6">
                <img src="dist/img/dashboard.png" class="img-responsive">
            </div>
        </div>
    </div>
</div>

<div id="pricing" class="bg-gray">
    <div class="container-fluid text-center">

        <h2><?php echo $title7; ?></h2>
        <p><?php echo $description7; ?></p>
        <div class="margin-top-30 text-center">
                <a data-toggle="modal" class="btn-pricing" data-target="#services" href="#"><?php lang('service_list'); ?></a>
                <a data-toggle="modal" class="btn-pricing" data-target="#extras" href="#"><?php lang('extras_list'); ?></a>
        </div>


    </div>
</div>

<!--=== Content Part ===-->
<div class="container-fluid margin-top-100"> 
    <!-- Service Blocks -->
    <div class="row">
        <div class="col-md-4">
            <div class="service clearfix">
                <i class="fa <?php echo $icon2; ?>"></i>
                <div class="desc">
                    <h4><?php echo $title2; ?></h4>
                    <p><?php echo $description2; ?></p>
                </div>
            </div>  
        </div>
        <div class="col-md-4">
            <div class="service clearfix">
                <i class="fa <?php echo $icon3; ?>"></i>
                <div class="desc">
                    <h4><?php echo $title3; ?></h4>
                    <p><?php echo $description3; ?></p>
                </div>
            </div>  
        </div>
        <div class="col-md-4">
            <div class="service clearfix">
                <i class="fa <?php echo $icon4; ?>"></i>
                <div class="desc">
                    <h4><?php echo $title4; ?></h4>
                    <p><?php echo $description4; ?></p>
                </div>
            </div>  
        </div>              
    </div><!--/row-->
</div><!-- //End Service Blokcs -->
<?php
require_once 'footer.php';