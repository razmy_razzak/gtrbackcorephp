<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin area!</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="index.php">Login</a>
      </div><!-- /.login-logo -->

<?php

require_once 'includes/connect.php';
?>

<?php
session_start();
if (isset($_SESSION['adminname'])) {
    $user_check = $_SESSION['adminname'];
    // SQL Query To Fetch Complete Information Of User
    $ses_sql=mysqli_query($conn , "SELECT `username` FROM `admins` WHERE username='$user_check'");
    $row = mysqli_fetch_assoc($ses_sql);
    $login_session =$row['username'];
};
if(isset($login_session)){
    echo "<script type='text/javascript'> document.location = 'index.php'; </script>";
}
?>



<?php

if (isset ($_POST['submit'])){

$adminname = mysqli_real_escape_string($conn , htmlspecialchars($_POST['user_name']));
$password = mysqli_real_escape_string($conn , htmlspecialchars($_POST['password']));


    $sql="SELECT * FROM `admins` WHERE username='$adminname' AND password='$password'";
    $result=mysqli_query($conn , $sql);
    // Mysql_num_row is counting table row
    $count=mysqli_num_rows($result);


    //$user_id = mysqli_data_seek($result, 0, "id");
    //$user_status = mysqli_data_seek($result, 0, "status");

    //$user_record = mysqli_fetch_assoc ($result , 0);

    while ($record = mysqli_fetch_array($result)){
        $user_id = $record['id'];
    }

    // If result matched $myusername and $mypassword, table row must be 1 row
    if($count==1){

        // Register $myusername, $mypassword and redirect to file "login_success.php"
        //session_register("adminname");
        $_SESSION['adminname']= $adminname;
        $_SESSION['userid']= $user_id;

        echo '<div class="alert alert-success">Login OK!</div>';
        //header("Location: order.php");
        echo "<script type='text/javascript'> document.location = 'index.php'; </script>";
        //echo $_SESSION['adminname'];
    }
    else {
        echo '<div class="alert alert-danger">Wrong Username or Password!</div>';
    }


    mysqli_close($conn);
}

?>





      <div class="login-box-body">
        <p class="login-box-msg">Sign in to access</p>
        <form action="login.php" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Username" name="user_name" required />
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password" required/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">

            <div class="col-xs-4 col-xs-offset-8">
              <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="../plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
