<?php
require 'includes/header.php';
?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='reports.php']" ).parent().addClass( "active" );
  
});
</script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-bars"></i> <?php lang('reports'); ?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="dashboard.php"><i class="fa fa-dashboard"></i><?php lang('dashboard'); ?></a></li>
    <li class="active"><?php lang('reports'); ?></li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">
  <div class="box box-primary">
    <div class="panel-body">
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th><?php lang('id'); ?></th>
            <th><?php lang('created'); ?> <a data-toggle="tooltip" data-placement="top" title="GMT Date"><sup class="fa fa-info-circle text-gray"></sup></a></th>
            <th><?php lang('service'); ?></th>
            <th><?php lang('qty'); ?>.</th>
            <th><?php lang('charge'); ?></th>
            <th><?php lang('details'); ?></th>
            <th><?php lang('status'); ?></th>
            <th><?php lang('reports'); ?></th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        $('.table').DataTable({
            "columns": [
                {"data": "id"},
                {"data": "date"},
                {"data": "ser"},
                {"data": "qty"},
                {"data": "price"},
                {"data": "details"},
                {"data": "status"},
                {"data": "report"}
            ],
            "ordering": false,
            "lengthMenu": [ 10, 25, 50],
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: 'ajax_orders.php',
                type: 'POST'
            },
                    "oLanguage": {
       "sProcessing":   "<?php lang('sProcessing'); ?>",
       "sLengthMenu":   "<?php lang('sLengthMenu'); ?>  _MENU_",
       "sZeroRecords":  "<?php lang('sZeroRecords'); ?>",
       "sInfo":         "<?php lang('sInfo');?> (_START_ => _END_) <?php lang('sinfo1'); ?>  _TOTAL_ ",
       "sInfoEmpty":    "<?php lang('sInfoEmpty'); ?>",
       "sInfoFiltered": "<?php lang('sInfoFiltered');?>   _MAX_",
       "sInfoPostFix":  "<?php lang('sInfoPostFix'); ?>",
       "sSearch":       "<?php lang('sSearch'); ?>",
       "oPaginate": {
       "sFirst":    "<?php lang('sFirst'); ?>",
       "sPrevious": "<?php lang('sPrevious'); ?>",
        "sNext":     "<?php lang('sNext'); ?>",
        "sLast":     "<?php lang('sLast'); ?>"
    }
  }
        });
    });
</script>

<?php
  include 'includes/footer.php';
?>