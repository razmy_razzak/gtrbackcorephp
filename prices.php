<?php 
require 'includes/header.php';
$sPercentage = 1 + $serPricePlus/100;
$ePercentage = 1 + $extraPricePlus/100;
?>

<script>
$(document).ready(function(){
  $( ".sidebar-menu li a[href^='prices.php']" ).parent().addClass( "active" ); 
});
</script>
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-list"></i> <?php lang('prices'); ?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="dashboard.php"><i class="fa fa-dashboard"></i><?php lang('dashboard'); ?></a></li>
    <li class="active"><?php lang('prices'); ?></li>

  </ol>
</section><!-- /.content Header-->


<?php
$balance = mysqli_query($conn , "SELECT `amount`, `date`,`name`,`txn_id` FROM `payment` left join `payment_methods` on `method` = `payment_methods`.`id` WHERE uid = $uid ORDER BY `date`");
?>
<!-- Content -->
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title"><?php lang('services_price'); ?></h3>
    </div>
    <div class="box-body">
      <table class="table table-striped table-hover" >
        <thead><tr>
            <th><?php lang('code'); ?></th>
            <th><?php lang('service_description'); ?> </th>
            <th><?php lang('min_qty'); ?></th>
            <th><?php lang('price_per_1'); ?></th>
        </tr></thead>
        <?php
        $result= mysqli_query($conn , "SELECT * FROM `service` WHERE `status` = 1 ORDER BY `ordering`");
        while ($row = mysqli_fetch_assoc($result)){
            if (empty($row['price']) || $row['price'] == 0){
              $price = ($row['panel_price']*$sPercentage)/100;
            }else {
              $price = $row['price']/100;
            }
            echo '<tr><td>'
                    . '<span class="label bg-blue">'.$row['code'].'</span>'
                    . '</td><td>'
                    . $row['description']
                    . '</td><td>'
                    . $row['min_qty']
                    . '</td><td>'
                    .$currency." ".$price
                    . '</td></tr>';
        }
        ?>
      </table>
    </div>
  </div>
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title"><?php lang('extras_price'); ?></h3>
    </div>
    <div class="box-body">
      <table class="table table-striped table-hover" >
        <thead><tr>
            <th><?php lang('code'); ?></th>
            <th><?php lang('extra_description'); ?></th>
            <th><?php lang('price_per_1'); ?></th>
        </tr></thead>
        <?php
        $result= mysqli_query($conn , "SELECT * FROM `extras` WHERE `status` = '1'");
        while ($row = mysqli_fetch_assoc($result)){
            if (empty($row['price']) || $row['price'] == 0){
              $price = ($row['panel_price']*$ePercentage)/100;
            }else {
              $price = $row['price']/100;
            }
            echo '<tr><td>'
                    . '<span class="label bg-yellow">'.$row['code'].'</span>'
                    . '</td><td>'
                    . $row['description']
                    . '</td><td>'
                    .$currency." ".$price
                    . '</td></tr>';
        }
        ?>
      </table>
    </div>
  </div>
</section>
<script>
  $(function () {
    $('.table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
             "oLanguage": {
       "sProcessing":   "<?php lang('sProcessing'); ?>",
       "sLengthMenu":   "<?php lang('sLengthMenu'); ?>  _MENU_",
       "sZeroRecords":  "<?php lang('sZeroRecords'); ?>",
       "sInfo":         "<?php lang('sInfo');?> (_START_ => _END_) <?php lang('sinfo1'); ?>  _TOTAL_ ",
       "sInfoEmpty":    "<?php lang('sInfoEmpty'); ?>",
       "sInfoFiltered": "<?php lang('sInfoFiltered');?>   _MAX_",
       "sInfoPostFix":  "<?php lang('sInfoPostFix'); ?>",
       "sSearch":       "<?php lang('sSearch'); ?>",
       "oPaginate": {
       "sFirst":    "<?php lang('sFirst'); ?>",
       "sPrevious": "<?php lang('sPrevious'); ?>",
        "sNext":     "<?php lang('sNext'); ?>",
        "sLast":     "<?php lang('sLast'); ?>"
    }
  }
    });
  });
</script>
<?php 
require 'includes/footer.php';
?>