$( document ).ready(function() {
    $('.select_services').change(function() {
        var form_id = $(this).closest('form').attr('id');
        $('#total_'+form_id).val(0);
        $('#product_tot_'+form_id).val(0);
        var max_price = $('input[name=max_price_'+form_id+']').val();
        var total_price = $('input[name=total_'+form_id+']').val();
        var extra = $('#'+form_id+' .extra_ser').find(':selected').data('value');
        var elem_error = $('#error').length;
        if(parseFloat(total_price) > parseFloat(max_price) ){
            alert('too much');
            $('input[name=total_'+form_id+']').after('<stong style="color: red" class="">The total should be less than the price tag</stong>');
        }
        else {
            $('input[name=total_'+form_id+']').find('stong').remove();
        }
        var data_price = $(this).find(':selected').data('value');
        var data_qty = $(this).find(':selected').data('qty');
        $('#product_tot_'+form_id).val(data_qty);
        var serv_tot = parseFloat(data_price) * parseFloat(data_qty);
        $('input[name=service_tot_'+form_id+']').val(serv_tot);
        extra == undefined ? extra =0 : extra;
        var total = parseFloat(serv_tot) + parseFloat(extra);
        $('#total_'+form_id).val(total.toFixed(2));
        $('#etra_'+form_id).find('option').remove();
        formData = {
            'serive_id' : this.value
        };
        if(this.value != ''){
            $.post("doExtraServices.php",
                formData,
                function(data, status){
                    data = jQuery.parseJSON(data);
                    if(data.error){

                    }
                    // if success we show loder and message
                    else if (data.extras){
                        $('#etra_'+form_id).prop( "disabled", false );
                        $('#etra_'+form_id).append('<option data-value="0" value="0">Select</option>');
                        $.each(data.extras, function( index, value ) {
                             $('#etra_'+form_id).append('<option data-value="'+value.panel_price+'" value="'+value.id+'">'+value.description+' - $'+value.panel_price+'</option>');
                        } );
                    }
                });
        }
        else {
            $('#etra_'+form_id).prop( "disabled", true );
        }
    });

    $('.qty').change(function() {
        if( $('#error').length ){
            $('#error').remove();
        }
        var form_id = $(this).closest('form').attr('id');
        var extra = $('#'+form_id+' .extra_ser').find(':selected').data('value');
        var min_qty = $('#'+form_id+' .select_services').find(':selected').data('qty');
        var max_price = $('input[name=max_price_'+form_id+']').val();
        var elem_error = $('#error').length;
        var new_qty = $('input[name=product_tot_'+form_id+']').val();
        var data_price = $('#'+form_id+' .select_services').find(':selected').data('value');
        var serv_tot = parseFloat(data_price) * parseFloat(new_qty);
        console.log(min_qty);
        console.log(new_qty);
        var total = parseFloat(serv_tot) + parseFloat(extra);
        $('#total_'+form_id).val(total.toFixed(2));
        var total_price = $('input[name=total_'+form_id+']').val();
        if(parseFloat(min_qty) > parseFloat(new_qty) && elem_error ==0){
            $('input[name=total_'+form_id+']').after('<stong id="error" style="color: red" class="">minimum qty:'+min_qty+'</stong>');
        }
        if(parseFloat(total_price) > parseFloat(max_price) && elem_error ==0 ){
            alert('too much');
            $('input[name=total_'+form_id+']').after('<stong id="error" style="color: red" class="">The total should be less than the price tag</stong>');
        }
    });

    $('.extra_ser').change(function() {
        var form_id = $(this).closest('form').attr('id');
        $('#total_'+form_id).val(0);
        var data_service = $(this).find(':selected').data('value');
        var serv_total = $('input[name=service_tot_'+form_id+']').val();
        var add_exr =  parseFloat(data_service) + parseFloat(serv_total);
        if( serv_total != ''){
            $('#total_'+form_id).val(add_exr.toFixed(2) );
        }
        var max_price = $('input[name=max_price_'+form_id+']').val();
        var total_price = $('input[name=total_'+form_id+']').val();
        var elem_error = $('#error').length;
        console.log(max_price);
        console.log(total_price);

        if(parseFloat(total_price) > parseFloat(max_price) && elem_error ==0 ){
            alert('too much');
            $('input[name=total_'+form_id+']').after('<stong id="error" style="color: red" class="">The total should be less than the price tag</stong>');
        }
        else {
            $('#error').remove();
        }
    });

    $('.extra_serv_form').submit(function (e) {
        if( $('#alert').length ){
            $('#alert').remove();
        }
        e.preventDefault();
        var form_id = $(this).closest('form').attr('id');
        var max_price = $('input[name=max_price_'+form_id+']').val();
        var total_price = $('input[name=total_'+form_id+']').val();
        if($('#error').length == 0){
            var camp_id = $('#'+form_id).find('input[name=camp_id]').val();
            var service_id =  $('#'+form_id+' .select_services').find(':selected').val();
            var extra_id =  $('#'+form_id+' .extra_ser').find(':selected').val();
            var service_qty =  $('#'+form_id+' .select_services').find(':selected').data('qty');
            var price =  $('#total_'+form_id).val();
            var formData = {
                'campin_id' : camp_id,
                'service_id' : service_id,
                'extra_id' : extra_id,
                'price' : price,
                'qty_service' : service_qty
            };
            console.log(formData);
            $.post("doCampinChild.php",
                formData,
                function(data, status){
                    data = jQuery.parseJSON(data);
                    if(data.error){
                        $('#'+form_id).append('<div id="alert" class="alert alert-danger">'+data.error+'</div>');
                    }
                    // if success we show loder and message
                    else if (data.success){
                        $('#'+form_id).append('<div id="alert" class="alert alert-success">Data saved</div>');
                    }
                });
        }


    });

});