<?php
require_once 'includes/connect.php';
require"includes/lang.php";
$data = mysqli_query($conn,"SELECT * FROM `main` WHERE `id` = '1'");
$row = mysqli_fetch_array($data);
  $site_name = $row['site_name'];
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login to <?php echo $site_name;?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/style.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <?php lang('login_to_seo'); echo $site_name;?>
      </div><!-- /.login-logo -->
      <p class="text-center"><?php lang('login_to_your'); ?></p>


<?php
session_start();
if (isset($_SESSION['username'])) {
    $user_check = $_SESSION['username'];
    // SQL Query To Fetch Complete Information Of User
    $ses_sql=mysqli_query($conn,"SELECT `username` FROM `users` WHERE username='$user_check'");
    $row = mysqli_fetch_assoc($ses_sql);
    $login_session =$row['username'];
    $_SESSION['id']= $row['id'] ;
};
if(isset($login_session)){
    echo "<script type='text/javascript'> document.location = 'dashboard.php'; </script>";
}
?>



<?php

if (isset ($_POST['submit'])){

$username = mysqli_real_escape_string($conn ,htmlspecialchars($_POST['user_name']));
$password = mysqli_real_escape_string($conn ,htmlspecialchars($_POST['password']));


    $sql="SELECT * FROM `users` WHERE username='$username' AND password='$password'";
    $result=mysqli_query($conn,$sql);
    // Mysql_num_row is counting table row
    $count=mysqli_num_rows($result);


    //$user_id = mysqli_data_seek($result, 0, "id");
    //$user_status = mysqli_data_seek($result, 0, "status");

    //$user_record = mysqli_fetch_assoc ($result , 0);

    while ($record = mysqli_fetch_array($result)){
        $user_id = $record['id'];
        $user_status =$record['status'];
    }
 
    // If result matched $myusername and $mypassword, table row must be 1 row
    if($count==1 && $user_status == 1){

        // Register $myusername, $mypassword and redirect to file "login_success.php"
        //session_register("username");
        $_SESSION['username']= $username;
        $_SESSION['userid']= $user_id;

        echo '<div class="alert alert-success">Login OK!</div>';
        //header("Location: order.php");
        echo "<script type='text/javascript'> document.location = 'dashboard.php'; </script>";
        //echo $_SESSION['username'];
    }elseif($user_status === 0 ){
      echo '<div class="alert alert-danger">You Are Suspended</div>';
    }
    else {
        echo '<div class="alert alert-danger">Wrong Username or Password!</div>';
    }


    mysqli_close($conn);
}

?>





      <div class="login-box-body">
        <p class="login-box-msg"><?php lang('sign_access'); ?></p>
        <form action="login.php" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Username" name="user_name" required />
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password" required/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>

          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat"><?php lang('sign'); ?></button>
        </form>

        <div class="form-group text-right small margin-top-20">
            <a href="reset.php?username"><?php lang('forget_username'); ?></a>
             - 
            <a href="reset.php?password"><?php lang('forget_password'); ?></a>
        </div>
        <hr>
        <?php lang('not_member'); ?> <a href="register.php"><?php lang('register_now'); ?></a>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <div class="container text-center">
      <a href="index.php"><i class="fa fa-home" aria-hidden="true"></i> <?php lang('back_to_home'); ?></a>
    </div>


  </body>
</html>
