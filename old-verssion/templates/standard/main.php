
<!DOCTYPE html>
<!--[if IE 7]> <html lang="en" class="ie7"> <![endif]-->  
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title><?php echo $title ?></title>

    <!-- Meta -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="<?php echo $description ?>" />
<?php
    if (isset($kw)) {
        echo '<meta name="keywords" content="'.$kw.'" />';
    }
?>
    <meta name="author" content="SEOeStore" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />

    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

    <!-- font-awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">    

    <!-- custom css -->
    <link rel="stylesheet" href="dist/css/style1.css?v=3" />
    <link rel="stylesheet" href="dist/css/themes/color.php?v=2" id="style_color?v=2" />
    <!-- JS Global Compulsory -->     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- JS Implementing Plugins -->
    <script type="text/javascript" src="plugins/back-to-top.js"></script>
    <script type="text/javascript" src="plugins/jquery.parallax.js"></script>
    <!-- JS Customization -->
    <script type="text/javascript" src="dist/js/custom.js"></script>
    <!-- JS Page Level -->           
    <script type="text/javascript" src="dist/js/app.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
            App.initParallaxBg();
        });
    </script>
    <!--[if lt IE 9]>
        <script src="assets/js/respond.js"></script>
    <![endif]-->
</head> 
<body>
<!--=== Header ===-->
<div class="header">               
    <div class="container"> 
        <div class="row">
            <div class="col-xs-6 margin-bottom-10">
            <!-- Logo -->
            <a class="logo" href="index.php">
            <?php if(empty($logo)){
                echo '<h1>'.$site_name.'</h1>';
            }else{
                echo '<img class="img-responsive" src="'. $logo . '" alt="Logo" />';
            }?>
            </a>
            </div>
            
            <div class="col-xs-6">
            <?php if(isset($_SESSION['username'])) {
            ?>
                <div class="pull-right margin-top-30">
                    <a class="btn btn-custom hover-effect" href="dashboard.php">Client Dashboard</a>
                </div>
            <?php
            }else{
            ?>
                <div class="pull-right margin-top-30">
                    <a class="btn btn-default hover-effect" href="login.php">Login</a>
                    <a class="btn btn-custom hover-effect" href="register.php">Register</a>
                </div>
            <?php    
            }
            ?>
            </div><!-- /navbar -->
        </div>

    </div><!-- /container -->               
</div><!--/header -->      
<!--=== End Header ===-->


<?php
// Get data
$data = mysql_fetch_array(mysql_query("SELECT `image_link` FROM `slideshow` WHERE `id`='1'"));
$image1 = $data[0];
$data = mysql_fetch_array(mysql_query("SELECT `image_link` FROM `slideshow` WHERE `id`='2'"));
$image2 = $data[0];
$data = mysql_fetch_array(mysql_query("SELECT `image_link` FROM `slideshow` WHERE `id`='3'"));
$image3 = $data[0];


$data = mysql_query("SELECT * FROM `paragraphs` WHERE `id`='1'");
$row = mysql_fetch_array($data);
  $title1 = $row['title'];
  $description1 = $row['description'];
  $button1 = $row['button'];
  $link1 = 'register.php';

$data = mysql_query("SELECT * FROM `paragraphs` WHERE `id`='2'");
$row = mysql_fetch_array($data);
  $title2 = $row['title'];
  $icon2 = $row['icon'];
  $description2 = $row['description'];

$data = mysql_query("SELECT * FROM `paragraphs` WHERE `id`='3'");
$row = mysql_fetch_array($data);
  $title3 = $row['title'];
  $icon3 = $row['icon'];
  $description3 = nl2br($row['description']);

$data = mysql_query("SELECT * FROM `paragraphs` WHERE `id`='4'");
$row = mysql_fetch_array($data);
  $title4 = $row['title'];
  $icon4 = $row['icon'];
  $description4 = $row['description'];

if(isset($_SESSION['username'])) {
    $un = 1;
    $button1 = "Go to Dashboard";
    $link1 = 'dashboard.php';
}else{
    $un = 0;
}

?>

<!-- revolution slider css-->
<link rel="stylesheet" href="plugins/revolution-slider/rs-plugin/css/settings.css" type="text/css" media="screen">
<!--[if lt IE 9]><link rel="stylesheet" href="plugins/revolution-slider/rs-plugin/css/settings-ie8.css" type="text/css" media="screen"><![endif]-->

<script type="text/javascript" src="plugins/app.js"></script>
<!-- revolution slider js-->
<script type="text/javascript" src="plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="dist/js/revolution-slider.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        App.init();
        RevolutionSlider.initRSfullWidth();
    });
</script>

<!--=== choose active menu ===-->
<script>
$(document).ready(function(){
    $( ".nav li a[href^='index.php']" ).parent().addClass( "active" );
});
</script>

<!--=== Slider ===-->
<?php if(!empty($image1) || !empty($image2) || !empty($image3)) { ?>
<div class="fullwidthbanner-container tp-banner-container">
    <div class="fullwidthabnner tp-banner">
        <ul>
            
            <?php if(!empty($image1)) { ?>
            <!-- THE FIRST SLIDE -->
            <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000">
                <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                <img src="<?php echo $image1 ?>" />
            </li>
            <?php } if(!empty($image2)) { ?>
            <!-- THE SECOND SLIDE -->
            <li data-transition="3dcurtain-vertical" data-slotamount="10" data-masterspeed="300">
                <!-- THE MAIN IMAGE IN THE SECOND SLIDE -->                                               
                <img src="<?php echo $image2 ?>" />
            </li>
            <?php } if(!empty($image3)) { ?>
            <!-- THE THIRD SLIDE -->
            <li data-transition="3dcurtain-vertical" data-slotamount="10" data-masterspeed="300">
                <!-- THE MAIN IMAGE IN THE THIRD SLIDE -->                                               
                <img src="<?php echo $image3 ?>" />
            </li>
            <?php } ?>
        </ul>
        <div class="tp-bannertimer tp-bottom"></div>
    </div>
</div>
<?php } ?>
<!--=== End Slider ===-->

<!--=== Purchase Block ===-->
<div class="purchase margin-bottom-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <span><?php echo $title1; ?></span>
                <p><?php echo $description1; ?></p>
                <a class="btn-dash hover-effect" href="<?php echo $link1; ?>"><?php echo $button1; ?></a>
            </div>
        </div>
    </div>
</div><!--/row-->
<!-- End Purchase Block -->

<!--=== Content Part ===-->
<div class="container"> 
    <!-- Service Blocks -->
    <div class="row">
        <div class="col-md-4">
            <div class="service clearfix">
                <i class="fa <?php echo $icon2; ?>"></i>
                <div class="desc">
                    <h4><?php echo $title2; ?></h4>
                    <p><?php echo $description2; ?></p>
                </div>
            </div>  
        </div>
        <div class="col-md-4">
            <div class="service clearfix">
                <i class="fa <?php echo $icon3; ?>"></i>
                <div class="desc">
                    <h4><?php echo $title3; ?></h4>
                    <p><?php echo $description3; ?></p>
                </div>
            </div>  
        </div>
        <div class="col-md-4">
            <div class="service clearfix">
                <i class="fa <?php echo $icon4; ?>"></i>
                <div class="desc">
                    <h4><?php echo $title4; ?></h4>
                    <p><?php echo $description4; ?></p>
                </div>
            </div>  
        </div>              
    </div><!--/row-->
</div><!-- //End Service Blokcs -->


<?php
require 'includes/footermain.php';
?>