<?php 
require 'includes/header.php';
$user = mysqli_query($conn , "SELECT * FROM `users`")
?>
<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#users']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='users-manage.php']" ).parent().addClass( "active" );
  
});
</script>
<link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-users"></i> User manage
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li>Users</li>
    <li class="active">Manage users</li>
  </ol>
</section><!-- /.content Header-->
<!-- Content -->
<section class="content">
  <div class="box box-primary">
    <div class="box-body">
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th> 
            <th>Email</th> 
            <th>Balance</th>
            <th>Status</th>
            <th>Date <a data-toggle="tooltip" data-placement="top" title="GMT Date"><sup class="fa fa-info-circle text-gray"></sup></a></th>
          </tr>
        </thead>
        <tbody>
<?php
while ($data= mysqli_fetch_array($user)) {
  $id =$data['id'];
?>
          <tr><td>
            <?php echo $id ?>
          </td><td>
            <b><?php echo $data['username']; ?></b> <a href="user-edit.php?id=<?php echo $id;?>" data-toggle="tooltip" data-placement="top" title="Edit user"><i class="fa fa-pencil-square-o"></i>
          </td><td>
            <?php echo $data['email']; ?>
          </td><td>
            <?php
            $sql = mysqli_query($conn , "SELECT SUM(amount) FROM `payment` WHERE uid='$id'");
            $userFunds = mysqli_fetch_array($sql);
            $userFunds = $userFunds [0];
            $sql = mysqli_query($conn , "SELECT SUM(price) FROM `orders` WHERE uid='$id'");
            $userOrders = mysqli_fetch_array($sql);
            $userOrders  = $userOrders [0];
            $userBalance = $userFunds -$userOrders/100;
            echo $currency." ".$userBalance; 
            ?>
          </td>
          <td><?php 
switch ($data['status']) {
  case '0':
    echo '<span class="label label-danger">Suspended</span>';
    break;
  case '1':
    echo '<span class="label bg-green">Active</span>';
    break;
}
          ?></td>
          <td><?php
  if ($data['date'] == '0000-00-00 00:00:00') {
    $general ->toolTipClass('N/A', 'Date is note defined!', 'label label-gray');
  }else {
    $recordDate = strtotime($data['date']);
    if (gmdate("Ymd") == date("Ymd", $recordDate)){
        $time =  date("h:i A", $recordDate);
        $general ->toolTipClass($time , "Today on " . $time, 'label label-gray');
    }
    else {
        $date = date("Y M", $recordDate) . " " . intval(date("d", $recordDate));
        $fullDate = date("Y/m/d h:i A", $recordDate);
        $general ->toolTipClass($date, $fullDate, 'label label-gray');
    }
  }
          ?></td>

          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</section>
<script>
  $(function () {
    $('.table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<?php 
require 'includes/footer.php';
?>