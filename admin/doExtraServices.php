<?php
require_once 'AdminController/ExtraServices.php';

if (isset($_POST['serive_id'])) {
    $extraService = new ExtraServices();
    $data = $extraService->getServiceById( $_POST['serive_id'] );
    $ids = explode(",", $data['extras']);
    $extras = $extraService->getExtrServById( $ids );
    if ( $extras == 503){
        echo json_encode(array('error' => 'something went wrong' ));
    }
    echo json_encode(array('extras' => $extras ));
}
