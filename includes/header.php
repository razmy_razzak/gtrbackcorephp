<?php
require_once 'includes/session.php';
require 'includes/general_class.php';
include 'includes/submit.php';
require 'includes/lang.php';

$general = new general();
$admin = mysqli_query($conn ,"SELECT `email`,`api_key` FROM `admins` WHERE `id` = 1");
$data = mysqli_fetch_array($admin);
$url = 'http://panel.seoestore.net/action/api.php';
$api_key = $data['api_key'];
$email = $data['email'];
$uid = $_SESSION['userid'];
$data = mysqli_fetch_array(mysqli_query($conn , "SELECT * FROM `main` WHERE `id`='1'"));
$serPricePlus = $data['service_price'];
$extraPricePlus = $data['extra_price'];
$data = mysqli_fetch_array(mysqli_query($conn ,"SELECT SUM(amount) FROM `payment` WHERE uid='$uid'"));
$userFunds = $data[0];
$data = mysqli_fetch_array(mysqli_query($conn , "SELECT SUM(price) FROM `orders` WHERE uid='$uid'"));
$userOrders = $data[0];
$userBalance = $userFunds -$userOrders/100;

//get the currency-id that user set from main_info table
//and matches with paypal-currencies to get the currency code

$data = mysqli_query($conn , "SELECT currency_id FROM `main` WHERE `id`='1'");
$row = mysqli_fetch_array($data);
$currency_id=$row['currency_id'];

$data = mysqli_query($conn , "SELECT code FROM `paypal_currencies` where id = $currency_id");
$row = mysqli_fetch_array($data);
$currency=$row['code'];
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Select 2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/style.css?v=3">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <?php 
    $query = mysqli_query($conn , "SELECT dir FROM `main`");
    $data = mysqli_fetch_array($query);
    $active = $data['dir'];
    if ($active == 'rtl' ) {
    ?>
      <link rel="stylesheet" href="dist-rtl/css/AdminLTE-rtl.css">
      <link rel="stylesheet" href="dist-rtl/css/skins/_all-skins-rtl.min.css">
    <?php
    }else{
    ?>
      <link rel="stylesheet" href="dist/css/AdminLTE.css">
      <link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
    <?php 
    }
    ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <link rel="stylesheet" href="dist/css/custom.css">

  </head>

  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="dashboard.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><i class="fa fa-dashboard"></i></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><?php lang('dashboard'); ?></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

            

              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="dist/img/avatar1.png" class="user-image" alt="User Image">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs"><?php echo $_SESSION['username']; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <img src="dist/img/avatar1.png" class="img-circle" alt="User Image">
                    <p>
                      <?php echo $_SESSION['username']; ?>
                    </p>
                  </li>

                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="profile.php" class="btn btn-default btn-flat"><?php lang('manage_profile'); ?></a>
                    </div>
                    <div class="pull-right">
                      <a href="logout.php" class="btn btn-default btn-flat"><?php lang('sign_out'); ?></a>
                    </div>
                  </li>
                </ul>
              </li>

            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div id="user_img" class="pull-left image">
              <img src="dist/img/avatar1.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['username']; ?></p>
              <span data-toggle="tooltip" data-placement="right" title="<?php lang('balance'); ?>">
                <span id="user-balance" class="label bg-green" > <?php echo $currency." ".$userBalance;?></span>
              </span>
              <a class="text-red" href="funds-add.php"><?php lang('top_up'); ?></a>
              <!-- Status -->
            </div>
          </div>

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header"><?php lang('main_menu'); ?></li>

            <!-- Optionally, you can add icons to the links -->
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> <span><?php lang('dashboard'); ?></span></a></li>
            <li><a href="funds-add.php"><i class="fa fa-usd text-green"></i> <span><?php lang('add_fund'); ?></span></a></li>
            <li><a href="order.php"><i class="fa fa-rocket text-yellow"></i> <span><?php lang('make_order'); ?></span></a></li>
            <li><a href="reports.php"><i class="fa fa-bars text-aqua"></i> <span><?php lang('reports'); ?> </span></a></li>
            <li><a href="prices.php"><i class="fa fa-list"></i> <span><?php lang('prices'); ?></span></a></li>
            <li><a href="payments.php"><i class="fa fa-credit-card"></i> <span><?php lang('payment_history'); ?></span></a></li>
        
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">