<?php

include 'new_template_parts/header.php';
?>
    <!--Note this has to be added here otherwise landing page will show before it loads -->
    <!-- preloader
    ================================================== -->
    <div id="preloader">
        <div id="loader">
        </div>
    </div>
<?php
include 'new_template_parts/header_nav_two.php';

$custom_pages = mysqli_query($conn, "SELECT * FROM `custom_pages` WHERE id = $id");
while ($data= mysqli_fetch_array($custom_pages)) {
$title=$data['title'];
$content =$data['content'];
$link = $data['link_text'];
}
?>
<?php
if($link == 'Services') {?>
    <?php include 'services.php'?>
<?php }
if($link == 'contact') {?>
    <?php include 'contact.php'?>
<?php }
if($link == 'terms') {?>
    <?php include 'terms.php'?>
<?php }
if($link == 'faq') {?>
    <?php include 'faq.php'?>
<?php }
if($link == 'price') {?>
    <?php include 'price.php'?>
<?php }
if($link == 'about') {?>
    <?php include 'about.php'?>
<?php }
if($link == 'campaigns') {?>
    <?php include 'campaigns.php'?>
<?php }
if($link == 'refund') {?>
    <?php include 'refund.php'?>
<?php }
if($link == 'Product') {
    include 'productPage.php';
}
if( isset($_SESSION['thanks'])){
    include 'thanks_block.php';
}
?>

<?php
include 'new_template_parts/contact_us.php';
?>
    <!-- Java Script
    ================================================== -->
    <script type="text/javascript"  src="dist/js_new/jquery-3.2.1.min.js"></script>
<?php if($link == 'Product'){?>
    <script type="text/javascript"   src="dist/js_new/materialize.min.js"></script>
<?php } ?>
    <script type="text/javascript"   src="dist/js_new/jquery.scrollTo.min.js"></script>
    <script type="text/javascript"   src="dist/js_new/owl.carousel.min.js"></script>
    <script type="text/javascript"  src="dist/js_new/plugins.js"></script>
    <script type="text/javascript"  src="dist/js_new/validate.js"></script>
    <script type="text/javascript"  src="dist/js_new/main.js"></script>
    <script type="text/javascript"   src="dist/js_new/jquery.rateyo.min.js"></script>
<?php if($link == 'campaigns'){?>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.2/bootstrap-slider.min.js"></script>
<?php } ?>
    <script type="text/javascript"  src="dist/js_new/custom.js"></script>
<?php if($link == 'Product'){?>
    <script type="text/javascript"  src="dist/js_new/custom_two.js"></script>
<?php } ?>
<?php include 'new_template_parts/footer.php'; ?>