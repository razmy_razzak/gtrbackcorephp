<?php 
require 'includes/header.php';
?>

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='funds-add.php']" ).parent().addClass( "active" );
  
});
</script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-usd"></i> Add funds
  </h1>
  <ol class="breadcrumb">
    <li><a href="dashboard.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li class="active">Add funds</li>

  </ol>
</section><!-- /.content Header-->

<section class="content-header">
<?php
//if submit
if(isset($_POST['add_funds'])){
  
  $amount = mysql_real_escape_string(htmlspecialchars($_POST['amount']));
  if (!preg_match("/^[0-9]*$/",$amount) || $amount <= 0 ){
      $general -> alert('Please enter a valid amount!', 'danger');
  }else{
    $_SESSION['amount']=$amount;
    //header("location:paypal.php");
    echo '<script> location.replace("paypal.php"); </script>';
  }
}

$userid = $_SESSION['userid'];
$sql=mysql_query("SELECT * FROM `users` WHERE id='$userid'", $conn);
$row = mysql_fetch_assoc($sql);
?>
</section>

<!-- Content -->
<section class="content">
  <div class="row">
      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-body">
            <div class="text-center">
              <h1>User balance</h1>
              <p>You have <span class="label label-success"><b>$<?php echo $userBalance; ?></b></span> in your balance.</p>
              <hr>
<?php
$balanceNote = mysql_fetch_array(mysql_query("SELECT description FROM `paragraphs` WHERE `id` = 5"))[0];
echo $balanceNote;
?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
      <div class="box box-primary col-md-6">
        <div class="box-body">
          <h3>Add funds</h3>
          <hr />
          <h4>Pay using <b>PayPal</b></h4>
          <form method="post" action="funds-add.php">
            <div class="form-group">
              <label for="amount">Amount in USD:</label>
              <input type="number" class="form-control" id="amount" name="amount" placeholder="Amount" required>
            </div>
            <input type="submit" class="btn btn-primary flat btn-block" name="add_funds" value="Add Funds">
          </form>
        </div>
      </div>
    </div>
  </div>
</section><!-- /.content -->




<?php 
require 'includes/footer.php';
?>