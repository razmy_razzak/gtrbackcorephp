<?php

/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 29/06/2018
 * Time: 18:09
 */
class RatingServices
{
    private $conn;
    public function __construct()
    {
        require 'includes/DbConnect.php';
        $DBCon = new DbConnect();
        $this->conn = $DBCon->getdbconnect();
    }

    public function createRating( $data ){
        if($data){
            $date = gmdate('ymdHis');
            $user_id = $data['uid'];
            $user_data =  mysqli_fetch_array(mysqli_query($this->conn,"SELECT `username` FROM `users` WHERE `id`=$user_id"));
            $username = $user_data['username'];
            $rating = $data['reviewRating'];
            if($date && $user_id && $username && $rating){
                $sql = "INSERT INTO `rating` (`username`, `date`, `rating`) VALUES ( '$username','$date','$rating')";
                mysqli_query($this->conn, $sql );
                return ['tag' => 100 ];
            }
            else{
                return false;
            }
        }
    }

}