<?php
require_once 'includes/session.php';
    define("MyTable", "orders");

/* Useful $_POST Variables coming from the plugin */
$draw = mysqli_real_escape_string($conn , htmlspecialchars($_POST["draw"]));//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
//$orderByColumnIndex  = $_POST['order'][0]['column'];// index of the sorting column (0 index based - i.e. 0 is the first record)
//$orderBy = $_POST['columns'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
//$orderType = $_POST['order'][0]['dir']; // ASC or DESC
$start  = mysqli_real_escape_string($conn , htmlspecialchars($_POST["start"]));//Paging first record indicator.
$length = mysqli_real_escape_string($conn , htmlspecialchars($_POST['length']));//Number of records that the table can display in the current draw
/* END of POST variables */
$sql = mysqli_query($conn , "SELECT count(*) FROM `orders`");
$recordsTotal = mysqli_fetch_array($sql);
$recordsTotal = $recordsTotal[0];

//function for toolTip
function toolTip($txt,$tt,$class){
	$code = '<span data-toggle="tooltip" data-placement="top" title="'.$tt.'"><span class="'.$class.'">'.$txt.'</span></span>';
	return $code;
}

/* SEARCH CASE : Filtered data */
if(!empty($_POST['search']['value'])){
    $search = mysqli_real_escape_string($conn , htmlspecialchars($_POST['search']['value']));
    $idsArr = mysqli_query($conn , "SELECT `oid` from `orders` WHERE `oid` LIKE '%$search%' OR `links` LIKE '%$search%' OR `keywords` LIKE '%$search%' ORDER BY `id` DESC limit $start, $length");
    $sql = mysqli_query($conn , "SELECT count(*) FROM `orders` WHERE `oid` LIKE '%$search%' OR `links` LIKE '%$search%' OR `keywords` LIKE '%$search%'");
    $recordsTotal = mysqli_fetch_array($sql);
    $recordsTotal = $recordsTotal [0];
    $query = mysqli_query($conn , "SELECT
    		`o`.`oid` as `id`,
            `o`.`date`,
    		`o`.`service_id`,
    		`o`.`extras` as `extras`,
    		`o`.`quantity` as `qty`,
    		`o`.`price`,
    		`o`.`links` as `links`,
    		`o`.`keywords` as `keywords`,
    		`s`.`code` as `ser_code`,
            `s`.`description` as `ser_name`,
    		`u`.`username` as `user`
    		FROM `orders` as `o`
            LEFT JOIN `service` as `s` ON `o`.`service_id` = `s`.`id`
    		LEFT JOIN `users` as `u` ON `o`.`uid` = `u`.`id`
    		WHERE o.`oid` LIKE '%$search%' OR o.`links` LIKE '%$search%' OR o.`keywords` LIKE '%$search%'
    		ORDER BY `id` DESC
    		limit $start, $length
			");
}
/* END SEARCH */
else {
	$idsArr = mysqli_query($conn , "SELECT `oid` from `orders` ORDER BY `id` DESC limit $start, $length");
	$query = mysqli_query($conn , "SELECT
			`o`.`oid` as `id`,
			`o`.`date`,
			`o`.`service_id`,
			`o`.`extras` as `extras`,
			`o`.`quantity` as `qty`,
			`o`.`price`,
			`o`.`links` as `links`,
			`o`.`keywords` as `keywords`,
			`s`.`code` as `ser_code`,
			`s`.`description` as `ser_name`,
            `u`.`username` as `user`
			FROM `orders` as `o`
			LEFT JOIN `service` as `s` ON `o`.`service_id` = `s`.`id`
            LEFT JOIN `users` as `u` ON `o`.`uid` = `u`.`id`
			ORDER BY `id` DESC
			limit $start, $length
			");
}
$recordsFiltered = $recordsTotal;

$data = mysqli_fetch_array(mysqli_query($conn , "SELECT `email`,`api_key` FROM `admins` WHERE `id` = 1"));
$url = 'http://panel.seoestore.net/action/api.php';
$api_key = $data['api_key'];
$email = $data['email'];

//get list of IDs
$data = array();
while($row = mysqli_fetch_assoc($idsArr)){
  $data[] = $row['oid'] ;
}
$idsArr = json_encode($data);

//start API
$data = Array(
	'api_key'=>$api_key,//must included in every request
	'email'=>$email,//must included in every request
	'action'=>'orders_check',//action must included in every request
	'order_ids'=>$idsArr//action must included in every request
);

if (is_array($data)) {
	foreach ($data as $key => $value) {
	  $_post[] = $key.'='.$value;
	}
}

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, join('&', $_post));
curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
$result = curl_exec($ch);
curl_close($ch);
$apiResult = json_decode($result,true);
//print_r($result);
//End API

$data = array();
while($row = mysqli_fetch_assoc($query)){
	$oid = $row['id'];
	$ser = $row['service_id'];
	$extrasID = $row['extras'];
	unset($row['extras']);
	$ser_code = $row['ser_code'];
	unset($row['ser_code']);
	$ser_name = $row['ser_name'];
	unset($row['ser_name']);

	//modify service colomn
	$row['ser'] = '<span class="tip label bg-blue" data-toggle="tooltip" data-placement="top" title="" data-original-title="Service: '.$ser_name.'">'.$ser_code.'</span>';
	  $extrasArr = "";
	if (!empty($extrasID)){
	    $extrasArr=explode(",",$extrasID);
	    foreach ($extrasArr as $key => $value){
	        if (strpos($value, ':') !== FALSE){
	            $valueArr = explode (":",$value);
	            $extrasArr[$key]=$valueArr [1];
	            $extraQ[$record['id']][$key] = $valueArr [0];
	        }
	    }
	}
    if (!empty($extrasArr)) {
        $row['ser'].='<br /><span class="text-yellow">&#x21B3;</span>';
        foreach($extrasArr as $key => $extraID){
        $extra = mysqli_fetch_assoc(mysqli_query($conn , "SELECT * FROM `extras` WHERE id='$extraID' "));
            if (!empty($extra)){
                $row['ser'].= " ";
                $extraCode = $extra["code"];
                $row['ser'].= '<span class="tip label label-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Extra: '.$extra["description"].'">'.$extraCode.'</span>';
            }
        }
    }

    //modify price colomn
	$row['price'] = '$'.$row['price']/100;

	//modify details colomn
	$links = $row['links'];
	unset($row['links']);
	$keywords = $row['keywords'];
	unset($row['keywords']);
	$row['details'] = '
            <a class="details-a" data-toggle="modal" data-target="#links'.$oid.'" href="#">
                <span class="tip details-tip" data-toggle="tooltip" data-placement="top" title="Links"><span class="circle bg-aqua"><i class="fa fa-link"></i><span></span></a>
            <div class="modal fade" tabindex="-1" role="dialog" id="links'.$oid.'">
                <div class="modal-dialog">
                    <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Links for order ID: '.$oid.'</h4>
                          </div>
                        <div class="modal-body">
                            <pre class="flat">'.$links.'</pre>
                        </div>
                    </div>
                </div>
            </div>
            
            <a class="details-a" data-toggle="modal" data-target="#kw'.$oid.'" href="#">
                <span class="tip details-tip" data-toggle="tooltip" data-placement="top" title="Keywords"><span class="circle bg-aqua"><i class="fa fa-anchor"></i></span></span></a>
            <div class="modal fade" tabindex="-1" role="dialog" id="kw'.$oid.'">
                <div class="modal-dialog">
                    <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Keywords for order ID: '.$oid.'</h4>
                          </div>
                        <div class="modal-body">
                            <pre class="flat">'.$keywords.'</pre>
                        </div>
                    </div>
                </div>
            </div>
            ';

    //modify status colomn
    foreach ($apiResult as $value) {
    	if ($value['order_id'] == $row['id']){
    		switch ($value['status']) {
    			case 'Pending':
    				$row['status'] = '<span class="label bg-gray">Pending</span>';
    				# code...
    				break;
    			case 'Processing':
    				$row['status'] = '<span class="label bg-blue">Processing</span>';
    				# code...
    				break;
    			case 'Completed':
    				$row['status'] = '<span class="label bg-green">Completed</span>';
    				# code...
    				break;
    			case 'Canceled':
    				$row['status'] = '<span class="label bg-red">Canceled</span>';
    				# code...
    				break;
    		}
    		if(!empty($value['report'])) {
    			$row['report'] = '/reports/'.$value['report'];
    			$row['report'] = '<a class="btn btn-sm flat bg-green" target="_blank" href="/reports/'.$value['report'] . '" ><i class="fa fa-download"></i></a>'; 
    		}else {
    			$row['report'] = '';
    		}
    		break;
    	}
    }

    //modify Date colomn
	if ($row['date'] == '0000-00-00 00:00:00' || empty($row['date'])) {
		$row['date'] = toolTip('N/A', 'Date is note defined!', 'label label-gray');
	}else {
		$recordDate = strtotime($row['date']);
		if (gmdate("Ymd") == date("Ymd", $recordDate)){
		    $time =  date("h:i A", $recordDate);
		    $row['date'] = toolTip($time , "Today on " . $time, 'label label-gray');
		}
		else {
		    $date = date("Y M", $recordDate) . " " . intval(date("d", $recordDate));
		    $fullDate = date("Y/m/d h:i A", $recordDate);
		    $row['date'] = toolTip($date, $fullDate, 'label label-gray');
		}
	}

	//modify ID colomn
    $row['id'] = '#'.$row['id'];

    //add record to $data array
    $data[] = $row ;
}
//}

/* Response to client before JSON encoding */
$response = array(
    "draw" => intval($draw),
    "recordsTotal" => $recordsTotal,
    "recordsFiltered" => $recordsFiltered,
    "data" => $data
);

echo json_encode($response);