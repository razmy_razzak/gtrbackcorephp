<?php
require_once 'controllers/Login.php';
if(isset($_POST['username'])){
    $data=[
        'username' => $_POST['username'],
        'password' => $_POST['password']
    ];

    $login = new Login();
    $result = $login->login( $data );
    if( $result && isset($result['msg']) ){
        echo json_encode(array('msg'=>$result['msg'],'id' =>$result['id'] ));
    }
    elseif ($result && isset($result['error'])){
        echo json_encode(array('error'=>$result['error']));
    }

}



