<?php 
require 'includes/header.php';
?>


<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php lang('edit_user'); ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i><?php lang('home'); ?></a></li>
        <li class="active"><?php lang('profile'); ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<?php
if(isset($_POST['submit'])) {
  // get fields
  $username = mysqli_real_escape_string($conn ,htmlspecialchars($_POST['username']));
  $email = mysqli_real_escape_string($conn ,htmlspecialchars($_POST['email']));
  $pwd = mysqli_real_escape_string($conn ,htmlspecialchars($_POST['password']));

  //insert data
  if(empty($pwd)){
    $sql = mysqli_query($conn ,"UPDATE `users` SET `email`='$email' WHERE `id`='$uid' AND `username`='$username'");
  }else{
    $sql = mysqli_query($conn ,"UPDATE `users` SET `email`='$email',`password`='$pwd' WHERE `id`='$uid' AND `username`='$username'");
  }

  //test
  if(!$sql){
      die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysqli_error($conn) . "<div>");
  }
  echo "<div class=\"alert alert-success\" role=\"alert\">User Details Successfully Updated!</div>";
}


  $sql = mysqli_query($conn ,"SELECT * FROM `users` WHERE `id`='$uid'");
    $data = mysqli_fetch_array($sql);
    $id = $data['id'];
    $uname = $data['username'];
    $email = $data['email'];
    $sstatus = $data['status'];
?>


        <div class="box box-primary">
            <div class="box-body">
                <form action="" method="post">
                <div class="box-body">
          <div class="form-group">
            <label for="username"><?php lang('username'); ?></label>
            <input class="form-control" type= "text" id="username" name="username" value="<?php echo $uname; ?>" readonly>
          </div>
          <div class="form-group">
            <label for="email"><?php lang('email'); ?></label>
            <input class="form-control" type= "text" id="email" name="email" value="<?php echo $email; ?>">
          </div>
            <div class="form-group">
                    <label for="password"><?php lang('reset'); ?></label> <span class="text-red"><?php lang('keep_empty'); ?>
                    <input class="form-control" type= "text" id="password" name="password" value="">
                </div>
                

                <input class="btn btn-primary flat" type="submit" name="submit" value="<?php lang('update'); ?>" />
            </div>
        </form>
        </div>
    </div>

</section><!-- /.content -->



<?php 
require 'includes/footer.php';
?>