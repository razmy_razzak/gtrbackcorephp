<?php
require_once 'header.php';
// Get data
$data = mysqli_fetch_array(mysqli_query($conn , "SELECT `image_link` FROM `slideshow` WHERE `id`='1'"));
$image1 = $data[0];
$data = mysqli_fetch_array(mysqli_query($conn , "SELECT `image_link` FROM `slideshow` WHERE `id`='2'"));
$image2 = $data[0];
$data = mysqli_fetch_array(mysqli_query($conn , "SELECT `image_link` FROM `slideshow` WHERE `id`='3'"));
$image3 = $data[0];


$data = mysqli_query($conn , "SELECT * FROM `paragraphs` WHERE `id`='1'");
$row = mysqli_fetch_array($data);
  $title1 = $row['title'];
  $description1 = $row['description'];
  $button1 = $row['button'];
  $link1 = 'register.php';

$data = mysqli_query($conn , "SELECT * FROM `paragraphs` WHERE `id`='2'");
$row = mysqli_fetch_array($data);
  $title2 = $row['title'];
  $icon2 = $row['icon'];
  $description2 = $row['description'];

$data = mysqli_query($conn , "SELECT * FROM `paragraphs` WHERE `id`='3'");
$row = mysqli_fetch_array($data);
  $title3 = $row['title'];
  $icon3 = $row['icon'];
  $description3 = nl2br($row['description']);

$data = mysqli_query($conn , "SELECT * FROM `paragraphs` WHERE `id`='4'");
$row = mysqli_fetch_array($data);
  $title4 = $row['title'];
  $icon4 = $row['icon'];
  $description4 = $row['description'];

if(isset($_SESSION['username'])) {
    $un = 1;
    $button1 = "Go to Dashboard";
    $link1 = 'dashboard.php';
}else{
    $un = 0;
}

?>

<!-- revolution slider css-->
<link rel="stylesheet" href="plugins/revolution-slider/rs-plugin/css/settings.css" type="text/css" media="screen">
<!--[if lt IE 9]><link rel="stylesheet" href="plugins/revolution-slider/rs-plugin/css/settings-ie8.css" type="text/css" media="screen"><![endif]-->

<script type="text/javascript" src="plugins/app.js"></script>
<!-- revolution slider js-->
<script type="text/javascript" src="plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="dist/js/revolution-slider.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        App.init();
        RevolutionSlider.initRSfullWidth();
    });
</script>

<!--=== choose active menu ===-->
<script>
$(document).ready(function(){
    $( ".nav li a[href^='index.php']" ).parent().addClass( "active" );
});
</script>

<!--=== Slider ===-->
<?php if(!empty($image1) || !empty($image2) || !empty($image3)) { ?>
<div class="fullwidthbanner-container tp-banner-container">
    <div class="fullwidthabnner tp-banner">
        <ul>
            
            <?php if(!empty($image1)) { ?>
            <!-- THE FIRST SLIDE -->
            <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000">
                <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                <img src="<?php echo $image1 ?>" />
            </li>
            <?php } if(!empty($image2)) { ?>
            <!-- THE SECOND SLIDE -->
            <li data-transition="3dcurtain-vertical" data-slotamount="10" data-masterspeed="300">
                <!-- THE MAIN IMAGE IN THE SECOND SLIDE -->                                               
                <img src="<?php echo $image2 ?>" />
            </li>
            <?php } if(!empty($image3)) { ?>
            <!-- THE THIRD SLIDE -->
            <li data-transition="3dcurtain-vertical" data-slotamount="10" data-masterspeed="300">
                <!-- THE MAIN IMAGE IN THE THIRD SLIDE -->                                               
                <img src="<?php echo $image3 ?>" />
            </li>
            <?php } ?>
        </ul>
        <div class="tp-bannertimer tp-bottom"></div>
    </div>
</div>
<?php } ?>
<!--=== End Slider ===-->

<!--=== Purchase Block ===-->
<div class="purchase margin-bottom-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <span><?php echo $title1; ?></span>
                <p><?php echo $description1; ?></p>
                <a class="btn-dash hover-effect" href="<?php echo $link1; ?>"><?php echo $button1; ?></a>
            </div>
        </div>
    </div>
</div><!--/row-->
<!-- End Purchase Block -->

<!--=== Content Part ===-->
<div class="container"> 
    <!-- Service Blocks -->
    <div class="row">
        <div class="col-md-4">
            <div class="service clearfix">
                <i class="fa <?php echo $icon2; ?>"></i>
                <div class="desc">
                    <h4><?php echo $title2; ?></h4>
                    <p><?php echo $description2; ?></p>
                </div>
            </div>  
        </div>
        <div class="col-md-4">
            <div class="service clearfix">
                <i class="fa <?php echo $icon3; ?>"></i>
                <div class="desc">
                    <h4><?php echo $title3; ?></h4>
                    <p><?php echo $description3; ?></p>
                </div>
            </div>  
        </div>
        <div class="col-md-4">
            <div class="service clearfix">
                <i class="fa <?php echo $icon4; ?>"></i>
                <div class="desc">
                    <h4><?php echo $title4; ?></h4>
                    <p><?php echo $description4; ?></p>
                </div>
            </div>  
        </div>              
    </div><!--/row-->
</div><!-- //End Service Blokcs -->


<?php
require 'footer.php';
?>