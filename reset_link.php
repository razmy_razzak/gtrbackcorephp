<?php
require_once 'includes/connect.php';
require_once 'controllers/EmailClass.php';
require_once 'controllers/ResetServices.php';
require"includes/lang.php";
$data = mysqli_query($conn , "SELECT * FROM `main` WHERE `id` = '1'");
$row = mysqli_fetch_array($data);
$site_name = $row['site_name'];
$hash = isset($_GET['hash'])? $_GET['hash'] : '';

$resetService = new ResetServices();
$result = $resetService->getUserDataByHash($hash);
$user_id = isset($result['id']) ? $result['id'] : '';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Username or Password Reset - <?php echo $site_name;?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition login-page">

<?php
if (is_null($result)){
    echo '<div class="alert alert-danger">Reset hash not found!</div>';
}
else{

}

if (isset ($_POST['submit'])){
    if( $_POST['password'] == $_POST['password_conf'] && $_POST['user_id'] != ''){
        $update_user = $resetService->updatePassword( $_POST['user_id'], $_POST['password'] );
        if($update_user){
        }
    }
    else{
        echo '<div class="alert alert-danger">Password and confirmation password Not matched!</div>';
    }
}

?>
<div class="login-box">
    <div class="login-logo">
        Password Reset
    </div><!-- /.login-logo -->
    <p class="text-center"><?php lang('gain_instant'); echo $site_name;?>.</p>
    <div class="login-box-body">
        <p class="login-box-msg">Reset Form</p>
        <form action="reset_link.php" method="post">
            <div class="form-group has-feedback">
                <input type="hidden" name="user_id" value="<?php echo $user_id ?>" />
                <input class="form-control" placeholder="<?php lang('password'); ?>" type="password" name="password" required/>
            </div>
            <div class="form-group has-feedback">
                <input class="form-control" placeholder="Confirm Password" type="password" name="password_conf" required/>
            </div>
            <div class="row">

                <div class="col-xs-4 col-xs-offset-8">
                    <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                </div><!-- /.col -->
            </div>
        </form>
        <?php lang('already_member'); ?><a href="login.php"><?php lang('login'); ?></a>
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
<div class="container text-center">
    <a href="index.php"><i class="fa fa-home" aria-hidden="true"></i> <?php lang('back_to_home'); ?>Back to home</a>
</div>
</body>
</html>
