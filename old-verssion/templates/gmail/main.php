
<!DOCTYPE html>
<!--[if IE 7]> <html lang="en" class="ie7"> <![endif]-->  
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title><?php echo $title ?></title>

    <!-- Meta -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="<?php echo $description ?>" />
<?php
    if (isset($kw)) {
        echo '<meta name="keywords" content="'.$kw.'" />';
    }
?>
    <meta name="author" content="SEOeStore" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />

    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

    <!-- font-awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">    

    <!-- custom css -->
    <link rel="stylesheet" href="dist/css/style-gmail.css" />
    <link rel="stylesheet" href="dist/css/themes/color.php" id="style_color?v=0" />
    <!-- JS Global Compulsory -->     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- JS Implementing Plugins -->
    <script type="text/javascript" src="plugins/back-to-top.js"></script>
    <script type="text/javascript" src="plugins/jquery.parallax.js"></script>
    <!-- JS Customization -->
    <script type="text/javascript" src="dist/js/custom.js"></script>
    <!-- JS Page Level -->           
    <script type="text/javascript" src="dist/js/app.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
            App.initParallaxBg();
        });
    </script>
    <!--[if lt IE 9]>
        <script src="assets/js/respond.js"></script>
    <![endif]-->
</head> 
<body>
<!--=== Header ===-->
<div class="header">               
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-xs-6 margin-bottom-10 margin-top-20">
            <!-- Logo -->
            <a class="logo" href="index.php">
            <?php if(empty($logo)){
                echo '<h1>'.$site_name.'</h1>';
            }else{
                echo '<img class="img-responsive" src="'. $logo . '" alt="Logo" />';
            }?>
            </a>
            </div>
            
            <div class="col-xs-6">
            <?php if(isset($_SESSION['username'])) {
            ?>
                <div class="right-top">
                    <a class="btn-top btn-register" href="dashboard.php">Dashboard</a>
                </div>
            <?php
            }else{
            ?>
                <div class="right-top">
                    <a class="btn-top" href="login.php">SIGN IN</a>
                    <a class="btn-top btn-register" href="register.php">CREATE AN ACCOUNT</a>
                </div>

            <?php    
            }
            ?>
            </div><!-- /navbar -->
        </div>

    </div><!-- /container -->               
</div><!--/header -->      
<!--=== End Header ===-->



<!-- revolution slider css-->
<link rel="stylesheet" href="plugins/revolution-slider/rs-plugin/css/settings.css" type="text/css" media="screen">
<!--[if lt IE 9]><link rel="stylesheet" href="plugins/revolution-slider/rs-plugin/css/settings-ie8.css" type="text/css" media="screen"><![endif]-->

<script type="text/javascript" src="plugins/app.js"></script>
<!-- revolution slider js-->
<script type="text/javascript" src="plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="dist/js/revolution-slider.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        App.init();
        RevolutionSlider.initRSfullWidth();
    });
</script>



<!--=== Slider ===-->
<?php if(!empty($images)) { ?>
<div class="fullwidthbanner-container tp-banner-container">
    <div class="fullwidthabnner tp-banner">
        <ul>
            <?php
                foreach($images as $image){
                    ?>
                    <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="300">
                        <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                        <img src="<?php echo $image ?>" />
                    </li>
                <?php    
                }?>
            
        </ul>
        <div class="tp-bannertimer tp-bottom"></div>
    </div>
</div>
<?php } ?>
<!--=== End Slider ===-->

<!--=== Purchase Block ===-->
<div class="margin-bottom-50 margin-top-100">
    <div class="container-fluid text-center">
        <p class="main-title"><?php echo $title1; ?></p>
        <p class="main-desc"><?php echo $description1; ?></p>
        <a class="btn-main" href="<?php echo $link1; ?>"><?php echo $button1; ?></a>
    </div>
</div><!--/row-->
<!-- End Purchase Block -->

<div class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-6 margin-top-100 text-center">
                <h3><?php echo $title6; ?></h3>
                <p><?php echo $description6; ?></p>
            </div>
            <div class="col-md-6">
                <img src="dist/img/dashboard.png" class="img-responsive">
            </div>
        </div>
    </div>
</div>

<div id="pricing" class="bg-gray">
    <div class="container-fluid text-center">

        <h2><?php echo $title7; ?></h2>
        <p><?php echo $description7; ?></p>
        <div class="margin-top-30">
            <div class="col-sm-5 col-sm-offset-1 col-md-3 col-md-offset-3 text-right">
                <a data-toggle="modal" class="btn-pricing" data-target="#services" href="#">Service list & prices</a>
            </div>
            <div class="col-sm-5 col-md-3 text-left">
                <a data-toggle="modal" class="btn-pricing" data-target="#extras" href="#">Extras list & prices</a>
            </div>
        </div>


    </div>
</div>

<!--=== Content Part ===-->
<div class="container-fluid margin-top-100"> 
    <!-- Service Blocks -->
    <div class="row">
        <div class="col-md-4">
            <div class="service clearfix">
                <i class="fa <?php echo $icon2; ?>"></i>
                <div class="desc">
                    <h4><?php echo $title2; ?></h4>
                    <p><?php echo $description2; ?></p>
                </div>
            </div>  
        </div>
        <div class="col-md-4">
            <div class="service clearfix">
                <i class="fa <?php echo $icon3; ?>"></i>
                <div class="desc">
                    <h4><?php echo $title3; ?></h4>
                    <p><?php echo $description3; ?></p>
                </div>
            </div>  
        </div>
        <div class="col-md-4">
            <div class="service clearfix">
                <i class="fa <?php echo $icon4; ?>"></i>
                <div class="desc">
                    <h4><?php echo $title4; ?></h4>
                    <p><?php echo $description4; ?></p>
                </div>
            </div>  
        </div>              
    </div><!--/row-->
</div><!-- //End Service Blokcs -->


<!--=== Footer ===-->
<div class="footer">
  <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-12">
                <div class="headline"><h3><?php echo $footer1_title ?></h3></div>   
                <p class="margin-bottom-25"><?php echo $footer1_desc ?></p>
            </div>          
            <div class="col-lg-4 col-sm-12">
                <div class="headline"><h3><?php echo $footer2_title ?></h3></div>
                <ul class="social-icons">
                    <?php
                    if (!empty($fb_link)) {
                        echo '<li><a href="'. $fb_link .'" data-original-title="Facebook" class="social_facebook"></a></li>';
                    }
                    if (!empty($tw_link)) {
                        echo '<li><a href="'. $tw_link .'" data-original-title="Twitter" class="social_twitter"></a></li>';
                    }
                    if (!empty($gplus_link)) {
                        echo '<li><a href="'. $gplus_link .'" data-original-title="Goole Plus" class="social_googleplus"></a></li>';
                    }
                    if (!empty($linkedin_link)) {
                        echo '<li><a href="'. $linkedin_link .'" data-original-title="Linkedin" class="social_linkedin"></a></li>';
                    }
                    ?>
                </ul>
                <p class="margin-bottom-25"></p>
            </div>
            <div class="col-lg-4 col-sm-12 map-img">
                <div class="headline"><h3><?php echo $footer3_title ?></h3></div>   
                <address class="margin-bottom-30">
                <?php
                if (!empty($address)) {
                    echo '<i class="fa fa-map-marker"></i>' . $address . '<br>';
                } if (!empty($phone)) {
                    echo '<i class="fa fa-phone"></i>' . $phone . '<br>';
                } if (!empty($mobile)) {
                    echo '<i class="fa fa-mobile"></i>' . $mobile . '<br>';
                } if (!empty($fax)) {
                    echo '<i class="fa fa-fax"></i>' . $fax . '<br>';
                } if (!empty($email)) {
                    echo '<i class="fa fa-envelope-o"></i> <a href="mailto:' . $email . '" class="">' . $email . '</a>';
                }?>
                </address>
            </div>
        </div><!--/row-->   
    </div><!--/container--> 
</div><!--/footer-->    
<!--=== End Footer ===-->

<!--=== Copyright ===-->
<div class="copyright">
  <div class="container">
    <div class="row margin-top-20">
      <div class="col-md-12 ltr">           
        <p class="cright">&copy; <?php echo date("Y") ?> <b><?php echo $site_name ?></b>. All Rights Reserved.</p>
        <div class="clearfix"></div>
        <!--<div id="ABM">
            <div class="text">Powered by</div>
            <a id="relative" href="http://abmegypt.net" target="_blank">
                <span>
                     <img src="dist/img/abm_gray.png" alt="ABM logo gray" border="0">
                     <img class="img_ABM" src="dist/img/abm.png" alt="ABM logo" border="0">
                     <span id="text_ABM">We Control E-Marketing World</span>
                 </span>
            </a>
        </div>-->
      </div>
    </div><!--/row-->
  </div><!--/container--> 
</div><!--/copyright--> 

<div class="modal fade" tabindex="-1" id="services" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3>Services list & prices</h3>
                <hr>
                <table class="table table-striped table-hover">
                <thead><tr>
                    <th>Code</th>
                    <th>Service description</th>
                    <th>Min. Qty.</th>
                    <th>Price per 1</th>
                </tr></thead>
                <?php
                $result= mysql_query("SELECT * FROM `service` WHERE `status` = 1 ORDER BY `ordering`");
                while ($row = mysql_fetch_assoc($result)){
                    if (empty($row['price']) || $row['price'] == 0){
                      $price = ($row['panel_price']*$sPercentage)/100;
                    }else {
                      $price = $row['price']/100;
                    }
                    echo '<tr><td>'
                            . '<span class="label label-primary">'.$row['code'].'</span>'
                            . '</td><td>'
                            . $row['description']
                            . '</td><td>'
                            . $row['min_qty']
                            . '</td><td>$'
                            . $price
                            . '</td></tr>';
                }
                ?>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="extras" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3>Extras list & prices</h3>
                <hr>
                <table class="table table-striped table-hover">
                <thead><tr>
                    <th>Code</th>
                    <th>Extra description</th>
                    <th>Price per 1</th>
                </tr></thead>
                <?php
                $result= mysql_query("SELECT * FROM `extras` WHERE `status` = '1'");
                while ($row = mysql_fetch_assoc($result)){
                    if (empty($row['price']) || $row['price'] == 0){
                      $price = ($row['panel_price']*$ePercentage)/100;
                    }else {
                      $price = $row['price']/100;
                    }
                    echo '<tr><td>'
                            . '<span class="label label-primary">'.$row['code'].'</span>'
                            . '</td><td>'
                            . $row['description']
                            . '</td><td>$'
                            . $price
                            . '</td></tr>';
                }
                ?>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
if (!empty($analytics)){
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo $analytics; ?>', 'auto');
  ga('send', 'pageview');

</script>
<?php } ?>
  </body>
</html> 