<div id="id01" class="modal">
    <form class="modal-content animate"  id="login_form">
        <div class="imgcontainer">
            <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>

        </div>
        <div id="login" class="container">
            <div id="error" class="alert-box--error"></div>
            <div id="success" class="alert-box--success"></div>
            <label for="uname"><b>Username</b></label>
            <input type="text" placeholder="Enter Username" name="username" >

            <label for="psw"><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="password" >

            <button type="submit">Login</button>
            </div>

        <div class="container" style="background-color:#f1f1f1">
            <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
            <span class="psw">Forgot <a href="user_pass_rest.php">password?</a></span>
        </div>
    </form>
</div>