<?php
require_once 'includes/session.php';
require '../includes/general_class.php';
$general = new general();
$admin = mysqli_query($conn , "SELECT `email`,`api_key` FROM `admins` WHERE `id` = 1");
$data = mysqli_fetch_array($admin);
$url = 'http://panel.seoestore.net/action/api.php';
$api_key = $data['api_key'];
$email = $data['email'];
$uid = $_SESSION['userid'];
$data = Array(
  'api_key'=>$api_key,//must included in every request
  'email'=>$email,//must included in every request
  'action'=>'balance',//action must included in every request
  );

if (is_array($data)) {
  foreach ($data as $key => $value) {
    $_post[] = $key.'='.$value;
  }
}

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, join('&', $_post));
curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
$result = curl_exec($ch);
curl_close($ch);
$balance = json_decode($result,true);
if(isset($balance['error'])){
  $balance = 'N/A';
}elseif(isset($balance['balance'])){
  $balance = $balance['balance']/100;
}else{
  $balance = '0';
}

//get the currency-id that user set from main_info table
//and matches with paypal-currencies to get the currency code

$data = mysqli_query($conn , "SELECT currency_id FROM `main` WHERE `id`='1'");
$row = mysqli_fetch_array($data);
$currency_id=$row['currency_id'];

$data = mysqli_query($conn , "SELECT code FROM `paypal_currencies` where id = $currency_id");
$row = mysqli_fetch_array($data);
$currency=$row['code'];
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin area!</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Select 2 -->
    <link rel="stylesheet" href="../plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="../dist/css/style.css?v=3">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="../dist/css/skins/skin-blue.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>

  </head>

  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><i class="fa fa-dashboard"></i></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">Admin Area</span>
        </a>


        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

              <li>
                <a href="settings.php"><i class="fa fa-cogs"></i> Settings</a>
              </li>

              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="../dist/img/avatar1.png" class="user-image" alt="User Image">
                  <!-- hidden-xs hides the adminname on small devices so only the image appears. -->
                  <span class="hidden-xs"><?php echo $_SESSION['adminname']; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <img src="../dist/img/avatar1.png" class="img-circle" alt="User Image">
                    <p>
                      <?php echo $_SESSION['adminname']; ?> - Admin
                    </p>
                  </li>

                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="profile.php" class="btn btn-default flat">Mange Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="logout.php" class="btn btn-default flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>

            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="../dist/img/avatar1.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['adminname']; ?></p>
              <span data-toggle="tooltip" data-placement="right" title="Balance on SEOeStore">
                <span id="user-balance" class="label bg-green" > <?php echo $currency ." ". $balance;?></span>
              </span>
              <a class="text-red" href="http://panel.seoestore.net/balance.php" target="_blank">TOP UP</a>
              <!-- Status -->
            </div>
          </div>

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">Main Menu</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

            <li><a href="main.php"><i class="ion ion-clipboard"></i> <span>Main Information</span></a></li>
            <li class="treeview">
              <a href="#home">
                <i class="fa fa-home"></i> <span>Home page</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="home-template.php"><i class="fa fa-desktop"></i> Template</a></li>
                <li><a href="home-logo.php"><i class="ion ion-image"></i> Logo</a></li>
                <li><a href="home-slideshow.php"><i class="ion ion-images"></i> Slideshow</a></li>
                <li><a href="home-main-paragraph.php"><i class="ion ion-drag"></i> Main paragraph</a></li>
                <li><a href="home-paragraph2.php"><i class="ion ion-drag"></i> Paragraph II</a></li>
                <li><a href="home-pricing-paragraph.php"><i class="ion ion-drag"></i> Pricing paragraph</a></li>
                <li><a href="home-3-paragraph.php"><i class="fa fa-file-text-o"></i> 3 paragraphs section</a></li>
                <li><a href="home-footer.php"><i class="fa fa-dot-circle-o"></i> footer</a></li>
                <li><a href="home-seo.php"><i class="fa fa-line-chart"></i> SEO details</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#users">
                <i class="fa fa-users"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="users-manage.php"><i class="fa fa-users" aria-hidden="true"></i> <span>Manage users</span></a></li>
                <li><a href="user-edit.php"><i class="ion ion-edit"></i> <span>Edit user</span></a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#payment">
                <i class="fa fa-usd"></i> <span>Payment</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="payment-note.php"><i class="ion ion-drag"></i> Payment note</a></li>
                <li><a href="funds-add.php"><i class="fa fa-money"></i> <span>Add user funds</span></a></li>
                <li><a href="payment-method.php"><i class="fa fa-th-list"></i> <span>Add payment method</span></a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#services">
                <i class="fa fa-bars"></i> <span>Services</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="services-manage.php"><i class="fa fa-bars"></i> <span>Manage services</span></a></li>
                <li><a href="service-edit.php"><i class="ion ion-edit"></i> <span>Edite service</span></a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#extras">
                <i class="fa fa-bars"></i> <span>Extras</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="extras-manage.php"><i class="fa fa-bars"></i> <span>Manage extras</span></a></li>
                <li><a href="extra-edit.php"><i class="ion ion-edit"></i> <span>Edit extra</span></a></li>
              </ul>
            </li>
            
            <li class="treeview">
              <a href="#reports">
                <i class="fa fa-pie-chart"></i> <span>Reports</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="sales.php"><i class="fa fa-pie-chart" aria-hidden="true"></i> <span>Daily sales reports </span></a></li>
                <li><a href="orders.php"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>Orders list</span></a></li>
                <li><a href="payments.php"><i class="fa fa-money" aria-hidden="true"></i> <span>Payment records</span></a></li>
              </ul>
            </li>
            
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>




      

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">