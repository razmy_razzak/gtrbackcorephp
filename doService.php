<?php
require_once 'controllers/Services.php';

$eIds = isset($_POST['extra'])?$_POST['extra'] : [];
//$keywords = isset($_POST['keywords'])?$_POST['keywords']:[];
//$links = isset($_POST['links'])?$_POST['links']:[];
//$keywords = explode(",", $keywords);
//$links = explode(",", $links);
$extras = implode(',', $eIds);
$data = [
    //hidden fields
    'uid'       => isset($_POST['uid'])? $_POST['uid']:'',
    'price'       => isset($_POST['price'])? $_POST['price']: 0,
    'date'         =>  gmdate('ymdHis'),
    'service_id'  => isset($_POST['service_id'])?$_POST['service_id']:0,
    //array values
    'extra'  => $extras,

    'keywords'  => isset($_POST['keywords'])?$_POST['keywords']:'',
    'links'  => isset($_POST['links'])?$_POST['links']:'',
    'quantity'  => isset($_POST['quantity'])? $_POST['quantity']:0,
    'article'  => isset($_POST['article'])? $_POST['article']:0,
    'refId'  => isset($_POST['refId'])?$_POST['refId']:'',
];

$service = new Services();
$result = $service->createOrder( $data );
if( $result['tag'] == 200) {
    //not enough fund
    echo json_encode(array('success' => true,'oid'=> $result['oid']));
}

elseif($result['tag'] == 219){
    echo json_encode(array('fund'=>'Please add more fund'));
}
elseif($result['tag'] == 300){
    echo json_encode(array('qtys'=>$result['qtys']));
}


