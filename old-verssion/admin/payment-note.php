<?php 
require 'includes/header.php';
require_once 'libraries/htmlpurifier/HTMLPurifier.auto.php';
$config = HTMLPurifier_Config::createDefault();
$purifier = new HTMLPurifier($config);
?>
<link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#payment']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='payment-note.php']" ).parent().addClass( "active" );
});
</script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="ion ion-drag"></i> Payment note
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li>Payment</li>
    <li class="active">Payment note</li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">

<?php
// Update
if(isset($_POST['submit'])) {
  // get fields
  $desc = $_POST['description'];
  $desc = $purifier->purify($desc);
  $desc = mysql_real_escape_string($desc);
  //insert data
  $sql = mysql_query("UPDATE `paragraphs` SET
                      `description`='$desc'
                      WHERE `id`='5'");
  //test
  if(!$sql){
      die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysql_error() . "<div>");
  }
  echo "<div class=\"alert alert-success\" role=\"alert\">Details Successfully Updated!</div>";
}


// Get data
$data = mysql_query("SELECT * FROM `paragraphs` WHERE `id`='5'");
$row = mysql_fetch_array($data);
  $description = $row['description'];
?>

  <div class="box box-primary">
    <div class="box-body">
      <form method="post" action="">

        <div class="form-group">
          <label for="description">Payment note</label> <small>(Custom paragraph on the user add funds page)</small>
          <textarea class="form-control" rows="4" id="description" name="description" placeholder="Description..."><?php echo $description ?></textarea>
        </div>

                      
        <input type="submit" class="btn btn-primary flat" name="submit" value="UPDATE">
      </form>
  	</div>
  </div>
</section><!-- /.content -->

<script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script>
  $(function () {
    $("textarea").wysihtml5();
  });
</script>

<?php 
require 'includes/footer.php';
?>