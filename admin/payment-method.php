<?php 
require 'includes/header.php';
?>

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#payment']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='payment-method.php']" ).parent().addClass( "active" );
});
</script>



<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-th-list"></i> Add payment method
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li>Payment</li>
    <li class="active">Add payment method</li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">
<?php
if(isset($_POST['submit'])) {
	// get fields
	$name = mysqli_real_escape_string($conn , htmlspecialchars($_POST['name']));
   
	$update = mysqli_query($conn , 
		"INSERT INTO `payment_methods`
		(`name`) VALUES
		('$name')");
		if (!$update) {
		{
		  die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysqli_error($conn) . "<div>");
		}
		echo "<div class=\"alert alert-success\" role=\"alert\">Method successfully Add!</div>";
}}
?>
    <div class="box box-primary">
        <div class="box-body">
			<form action="" method="post">
				<div class="form-group">
					<label for="name">Method name:*</label>
					<input class="form-control" type= "text" id="name" name="name" placeholder="Method name..." required>
				</div>
				<input class="btn btn-primary flat" type="submit" name="submit" value="Add method " />
				<br /><small>All fields with * are required!</small>
			</form>
		</div>
    </div>
</section><!-- /.content -->



<?php 
require 'includes/footer.php';
?>