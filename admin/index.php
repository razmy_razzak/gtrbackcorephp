<?php
require 'includes/header.php';
function getPaymentMethod($conn){
    $sql = mysqli_query($conn , "SELECT * FROM `payment_methods` ORDER BY `name` ASC");
        echo '<option value="">Choose payment method</option>'; 
    while($record = mysqli_fetch_array($sql)){
        echo '<option value="' . $record['id'] . '">' . $record['name']. '</option>'; 
    }
}
?>

<!-- DataTables -->
<link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>

<!-- ChartJS 1.0.1 -->
<script src="../plugins/chartjs/Chart.min.js"></script>
<?php
if($api_key=="" || $email=="" ||$balance == 'N/A'){
       $general -> alert('API key or Email is not set <a href="settings.php" >HERE YOU CAN SET IT</a>!', 'danger flat');
}
?>
<div class="container-fluid">
  <div class="row margin-top-30">
<?php
$sql = mysqli_query($conn , "SELECT
  (SELECT COUNT(*) FROM `users`) AS `total-users`,
  (SELECT COUNT(*) FROM `orders`) AS `total-orders`,
  (SELECT SUM(`price`) FROM `orders`) AS `orders-income`,
  (SELECT SUM(`amount`) FROM `payment`)  AS `total-funds`");
$info = mysqli_fetch_array($sql)
?>
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3><?php echo $info['total-users']; ?></h3>
          <p>Users</p>
        </div>
        <div class="icon">
          <i class="fa fa-users"></i>
        </div>
        <div class="small-box-footer">Total Clients</div>
      </div>
    </div><!-- ./col -->

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3><?php echo $info['total-orders']; ?></h3>
          <p>Sales</p>
        </div>
        <div class="icon">
          <i class="fa fa-shopping-cart"></i>
        </div>
        <div class="small-box-footer">Total Orders</div>
      </div>
    </div><!-- ./col -->

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3><?php echo round($info['orders-income']/100,0); ?></h3>
          <p><?php echo $currency ?></p>
        </div>
        <div class="icon">
          <i class="fa fa-money"></i>
        </div>
        <div class="small-box-footer">Total Orders Amount</div>
      </div>
    </div><!-- ./col -->

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3><?php echo  $info['total-funds']*1; ?></h3>
          <p><?php echo $currency ?></p>
        </div>
        <div class="icon">
          <i class="fa fa-credit-card"></i>
        </div>
        <div class="small-box-footer">Total Funds Added</div>
      </div>

    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="panel-body">
          <i class="fa fa-area-chart"></i> <strong>Monthly Recap Report</strong>
          <hr>
          <div class="row">
            <div class="col-md-6">
              <div class="sales-chart">
                <p class="text-center">
                  <strong>Sales: <?php echo date('m/Y',strtotime("-11 month"));?> to <?php echo date('m/Y');?></strong>
                </p>
                <div class="chart">
                  <!-- Sales Chart Canvas -->
                  <canvas id="salesChart" style="height: 180px;"></canvas>
                </div><!-- /.chart-responsive -->
              </div>
            </div><!-- /.col -->
            <div class="col-md-6">

              <table class="table table-striped table-hover monthly-report">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Orders amount</th>
                    <th>Orders income</th>
                    <th>Total charge</th>
                  </tr>
                </thead>
                <tbody>
<?php
$chartSQL = mysqli_query($conn , "SELECT
    DATE(`orders`.`date`) AS `dater`,
    COUNT(*) AS `orders-amount`,
    SUM(`orders`.`price`) AS `orders-income`,
    (SELECT SUM(`amount`) FROM `payment` WHERE  YEAR(`dater`) = YEAR(`payment`.`date`) AND MONTH(`dater`) = MONTH(`payment`.`date`)) AS `total_charge`
    FROM `orders`
    GROUP BY YEAR(`dater`), MONTH(`dater`)
    ORDER BY `dater` DESC
    ");

$x = 6;
while ($chart = mysqli_fetch_array($chartSQL)) {
  $date = strtotime($chart['dater']);
  $date = date('Y/m', $date);

  $amount = $income = $charge = 0;
  if (!empty($chart['orders-amount'])) {
    $amount = $chart['orders-amount']+0;
  }
  if (!empty($chart['orders-income'])) {
    $income = $chart['orders-income']/100+0;
  }
  if (!empty($chart['total_charge'])) {
    $charge = $chart['total_charge']+0;
  }
  echo '<tr><td>';
  echo $date;
  echo '</td><td>';
  echo $amount;
  echo '</td><td>';
  echo $currency." ".$income;
  echo '</td><td>';
  echo $currency." ".$charge;
  echo '</td></tr>';

  switch ($x) {
    case '6':
      $cA6 = $amount;
      $cI6 = $income;
      $cC6 = $charge;
      break;
    case '5':
      $cA5 = $amount;
      $cI5 = $income;
      $cC5 = $charge;
      break;
    case '4':
      $cA4 = $amount;
      $cI4 = $income;
      $cC4 = $charge;
      break;
    case '3':
      $cA3 = $amount;
      $cI3 = $income;
      $cC3 = $charge;
      break;
    case '2':
      $cA2 = $amount;
      $cI2 = $income;
      $cC2 = $charge;
      break;
    case '1':
      $cA1 = $amount;
      $cI1 = $income;
      $cC1 = $charge;
      break;
  }
  $x--;
}

function d0(&$y) {
  if (empty($y)) {
    $y = 0;
  }
}
d0($cA6);d0($cA5);d0($cA4);d0($cA3);d0($cA2);d0($cA1);
d0($cI6);d0($cI5);d0($cI4);d0($cI3);d0($cI2);d0($cI1);
d0($cC6);d0($cC5);d0($cC4);d0($cC3);d0($cC2);d0($cC1);
?>
                </tbody>
              </table>

            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- ./box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-md-8  ">
      <div class="box box-primary">
          <div class="panel-body">
            <i class="fa fa-bars"></i> <strong>Daily Sales Summary</strong>
            <hr>
            <table class="table table-striped table-hover">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Orders amount</th>
                  <th>Orders income</th>
                  <th>Total charge</th>
                </tr>
              </thead>
              <tbody>
<?php
$sql = mysqli_query($conn , "
SELECT  DATE(`orders`.`date`) AS `dater`,
COUNT(*) AS `orders-amount`,
SUM(`orders`.`price`) AS `orders-income`,
(SELECT SUM(`amount`) FROM `payment` WHERE DATE(`dater`)=DATE(`payment`.`date`)) AS `total_charge`
FROM `orders`
GROUP BY `dater`
ORDER BY `dater` DESC LIMIT 5;"
);
while ($row = mysqli_fetch_array($sql)) {
  echo '<tr><td>';
  echo $row['dater'];
  echo '</td><td>';
  echo $row['orders-amount'];
  echo '</td><td>';
  echo $currency." ".$row['orders-income']/100;
  echo '</td><td>';
  echo $currency." ".$row['total_charge']*1;
  echo '</td></tr>';
}
?>
            </tbody>
          </table>
          <hr>
          <div class="text-center">
            <a class="btn btn-sm btn-default flat" href="sales.php">View full Daily Sales Summary</a>
          </div>
        </div>
      </div>
    </div>
        <div class="col-md-4">
<script>
$(function () {
  $('#form-add-payment').submit(function(e){
        e.preventDefault();
    var user = $('#username').val();
    var amount = $('#amount').val();
    var transID = $('#transID').val();
    //$('#add-payment-result').load('add-payment.php?user=' + user + '&amount=' + amount + '&transID=' + transID );
    $.ajax({
      method: "POST",
      url: 'add-payment.php',
      data: { user: user, amount: amount, transID: transID },
      success: function(result){
        $('#add-payment-result').html(result);
      }
    })
  });
});
</script>
      <div class="box box-info">
              <div class="panel-body">
          <i class="fa fa-money"></i> <strong>Add Payment</strong>
          <hr>
          <div id="add-payment-result"></div>
          <div class="row">
            <div class="col-md-12">
              <form id="form-add-payment">
                <div class="form-group">
                    <label class="sr-only" for="username">User</label>
                    <select class="form-control input-sm select2" id="username" name="username" data-placeholder="Select username" required>
                    <option value="">- Select User -</option>
                    <?php $general -> usersRecords($conn); ?>
                    </select>
                </div>
                <div class="form-group">
                  <label class="sr-only" for="amount">Amount</label>
                  <div class="input-group">
                    <span class="input-group-addon flat"><?php echo $currency ?></span>
                    <input type="number" step="any" class="form-control" id="amount" name="amount" placeholder="Amount in <?php echo $currency ?>" required>
                  </div>
                </div>

                <div class="form-group">
                  <label class="sr-only" for="transID">Transaction ID</label>
                  <select list="txn-list" type="text" class="form-control" id="transID" name="transID" placeholder="Transaction ID" required>
                  <?php getPaymentMethod($conn)?>
                  </select>
                </div>
                <div class="form-group">
                  <input type="submit" name="submit" value="Submit" class="btn btn-default btn-block flat">
                </div>
              </form>
              <div class="text-center"><a href="payments.php"><small>VIEW ALL PAYMENT RECORDS</small></a></div>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.box-body -->
      </div><!-- /.box -->
      
    </div>
  </div>
</div>


<script>
  $(function () {
    $('.monthly-report').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>



<script>
  $(function () {

/* ChartJS
   * -------
   * Here we will create a few charts using ChartJS
   */

  //-----------------------
  //- MONTHLY SALES CHART -
  //-----------------------

  // Get context with jQuery - using jQuery's .get() method.
  var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
  // This will get the first returned node in the jQuery collection.
  var salesChart = new Chart(salesChartCanvas);

  var salesChartData = {
    labels: [
    		 <?php echo '"' . date('m/y',strtotime("-5 month")) . '", ' . 
         '"' . date('m/y',strtotime("-4 month")) . '", ' . 
    		 '"' . date('m/y',strtotime("-3 month")) . '", ' . 
    		 '"' . date('m/y',strtotime("-2 month")) . '", ' . 
    		 '"' . date('m/y',strtotime("-1 month")) . '", ' . 
    		 '"' . date('m/y') . '"';?>],
    datasets: [
      {
        label: "Orders income",
        fillColor: "#F39C12",
        strokeColor: "#F39C12",
        pointColor: "#F39C12",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "#F39C12",
        data: [<?php echo $cI1 . ', ' . $cI2 . ', ' . $cI3 . ', ' . $cI4 . ', ' . $cI5 . ', ' . $cI6;?>]
      },
      {
        label: "Orders amount",
        fillColor: "#00C0EF",
        strokeColor: "#00C0EF",
        pointColor: "#00C0EF",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "#00C0EF",
        data: [<?php echo $cA1 . ', ' . $cA2 . ', ' . $cA3 . ', ' . $cA4 . ', ' . $cA5 . ', ' . $cA6;?>]
      },
      {
        label: "Total charge",
        fillColor: "#337ab7",
        strokeColor: "#337ab7",
        pointColor: "#337ab7",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "#337ab7",
        data: [<?php echo $cC1 . ', ' . $cC2 . ', ' . $cC3 . ', ' . $cC4 . ', ' . $cC5 . ', ' . $cC6;?>]
      }
    ]
  };

  var salesChartOptions = {
    //Boolean - If we should show the scale at all
    showScale: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: true,
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - Whether the line is curved between points
    bezierCurve: true,
    //Number - Tension of the bezier curve between points
    bezierCurveTension: 0.3,
    //Boolean - Whether to show a dot for each point
    pointDot: true,
    //Number - Radius of each point dot in pixels
    pointDotRadius: 4,
    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 1,
    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius: 20,
    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,
    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 2,
    //Boolean - Whether to fill the dataset with a color
    datasetFill: false,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true,
    multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"
  };

  //Create the line chart
  salesChart.Line(salesChartData, salesChartOptions);

  //---------------------------
  //- END MONTHLY SALES CHART -
  //---------------------------

});
</script>

<script>
  $(function () {
  $(".select2").select2();
});
</script>

<?php
  include 'includes/footer.php';
?>