<?php 
require 'includes/header.php';
?>

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#contact']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='contact-details.php']" ).parent().addClass( "active" );
});
</script>


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-info-circle"></i> Contact details
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Admin</a></li>
    <li>Contact page</li>
    <li class="active">Contact details</li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">

<?php
// Update
if(isset($_POST['submit'])) {
  // get fields
  $address = mysqli_real_escape_string($conn , htmlspecialchars($_POST['address']));
  $phone = mysqli_real_escape_string($conn , htmlspecialchars($_POST['phone']));
  $fax = mysqli_real_escape_string($conn , htmlspecialchars($_POST['fax']));
  $mobile = mysqli_real_escape_string($conn , htmlspecialchars($_POST['mobile']));
  $email = mysqli_real_escape_string($conn , htmlspecialchars($_POST['email']));

  //insert data
  $sql = mysqli_query($conn , "UPDATE `contact` SET
                      `address`='$address',
                      `phone`='$phone',
                      `fax`='$fax',
                      `mobile`='$mobile',
                      `email`='$email'
                      WHERE `id`='1'");
  //test
  if(!$sql){
      die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysqli_error($conn) . "<div>");
  }
  echo "<div class=\"alert alert-success\" role=\"alert\">Details Successfully Updated!</div>";
}

// Reset
if(isset($_GET['reset'])) {
  $sql = mysqli_query($conn , "UPDATE `contact` SET
                      `address`='25 Al-Swesry A, No. 14, Cairo, Egypt.',
                      `phone`='+(2) 02 247 05136',
                      `fax`='+(2) 02 247 05136',
                      `mobile`='+(2) 010 23498733',
                      `email`='sales@abmegypt.com'
                      WHERE `id`='1'");
  //test
  if(!$sql){
      die("<div class=\"alert alert-danger\" role=\"alert\">Could not reset data: " . mysqli_error($conn) . "<div>");
  }
  echo "<div class=\"alert alert-success\" role=\"alert\">Details Successfully Reset!</div>";
}

// Get data
$data = mysqli_query($conn , "SELECT * FROM `contact` WHERE `id`='1'");
$row = mysqli_fetch_array($data);
  $address = $row['address'];
  $phone = $row['phone'];
  $fax = $row['fax'];
  $mobile = $row['mobile'];
  $email = $row['email'];
?>

  <div class="box box-primary">
    <div class="box-body">

      <form method="post" action="contact-details.php">
        <div class="form-group">
          <label for="address">Address</label>
          <input type="text" class="form-control" id="address" name="address" value="<?php echo $address ?>" placeholder="Address...">
        </div>

        <div class="form-group">
          <label for="phone">Phone</label>
          <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $phone ?>" placeholder="Phone...">
        </div>

        <div class="form-group">
          <label for="fax">Fax</label>
          <input type="text" class="form-control" id="fax" name="fax" value="<?php echo $fax ?>" placeholder="Fax...">
        </div>

        <div class="form-group">
          <label for="mobile">Mobile</label>
          <input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo $mobile ?>" placeholder="Mobile...">
        </div>

        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="email" name="email" value="<?php echo $email ?>" placeholder="Email...">
        </div>

        <input type="submit" class="btn btn-primary flat margin-top-20" name="submit" value="UPDATE">

        </form>
    </div>
  </div>
</section><!-- /.content -->



<?php 
require 'includes/footer.php';
?>