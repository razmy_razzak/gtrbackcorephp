<?php
require_once 'includes/header.php';
require_once 'libraries/htmlpurifier/HTMLPurifier.auto.php';
$config = HTMLPurifier_Config::createDefault();
$purifier = new HTMLPurifier($config);
$custom_css = fopen("../dist/css/custom.css", "r+") or die("Unable to open file!");
?>
<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='custom-css-manage.php']" ).parent().addClass( "active" );
});
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-users"></i> Custom CSS Mange
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="index.php">
                <i class="fa fa-dashboard"></i>Dashboard</a>
        </li>
        <li>Custom CSS</li>
        <li class="active">Manage Custom CSS</li>
    </ol>
</section>
<!-- /.content Header-->
<!-- Content -->
<section class="content">
<?php
if(isset($_POST['edit'])){
    $custom_css_new = mysqli_real_escape_string($conn , $_POST['custom_css']);
    $custom_css_new = $purifier->purify($custom_css_new);
    fwrite($custom_css, $custom_css_new);
    fclose($custom_css);
    echo "<script type='text/javascript'> document.location = 'custom-css-mange.php'; </script>";
}
?>
<form method="post" action="">
<div class="form-group">
          <label for="user">Edit Custom Css</label>
            <textarea name='custom_css' class="form-control" rows="50" col='' >
            <?php 
            if(filesize("../dist/css/custom.css")>0){
            echo fread($custom_css,filesize("../dist/css/custom.css"));
            }
            ?>
            </textarea>
        </div>
        <input type="submit" class="btn btn-primary flat" name="edit" value="Edit">
      </form>    
</form>
</section>

<?php 
require 'includes/footer.php';
?>