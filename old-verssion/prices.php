<?php 
require 'includes/header.php';
$sPercentage = 1 + $serPricePlus/100;
$ePercentage = 1 + $extraPricePlus/100;
?>

<script>
$(document).ready(function(){
  $( ".sidebar-menu li a[href^='prices.php']" ).parent().addClass( "active" ); 
});
</script>
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-list"></i> Prices
  </h1>
  <ol class="breadcrumb">
    <li><a href="dashboard.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li class="active">Prices</li>

  </ol>
</section><!-- /.content Header-->


<?php
$balance = mysql_query("SELECT `amount`, `date`,`name`,`txn_id` FROM `payment` left join `payment_methods` on `method` = `payment_methods`.`id` WHERE uid = $uid ORDER BY `date`");
?>
<!-- Content -->
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Services price list</h3>
    </div>
    <div class="box-body">
      <table class="table table-striped table-hover" >
        <thead><tr>
            <th>Code</th>
            <th>Service description</th>
            <th>Min. Qty.</th>
            <th>Price per 1</th>
        </tr></thead>
        <?php
        $result= mysql_query("SELECT * FROM `service` WHERE `status` = 1 ORDER BY `ordering`");
        while ($row = mysql_fetch_assoc($result)){
            if (empty($row['price']) || $row['price'] == 0){
              $price = ($row['panel_price']*$sPercentage)/100;
            }else {
              $price = $row['price']/100;
            }
            echo '<tr><td>'
                    . '<span class="label bg-blue">'.$row['code'].'</span>'
                    . '</td><td>'
                    . $row['description']
                    . '</td><td>'
                    . $row['min_qty']
                    . '</td><td>$'
                    . $price
                    . '</td></tr>';
        }
        ?>
      </table>
    </div>
  </div>
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Extras price list</h3>
    </div>
    <div class="box-body">
      <table class="table table-striped table-hover" >
        <thead><tr>
            <th>Code</th>
            <th>Extra description</th>
            <th>Price per 1</th>
        </tr></thead>
        <?php
        $result= mysql_query("SELECT * FROM `extras` WHERE `status` = '1'");
        while ($row = mysql_fetch_assoc($result)){
            if (empty($row['price']) || $row['price'] == 0){
              $price = ($row['panel_price']*$ePercentage)/100;
            }else {
              $price = $row['price']/100;
            }
            echo '<tr><td>'
                    . '<span class="label bg-yellow">'.$row['code'].'</span>'
                    . '</td><td>'
                    . $row['description']
                    . '</td><td>$'
                    . $price
                    . '</td></tr>';
        }
        ?>
      </table>
    </div>
  </div>
</section>
<script>
  $(function () {
    $('.table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<?php 
require 'includes/footer.php';
?>