<?php
require_once 'includes/connect.php';
require_once 'controllers/EmailClass.php';
require_once 'controllers/ResetServices.php';
require"includes/lang.php";
$data = mysqli_query($conn , "SELECT * FROM `main` WHERE `id` = '1'");
$row = mysqli_fetch_array($data);
$site_name = $row['site_name'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Username or Password Reset - <?php echo $site_name;?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition login-page">

<?php

function sendEmail($toEmail, $username =null, $link)
{

    $to = $toEmail;
    $subject = "Username password Reset";
    if($username){
        $message = "
                <html>
                <head>
                <title>Reset Email</title>
                </head>
                <body>
                <p><strong>Username: </strong>$username</p>
                <p><strong>Reset Link: </strong>$link</p>
                <p>Thank you</p>
                <p>System Admin</p>
                </body>
                </html>
                ";
    }
    else{
        $message = "
                <html>
                <head>
                <title>Reset Email</title>
                </head>
                <body>
                <p><strong>Reset Link: </strong>$link</p>
                <p>Thank you</p>
                <p>System Admin</p>
                </body>
                </html>
                ";
    }
// Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    mail($to, $subject, $message, $headers);
}

if (isset ($_POST['submit'])){
    $email = mysqli_real_escape_string($conn , htmlspecialchars($_POST['email']));
    $find_email= mysqli_fetch_array(mysqli_query($conn , "SELECT * from `users` WHERE `email` ='$email'"));
    if ($email==''){
        echo '<div class="alert alert-danger">Please enter email!</div>';
    }
    elseif (!$find_email){
        echo '<div class="alert alert-danger">Email not Found!</div>';
    }

    else{
        //found user email
        // generate activation link and hash
        //send activation email with user name if ticked
        $base_url = $_SERVER['HTTP_ORIGIN'];
        $reset_hash = md5($find_email['email'].$find_email['username']);
        $reset_url = $base_url.'/reset_link.php?hash='.$reset_hash;
        $email = $find_email['email'];
        $data = [
            'hash' => $reset_hash,
            'email' => $email
        ];
        $resetEmail = new ResetServices();
        $result = $resetEmail->createPasswordReset( $data );
        if($result){
            if(isset ($_POST['username']) && $_POST['username'] == 1){
                sendEmail( $email, $find_email['username'], $reset_url );
            }
            else{
                sendEmail( $email, null, $reset_url );
            }
            echo '<div class="alert alert-success">Password Reset email Sent!</div>';

        }
    }
}
?>
<div class="login-box">
    <div class="login-logo">
        Username or Password Reset
    </div><!-- /.login-logo -->
    <p class="text-center"><?php lang('gain_instant'); echo $site_name;?>.</p>
    <div class="login-box-body">
        <p class="login-box-msg">Reset Form</p>
        <form action="user_pass_rest.php" method="post">
            <div class="form-group has-feedback">
                <input class="form-control" placeholder="<?php lang('email'); ?>" type="email" name="email"/>
                <span class="fa fa-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <strong>Please select appropriate</strong><br>
                <input type="checkbox" name="username" value="1" > Forgot Username<br>
                <input type="checkbox" name="password" value="1"  required checked> Forgot Password<br>
            </div>

            <div class="row">

                <div class="col-xs-4 col-xs-offset-8">
                    <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Send</button>
                </div><!-- /.col -->
            </div>
        </form>
        <?php lang('already_member'); ?><a href="login.php"><?php lang('login'); ?></a>
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
<div class="container text-center">
    <a href="index.php"><i class="fa fa-home" aria-hidden="true"></i> <?php lang('back_to_home'); ?>Back to home</a>
</div>
</body>
</html>
