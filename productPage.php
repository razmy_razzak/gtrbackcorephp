<?php

require_once 'controllers/Services.php';
$service = new Services();
$data = $service->getServiceById( $_GET['productId'] );
$matchServices = $service->getServicesMatches( substr($data['code'], 0, 2) );
$user_id = isset($_SESSION['id'])? $_SESSION['id'] : 0;
$extras = explode(",",$data['extras']);

$extraServices = $service->getExtras( $extras );

$sPercentage = 1 + $serPricePlus/100;
$description = $data['description'];
$price = $data['price']/100;
$panel = $data['panel_price']/100;
$panel_price = $panel*$sPercentage;

$service->checkUserBalance(10);
$artical_cats = $service->getArtical_cats();
?>

<section id='services' class="s-services page_herader contailner_style bg-Dardk-pink">
    <div class=""></div>

    <div class="row section-header" data-aos="fade-up">
        <div class="col-full">
            <h1 class="display-1 productPage_title"><?php echo $data['description'] ?></h1>
            <ul class="listUnstyle">
                <li class="pageHear_colour"><i class="fa fa-star"></i> Minimum Quantity: <?php echo $data['min_qty']?></li>
                <li class="pageHear_colour"><i class="fa fa-star"></i> Panel Price: <?php echo $data['panel_price']?></li>
                <li class="pageHear_colour"><i class="fa fa-star"></i> Extra services: <?php echo $data['extras']? 'Available': 'No Available'?></li>
                <li class="pageHear_colour"><i class="fa fa-star"></i> Product Code: <?php echo $data['code'] ?></li>
            </ul>
        </div>
    </div> <!-- end section-header -->
</section> <!-- end s-services -->

<section id='services' class="s-services contailner_style2 light-gray">
    <div class="row services-list block-1-2 block-m-1-2 ">

            <div class="col-2-3 service-item " data-aos="fade-up">
                <ul class="list-inline order_nav">
                    <li><a href="#" id="order_btn">order</a></li>
                    |
                    <li><a href="#" id="readmore_btn">Readmore</a></li>
                    |
                    <li><a href="#" id="review_btn">review</a></li>
                </ul>
                    <div id="order" class="service-text form_card" style="display: block" >
                        <h3 class="h4 product_heading">ORDER</h3>
                       <form id="product_form" class="product_form" >
                        <!-- hidden fileds-->
                           <input name="uid" type="hidden" value="<?php echo $user_id;?>">
                           <input name="price" type="hidden" value="<?php echo $data['panel_price']?>">
                           <input name="service_id" type="hidden" value=" <?php echo $data['id']?>">
                           <input name="refId" type="hidden" value="0">

                        <!--hidden fileds-->

                           <input name="url" type="text" placeholder="Enter url">
                           <input name="quantity" type="text" placeholder="Enter Quantity" max="100000000000" min="10">
                           <span class="sample"> eg: www.link.com, www.link2.com</span>
                           <textarea name="links" placeholder="Enter Links with comma separated "
                                     rows="3" cols="33"
                                     wrap="hard"></textarea>
                           <span class="sample"> eg: seo, google, etc</span>
                           <textarea name="keywords" placeholder="Enter Keyword with comma separated "
                                     rows="3" cols="33"
                                     wrap="hard"></textarea>
                           <span class="sample"> Optional</span>
                           <select class="form-control select2" id="article" name="article" data-placeholder="Select Article Category">
                               <option value="">- Select Article Category -</option>
                               <option value="0">- Auto detect article category -</option>
                               <?php if($artical_cats){
                                   foreach ( $artical_cats as $artical_cat){?>
                                       <option value="<?php echo $artical_cat['code']?>"><?php echo $artical_cat['name']?></option>
                               <?php } } ?>
                           </select>
                           <div class="row services-list block-1-3 block-mob-1-3 ">
                               <div class="col-twelve service-item checklist_column" data-aos="fade-up">
                                   <strong>Please select appropriate extra services</strong>
                                   <br>
                                   <ul class="listUnstyle">
                                       <?php foreach ( $extraServices as $key => $extraService ){ ?>
                                       <li><label class="checkbox_lable"><?php echo $extraService['description']?> - (<span class="price">$ <?php echo $extraService['panel_price'] ?></span>)</label><input type="checkbox" name="extra[<?php echo $key?>]" value="<?php echo $extraService['id']?>"></li>
                                       <?php } ?>
                                   </ul>
                               </div>
                             </div>

                           <input type="submit" class="order_btn" name="subscribe" value="Submit">
                       </form>
                    </div>
                    <div id="read_more" class="service-text form_card" style="display: none">
                        <h3 class="h4 product_heading">READ MORE</h3>
                        comming soon ...
                    </div>
                    <div id="review" class="service-text form_card" style="display: none" >
                        <h3 class="h4 product_heading">SUBMIT YOUR REVIEW</h3>
                                <div class="center-align rate jq-ry-container" style="width: 200px;"><div class="jq-ry-group-wrapper"><div class="jq-ry-normal-group jq-ry-group"><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="40px" height="40px" fill="#d8d6d6"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="40px" height="40px" fill="#d8d6d6" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="40px" height="40px" fill="#d8d6d6" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="40px" height="40px" fill="#d8d6d6" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="40px" height="40px" fill="#d8d6d6" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg></div><div class="jq-ry-rated-group jq-ry-group" style="width: 0%;"><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="40px" height="40px" fill="#f39c12"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="40px" height="40px" fill="#f39c12" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="40px" height="40px" fill="#f39c12" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="40px" height="40px" fill="#f39c12" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="40px" height="40px" fill="#f39c12" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg></div></div></div>
                                <div class="rating-val" hidden="hidden">0</div>
                                <div class="input-field col s12">
                                    <div class="header2">Customer is Always Right!.</div>
                                    What do you think regarding our service.
                                    <form id="review_form" class="product_form">
                                    <input name="uid" type="hidden" value="<?php echo $user_id;?>">
                                    <input type="hidden" name="reviewRating" id="rating" value="[object HTMLInputElement]">
                                    <input type="hidden" name="reviewServiceNum" value="46">
                                    <input type="submit" class="order_btn" name="subscribe" value="Review">
                                    </form>
                                </div>

                    </div>
            </div>
            <div class="col-1-3 service-item " data-aos="fade-up">
            <div class="service-text">
                <h3 class="subhead">Similar Products</h3>
                <?php foreach ($matchServices as $product) {?>
                    <div class="service-item " data-aos="fade-up">
                        <a href="custom-page.php?id=2&productId=<?php echo $product['id'] ?>">
                            <div class="service-text">
                                <h3 class="h4 product_heading"><?php echo $product['description'] ?></h3>
                                <div class="product" style="background: url(images/products/product_bg.png) no-repeat;  height: 150px">
                                    <div class="left">
                                        <i class="<?php echo $product['icon'] ?>"></i>
                                    </div>
                                    <div class="right">
                                        <p class="product_title"><?php echo strlen($product['description']) > 40 ? substr($product['description'],0,40)."..." : $product['description'];?></p>
                                    </div>
                                </div>
                                <p class="price">Price: $<?php echo $product['panel_price'] ?></p>
                            </div>
                        </a>
                    </div>
                <?php }?>
            </div>
        </div>
    </div> <!-- end services-list -->

</section> <!-- end s-services -->

<?php include 'login_model.php'?>
<?php include 'success_msg.php'?>




<script>
    var user_id='<?php echo $user_id; ?>';

</script>