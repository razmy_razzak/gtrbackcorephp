<?php include 'new_template_parts/header.php'?>
<!--Note this has to be added here otherwise landing page will show before it loads -->
<!-- preloader
================================================== -->
<div id="preloader">
    <div id="loader">
    </div>
</div>

<?php include 'new_template_parts/header_nav.php'?>


<?php include 'new_template_parts/home.php'?>


<?php include 'new_template_parts/about_us.php'?>


<?php include 'new_template_parts/our_service.php'?>


<?php include 'new_template_parts/our_works.php'?>


<?php include 'new_template_parts/stats.php'?>


<?php include 'new_template_parts/contact_us.php'?>

<!-- Java Script
================================================== -->
<script src="dist/js_new/jquery-3.2.1.min.js"></script>
<script src="dist/js_new/plugins.js"></script>
<script src="dist/js_new/main.js"></script>
<script type="text/javascript"  src="dist/js_new/custom.js"></script>

<?php include 'new_template_parts/footer.php'?>

