      </div><!-- /.content-wrapper -->
      
      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
        <small>By</small> <a href="http://panel.seoestore.net" target="_blank"><b>SEOeStore</b></a>
        </div>
        <!-- Default to the left -->
<?php
$data = mysqli_fetch_array(mysqli_query($conn , "SELECT `version` FROM `main` WHERE `id` = '1'"));
$version = $data[0];

?>
        <span>&copy;<?php echo date("Y") ?>.</span> <b>SEO Panel <small>V <?php echo $version; ?></b></small>
      </footer>


    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->


    <!-- Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <script src="../dist/js_new/admin.js"></script>
    <!-- Select2 -->
    <script src="../plugins/select2/select2.full.min.js"></script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->
<?php
if (!empty($analytics)){
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo $analytics; ?>', 'auto');
  ga('send', 'pageview');

</script>
<?php } ?>
  </body>
</html>
