<?php 
require 'includes/header.php';
?>

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#home']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='home-logo.php']" ).parent().addClass( "active" );
});
</script>


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="ion ion-image"></i> Logo
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
     <li>Home page</li>
    <li class="active">Logo</li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">

<?php


function imagehandler($x) {
  $path_parts = pathinfo($_FILES['image'.$x]["name"]);
  $db_file = "dist/img/" . $x . "_" . time() . "." . $path_parts['extension'];
  $target_file = "../" . $db_file;
  $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
  $uploadOk = 1;

  $check = getimagesize($_FILES['image'.$x]["tmp_name"]);
  if($check == false) {
    echo "<div class=\"alert alert-danger\" role=\"alert\"><b>File</b> is not an image.</div>";
    $uploadOk = 0;
  }

  // Check file size
  if ($_FILES['image'.$x]["size"] > 1145728) {
    echo "<div class=\"alert alert-danger\" role=\"alert\"><b>Image</b> error, file is too large.</div>";
    $uploadOk = 0;
  }
  // Allow certain file formats
  if($imageFileType != "png" && $imageFileType != "PNG") {
    echo "<div class=\"alert alert-danger\" role=\"alert\"><b>Image</b> error, only PNG files are allowed.</div>";
    $uploadOk = 0;
  }
  // Check if $uploadOk is set to 0 by an error
  if ($uploadOk == 0) {
      echo "<div class=\"alert alert-danger\" role=\"alert\">Sorry, <b>Logo</b>  was not uploaded.</div>";
  // if everything is ok, try to upload file
  } else {
      if (move_uploaded_file($_FILES['image'.$x]["tmp_name"], $target_file)) {
        //insert data
        $sql = mysql_query("UPDATE `logo` SET `image`='$db_file' WHERE `id`='1'");
        //test
        if(!$sql){
          die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysql_error() . "<div>");
        }
        echo "<div class=\"alert alert-success\" role=\"alert\"><b>Logo</b> successfully updated!</div>";

      } else {
        echo "<div class=\"alert alert-danger\" role=\"alert\"><b>Image</b> error, there was an error uploading your file.</div>";
      }
  }

}

if(isset($_POST["submit"])) {
  if (isset($_POST["del1"])) {
    $sql = mysql_query("UPDATE `logo` SET `image`='' WHERE `id`='1'");
    echo "<div class=\"alert alert-success\" role=\"alert\"><b> Logo </b> successfully deleted!</div>";
  } else {
    if($_POST['logo']!='none'){
      $db_path = $_POST['logo'];
      $sql = mysql_query("UPDATE `logo` SET `image`='$db_path' WHERE `id`='1'");
      if(!$sql){
        die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysql_error() . "<div>");
      }else{
        echo "<div class=\"alert alert-success\" role=\"alert\"><b>Logo</b> successfully updated!</div>";
      }
    }else{
      if(file_exists($_FILES['image1']['tmp_name'])) {
        imagehandler('1');
      }
    }
  }
}

// Get data
$data = mysql_query("SELECT * FROM `logo` WHERE `id`='1'");
$row = mysql_fetch_array($data);
$link1 = $row['image'];


?>

  <div class="box box-primary">
    <div class="box-body">

      <form method="post" action="home-logo.php" enctype="multipart/form-data">
        <div class="form-group">
          <h3><li class="ion ion-image"></li> Logo Image</b></h3>
        </div>

        <div class="form-group">
          <label>Select pre-deisigned logo:</label>
          <br>
        <?php
        $Lquery = mysql_query("SELECT * FROM `logos`");
        while ($Ldata = mysql_fetch_array($Lquery)){
          $path = $Ldata['path'];
          ?>
          <div class="radio">
            <label>
              <input value="<?php echo $path;?>" type="radio" name="logo">
              <img height="30px" src="../<?php echo $path;?>">
            </label>
          </div>
        <?php } ?>
          <div class="radio">
            <label>
              <input value="none" type="radio" name="logo" checked>
              Custom (upload your logo!)
            </label>
          </div>
        </div>

        <?php if(!empty($link1)) { ?>

        <div class="form-group">
          <label>Current logo:</label>
          <img class="img-responsive" src="../<?php echo $link1 ?>" width="200px">
        </div>
        <div class="checkbox">
          <label>
            <input name="del1" type="checkbox" /> <b class="text-red">Delete Logo</b>
          </label>
        </div>
        <?php } ?>
        <label for="image3">Choose image</label> <span><small class="text-blue">(Only PNG files & size less than 1 MB)</small></span>
        <input type="file" accept="image/png" class="form-control" name="image1" id="image1">
        <span class="label label-info">Best hight: 85 px</span><br>

        <div class="clearfix"></div>
        <input type="submit" class="btn btn-primary flat margin-top-20" name="submit" value="UPDATE">

      </form>
    </div>
  </div>
</section><!-- /.content -->



<?php 
require 'includes/footer.php';
?>