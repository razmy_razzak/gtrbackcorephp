<div class="modal fade" id="exampleModal<?php echo $camp['id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="<?php echo $camp['id']?>" class="extra_serv_form">
                    <input type="hidden" name="camp_id" class="form-control" value="<?php echo $camp['id']?>">
                    <input type="hidden" name="max_price_<?php echo $camp['id']?>" class="form-control" value="<?php echo $camp['pricerang'] ?>">
                    <input type="hidden" name="service_tot_<?php echo $camp['id']?>" class="form-control" value="<?php echo $camp['pricerang'] ?>">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Select Services</label>
                        <select id="admin_services" name="services" class="form-control select_services">
                            <option value="">Select</option>
                            <?php foreach ($allServices as $allService){ ?>
                                <option data-qty="<?php echo $allService['min_qty']?>" data-value="<?php echo $allService['panel_price']?>" value="<?php echo $allService['id']?>"><?php echo $allService['description']?> - $<?php echo $allService['panel_price']?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Qty: </label>
                        <input name="product_tot_<?php echo $camp['id']?>" id="product_tot_<?php echo $camp['id']?>" class="form-control qty" value="0" type="text">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Select Extra</label>
                        <select disabled class="form-control extra_ser" name="extra_servive" id="etra_<?php echo $camp['id']?>">
                            <option value="0">Select</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Total</label>
                        <input class="form-control" name="total_<?php echo $camp['id']?>" id="total_<?php echo $camp['id']?>"  type="text" readonly value="0">
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit"  class="btn btn-primary" >Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>