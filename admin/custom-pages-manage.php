<?php
require_once './includes/connect.php';
require_once 'includes/header.php';
?>
<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#custom_pages']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='custom-pages-manage.php']" ).parent().addClass( "active" );
});
</script>
<link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-users"></i> Custom Pages Mange
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="index.php">
                <i class="fa fa-dashboard"></i>Dashboard</a>
        </li>
        <li>Custom Pages</li>
        <li class="active">Manage Custom Pages</li>
    </ol>
</section>
<!-- /.content Header-->
<!-- Content -->
<section class="content">
<?php
if (isset($_POST['disable'])){
    $id = mysqli_real_escape_string($conn , htmlspecialchars($_POST['id']));
    $sql=mysqli_query($conn , "UPDATE `custom_pages` set `status`= 'disable' WHERE id = $id");
    if(!$sql){
          die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysqli_error($conn) . "<div>");
      }
    echo "<div class=\"alert alert-success\" role=\"alert\">Custom Page  Successfully Disabled!</div>";
}
?>
    <div class="box box-primary">
        <div class="box-body">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>TEXT Link</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $custom_pages = mysqli_query($conn , "SELECT * FROM `custom_pages`");
                    while ($data= mysqli_fetch_array($custom_pages)) {
                        $id =$data['id'];
                    ?>
                        <tr>
                            <td>
                                <?php echo $data['id']; ?>
                            </td>
                            <td>
                                <b>
                                    <?php echo $data['link_text']; ?>
                                </b>

                            </td>
                            <td>
                                <?php echo $data['title']; ?>
                            </td>
                             <td>
                                <?php echo $data['status']; ?>
                            </td>
                            <td>
                                <form method="post" class="form-inline">
                                <a href="custom-pages-edit.php?id=<?php echo $id;?>" data-toggle="tooltip" data-placement="top" title="Edit Custom Page">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>
                                    <input type="hidden" name="id" value="<?php echo $id;?>">
                                    <button style=" margin-left:12px;" name="disable" class="text-red btn-link" data-toggle="tooltip" data-placement="top" title="Disablee Custom Page" > <i class="fa fa-trash-o"></i></button>
                                </form>
                            </td>
                        </tr>
                        <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script>
    $(function () {
        $('.table').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false
        });
    });
</script>
<?php 
require 'includes/footer.php';
?>