<?php 
require 'includes/header.php';
?>
<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#users']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='user-edit.php']" ).parent().addClass( "active" );
});
</script>


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="ion ion-edit"></i> Edit User
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li>Users</li>
    <li class="active">Edit user</li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">
<?php
if(isset($_POST['submit'])) {
  // get fields
  $id = mysql_real_escape_string(htmlspecialchars($_POST['id']));
  $username = mysql_real_escape_string(htmlspecialchars($_POST['username']));
  $email = mysql_real_escape_string(htmlspecialchars($_POST['email']));
  $pwd = mysql_real_escape_string(htmlspecialchars($_POST['password']));
  $status = mysql_real_escape_string(htmlspecialchars($_POST['status']));

  //insert data
  if(empty($pwd)){
    $sql = mysql_query("UPDATE `users` SET `status`='$status',`email`='$email' WHERE `id`='$id' AND `username`='$username'");
  }else{
    $sql = mysql_query("UPDATE `users` SET `status`='$status',`email`='$email',`password`='$pwd' WHERE `id`='$id' AND `username`='$username'");
  }

  //test
  if(!$sql){
      die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysql_error() . "<div>");
  }
  echo "<div class=\"alert alert-success\" role=\"alert\">User Details Successfully Updated!</div>";
}

if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
?>
  <div class="box box-primary">
    <div class="box-body">
      <form method="get" action="">

        <div class="form-group">
          <label for="user">Choose user to edit</label>
            <select class="form-control select2" style="width: 100%;" id="user" name="id" required>
            <option value="">- Select User -</option>
            <?php $general -> usersRecords(); ?>
            </select>
        </div>
        <input type="submit" class="btn btn-primary flat" value="Edit">
      </form>
    </div>
  </div>
  <?php
} else {
  $id = mysql_real_escape_string(htmlspecialchars($_GET['id']));
  $sql = mysql_query("SELECT * FROM `users` WHERE `id`='$id'");
  if(mysql_num_rows($sql) > 0) {
    $data = mysql_fetch_array($sql);
    $id = $data['id'];
    $uname = $data['username'];
    $email = $data['email'];
    $sstatus = $data['status'];
?>

    <div class="box box-primary">
        <div class="box-body">
			<form action="" method="post">
			    <div class="box-body">
          <div class="form-group">
            <label for="id">User ID:</label>
            <input class="form-control" type= "text" id="id" name="id" value="<?php echo $id; ?>" readonly>
          </div>
          <div class="form-group">
            <label for="username">Username:</label>
            <input class="form-control" type= "text" id="username" name="username" value="<?php echo $uname; ?>" readonly>
          </div>
          <div class="form-group">
            <label for="email">Email:</label>
            <input class="form-control" type= "text" id="email" name="email" value="<?php echo $email; ?>">
          </div>
					<div class="form-group">
						<label for="password">Reset Password:</label> <span class="text-red">(Keep empty if you don't want to edit/reset the password)
						<input class="form-control" type= "text" id="password" name="password" value="">
					</div>
					
          <div class="form-group">
          <label for="status">User status:</label>
            <select class="form-control select2" style="width: 100%;" id="status" name="status" required>
<?php
  if ( $sstatus != 0) {
  echo '<option value="0" >Suspended</option>';
  echo '<option value="1" selected>Active</option>';
  }else{
  echo '<option value="0" selected >Suspended</option>';
  echo '<option value="1">Active</option>';
  }
?>
            </select>
					</div>
					<input class="btn btn-primary flat" type="submit" name="submit" value="Update" />
				</div>
			</form>
		</div>
    </div>
<?php
  } else {
    echo "<script type='text/javascript'> document.location = 'user-edit.php'; </script>";
  }
}
?>
</section><!-- /.content -->
<script>
  $(function () {
  $(".select2").select2();
});
</script>
<?php 
require 'includes/footer.php';
?>