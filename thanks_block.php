<?php
session_start();
include 'new_template_parts/header.php';
include 'new_template_parts/header_nav_two.php';
?>

<section id='services' class="s-services light-gray">

    <div class="row section-header">
        <div class="col-full">
            <h1 class="display-1">Thank you for the Order</h1>
        </div>
    </div>

</section>

<?php
include 'new_template_parts/contact_us.php';
?>

<script type="text/javascript"  src="dist/js_new/jquery-3.2.1.min.js"></script>
<script type="text/javascript"   src="dist/js_new/materialize.min.js"></script>
<script type="text/javascript"   src="dist/js_new/jquery.scrollTo.min.js"></script>
<script type="text/javascript"   src="dist/js_new/owl.carousel.min.js"></script>
<script type="text/javascript"  src="dist/js_new/plugins.js"></script>
<script type="text/javascript"  src="dist/js_new/validate.js"></script>
<script type="text/javascript"  src="dist/js_new/main.js"></script>
<script type="text/javascript"   src="dist/js_new/jquery.rateyo.min.js"></script>
<script type="text/javascript"  src="dist/js_new/custom.js"></script>
<script type="text/javascript"  src="dist/js_new/custom_two.js"></script>
<?php include 'new_template_parts/footer.php'; ?>
