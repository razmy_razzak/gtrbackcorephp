<?php 
require 'includes/header.php';
require_once 'libraries/htmlpurifier/HTMLPurifier.auto.php';
$config = HTMLPurifier_Config::createDefault();
$purifier = new HTMLPurifier($config);
?>
<link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#home']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='home-footer.php']" ).parent().addClass( "active" );
});
</script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-dot-circle-o"></i> Footer
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li class="active">Main Information</li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">

<?php
// Update
if(isset($_POST['submit'])) {
  // get fields
  $footer1_title = mysql_real_escape_string(htmlspecialchars($_POST['footer1_title']));

  $footer1_desc = $_POST['footer1_desc'];
  $footer1_desc = $purifier->purify($footer1_desc);
  $footer1_desc = mysql_real_escape_string($footer1_desc);

  $footer2_title = mysql_real_escape_string(htmlspecialchars($_POST['footer2_title']));
  $fb_link = mysql_real_escape_string(htmlspecialchars($_POST['fb_link']));
  $tw_link = mysql_real_escape_string(htmlspecialchars($_POST['tw_link']));
  $gplus_link = mysql_real_escape_string(htmlspecialchars($_POST['gplus_link']));
  $linkedin_link = mysql_real_escape_string(htmlspecialchars($_POST['linkedin_link']));
  $footer3_title = mysql_real_escape_string(htmlspecialchars($_POST['footer3_title']));
  $address = mysql_real_escape_string(htmlspecialchars($_POST['address']));
  $phone = mysql_real_escape_string(htmlspecialchars($_POST['phone']));
  $fax = mysql_real_escape_string(htmlspecialchars($_POST['fax']));
  $mobile = mysql_real_escape_string(htmlspecialchars($_POST['mobile']));
  $email = mysql_real_escape_string(htmlspecialchars($_POST['email']));
  
  //insert data
  $sql = mysql_query("UPDATE `footer` SET
                      `footer1_title`='$footer1_title',
                      `footer1_desc`='$footer1_desc',
                      `footer2_title`='$footer2_title',
                      `fb_link`='$fb_link',
                      `tw_link`='$tw_link',
                      `gplus_link`='$gplus_link',
                      `linkedin_link`='$linkedin_link',
                      `footer3_title`='$footer3_title',
                      `address`='$address',
                      `phone`='$phone',
                      `fax`='$fax',
                      `mobile`='$mobile',
                      `email`='$email'
                      WHERE `id`='1'");
  //test
  if(!$sql){
      die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysql_error() . "<div>");
  }
  echo "<div class=\"alert alert-success\" role=\"alert\">Main Information Successfully Updated!</div>";
}

// Get data
$data = mysql_query("SELECT * FROM `footer` WHERE `id`='1'");
$row = mysql_fetch_array($data);
  $footer1_title = $row['footer1_title'];
  $footer1_desc = $row['footer1_desc'];
  $footer2_title = $row['footer2_title'];
  $fb_link = $row['fb_link'];
  $tw_link = $row['tw_link'];
  $gplus_link = $row['gplus_link'];
  $linkedin_link = $row['linkedin_link'];
  $footer3_title = $row['footer3_title'];
  $address = $row['address'];
  $phone = $row['phone'];
  $fax = $row['fax'];
  $mobile = $row['mobile'];
  $email = $row['email'];
?>

  <div class="box box-primary">
    <div class="box-body">
      <form method="post" action="home-footer.php">
        <div class="form-group margin-top-40">
          <h3>
            <i class="fa fa-dot-circle-o"></i> Footer <b>#1</b>
            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="About section"><sup><i class="fa fa-info-circle text-gray"></i></sup></a>
          </h3>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label for="footer1_title">Title</label>
            <input type="text" class="form-control" id="footer1_title" name="footer1_title" value="<?php echo $footer1_title ?>" placeholder="Title...">
          </div>
          <div class="form-group">
            <label for="footer1_desc">Description</label>
            <textarea class="form-control" rows="4" id="footer1_desc" name="footer1_desc" placeholder="Description..."><?php echo $footer1_desc ?></textarea>
          </div>
        </div>
        <div class="clearfix"></div>

        <div class="form-group margin-top-40">
          <h3>
            <i class="fa fa-dot-circle-o"></i> Footer <b>#2</b>
            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Social section"><sup><i class="fa fa-info-circle text-gray"></i></sup></a>
          </h3>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label for="footer2_title">Title</label>
            <input type="text" class="form-control" id="footer2_title" name="footer2_title" value="<?php echo $footer2_title ?>" placeholder="Title...">
          </div>
          <div class="form-group">
            <label for="fb_link">Facebook link</label>
            <input type="text" class="form-control" id="fb_link" name="fb_link" value="<?php echo $fb_link ?>" placeholder="Facebook link...">
          </div>    
          <div class="form-group"> 
            <label for="tw_link">Twitter link</label>
            <input type="text" class="form-control"  id="tw_link" name="tw_link" value="<?php echo $tw_link ?>" placeholder="Twitter link...">
          </div>    
          <div class="form-group"> 
            <label for="gplus_link">Google+ link</label>
            <input type="text" class="form-control"  id="gplus_link" name="gplus_link" value="<?php echo $gplus_link ?>" placeholder="Google+ link...">
          </div> 
          <div class="form-group"> 
            <label for="linkedin_link">LinkedIn link</label>
            <input type="text" class="form-control"  id="linkedin_link" name="linkedin_link" value="<?php echo $linkedin_link ?>" placeholder="LinkedIn link...">
          </div>
        </div>
        <div class="clearfix"></div>

        <div class="form-group margin-top-40">
          <h3>
            <i class="fa fa-dot-circle-o"></i> Footer <b>#3</b>
            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Contact section"><sup><i class="fa fa-info-circle text-gray"></i></sup></a>
          </h3>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label for="footer3_title">Title</label>
            <input type="text" class="form-control" id="footer3_title" name="footer3_title" value="<?php echo $footer3_title ?>" placeholder="Title...">
          </div>
          <div class="form-group">
            <label for="address">Address</label>
            <input type="text" class="form-control" id="address" name="address" value="<?php echo $address ?>" placeholder="Address...">
          </div>

          <div class="form-group">
            <label for="phone">Phone</label>
            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $phone ?>" placeholder="Phone...">
          </div>

          <div class="form-group">
            <label for="fax">Fax</label>
            <input type="text" class="form-control" id="fax" name="fax" value="<?php echo $fax ?>" placeholder="Fax...">
          </div>

          <div class="form-group">
            <label for="mobile">Mobile</label>
            <input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo $mobile ?>" placeholder="Mobile...">
          </div>

          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" value="<?php echo $email ?>" placeholder="Email...">
          </div>
        </div>
        <div class="clearfix"></div>

        <input class="btn btn-primary flat margin-top-40" type="submit" name="submit" value="UPDATE">
      </form>
    </div>
  </div>
</section><!-- /.content -->

<script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script>
  $(function () {
    $("textarea").wysihtml5();
  });
</script>

<?php 
require 'includes/footer.php';
?>