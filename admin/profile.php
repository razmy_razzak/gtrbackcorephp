<?php 
require 'includes/header.php';
$sql = mysqli_query($conn , "SELECT * FROM `admins`");
$admin = mysqli_fetch_array($sql);
$email = $admin['email'];
$api = $admin['api_key'];
$paypal = $admin['paypal'];
?>


<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Admin Password
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<?php
if(isset($_POST['submit'])) {
    // get fields
    $password = mysqli_real_escape_string($conn , $_POST['password']);
    
    //insert data
    $sql = mysqli_query($conn , " UPDATE `admins` SET `password`='$password' WHERE id =1 ");

    //test
    if(!$sql){
        die("<div class=\"alert alert-danger\" role=\"alert\">Could not enter data: " . mysqli_error($conn) . "<div>");
    }
    echo "<div class=\"alert alert-success\" role=\"alert\">Password Successfully Updated!</div>";
}
?>


        <div class="box box-primary">
            <div class="box-body">
                <form action="profile.php" method="post">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="password" >Password:</label>
                            <input class="form-control" type="password" id="password" name="password" placeholder="Insert new password..." />
                        </div>                                  
                        
                        <input class="btn btn-primary flat" type="submit" name="submit" value="Update " />
                    </div>
                </form>
    </section><!-- /.content -->




<?php 
require 'includes/footer.php';
?>