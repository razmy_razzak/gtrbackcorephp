<?php 
require 'includes/header.php';
?>
<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#extras']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='extras-manage.php']" ).parent().addClass( "active" );
  
});
</script>
<link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-bars"></i> Manage Extras
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li>Services</li>
    <li class="active">Manage Extras</li>
  </ol>
</section><!-- /.content Header-->
<!-- Content -->
<section class="content">
<?php 
if (isset($_POST['syn'])) {
  $data = Array(
    'api_key'=>$api_key,//must included in every request
    'email'=>$email,//must included in every request
    'action'=>'extras'//action must included in every request
    );

  if (is_array($data)) {
    foreach ($data as $key => $value) {
      $_post[] = $key.'='.$value;
    }
  }

  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, join('&', $_post));
  curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
  $result = curl_exec($ch);
  curl_close($ch);
  //print_r($result);
  $pService = json_decode($result,true);
  foreach ($pService as $key => $arr) {
    $pId =$arr['id'];
    $pCode =$arr['code'];
    $pDesc =$arr['description'];
    $pPrice =$arr['price'];
    $pMulti =$arr['multiple'];
    $update = mysqli_query($conn , "INSERT INTO `extras`
              (`id`, `description`, `code`, `panel_price`, `multiple`, `status`)
              VALUES('$pId', '$pDesc', '$pCode', '$pPrice', '$pMulti', '1')
              ON DUPLICATE KEY UPDATE
              `description` = '$pDesc',
              `code` = '$pCode',
              `panel_price` = '$pPrice',
              `multiple` = '$pMulti',
              `status` = '1'
              ");
  }
  //test
  if(!$result){
      echo "<div class=\"alert alert-danger\" role=\"alert\">Error!<div>";
  }else{
    echo "<div class=\"alert alert-success\" role=\"alert\">Extras Successfully Updated!</div>";
  }
}
?>
  <div class="box box-primary">
    <div class="box-body">
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th> 
            <th>Panel Price</th> 
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
<?php
$extra = mysqli_query($conn , "SELECT * FROM `extras`");
while ($data= mysqli_fetch_array($extra)) {
  $id =$data['id'];
?>
          <tr><td>
            <?php echo $id ?>
          </td><td>
            <a href="extra-edit.php?id=<?php echo $id;?>" data-toggle="tooltip" data-placement="top" title="Edit extra"><?php echo $data['description']; ?> <i class="fa fa-pencil-square-o"></i>
          </td><td>
            <?php echo "USD ".$data['panel_price']; ?>
          </td><td>
            <?php echo $currency." ".$data['price']; ?>
          </td></tr>
          <?php } ?>
        </tbody>
      </table>
      <hr>
      <form method="post" class="text-center">
        <button class="btn btn-warning flat" name="syn">Synchronize with <b>SEOeStore</b></button>
      </form>
    </div>
  </div>
</section>
<script>
  $(function () {
    $('.table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<?php 
require 'includes/footer.php';
?>