<?php
if ($dir == 'rtl') {
?>
<style type="text/css">
.social-icons li {

  float:right !important;
}
</style>
<?php } ?>
<!--=== Footer ===-->
<div class="footer">
  <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-12">
                <div class="headline"><h3><?php echo $footer1_title ?></h3></div>   
                <p class="margin-bottom-25"><?php echo $footer1_desc ?></p>
            </div>          
            <div class="col-lg-4 col-sm-12">
                <div class="headline"><h3><?php echo $footer2_title ?></h3></div>
                <ul class="social-icons">
                    <?php
                    if (!empty($fb_link)) {
                        echo '<li><a href="'. $fb_link .'" data-original-title="Facebook" class="social_facebook"></a></li>';
                    }
                    if (!empty($tw_link)) {
                        echo '<li><a href="'. $tw_link .'" data-original-title="Twitter" class="social_twitter"></a></li>';
                    }
                    if (!empty($gplus_link)) {
                        echo '<li><a href="'. $gplus_link .'" data-original-title="Goole Plus" class="social_googleplus"></a></li>';
                    }
                    if (!empty($linkedin_link)) {
                        echo '<li><a href="'. $linkedin_link .'" data-original-title="Linkedin" class="social_linkedin"></a></li>';
                    }
                    ?>
                </ul>
                <p class="margin-bottom-25"></p>
            </div>
            <div class="col-lg-4 col-sm-12 map-img">
                <div class="headline"><h3><?php echo $footer3_title ?></h3></div>   
                <address class="margin-bottom-30">
                <?php
                if (!empty($address)) {
                    echo '<i class="fa fa-map-marker"></i>' . $address . '<br>';
                } if (!empty($phone)) {
                    echo '<i class="fa fa-phone"></i>' . $phone . '<br>';
                } if (!empty($mobile)) {
                    echo '<i class="fa fa-mobile"></i>' . $mobile . '<br>';
                } if (!empty($fax)) {
                    echo '<i class="fa fa-fax"></i>' . $fax . '<br>';
                } if (!empty($email)) {
                    echo '<i class="fa fa-envelope-o"></i> <a href="mailto:' . $email . '" class="">' . $email . '</a>';
                }?>
                </address>
            </div>
        </div><!--/row-->   
    </div><!--/container--> 
</div><!--/footer-->    
<!--=== End Footer ===-->

<!--=== Copyright ===-->
<div class="copyright">
  <div class="container">
    <div class="row margin-top-20">
      <div class="col-md-12 ltr">           
        <p class="cright">&copy; <?php echo date("Y") ?> <b><?php echo $site_name ?></b>. All Rights Reserved.</p>
        <div class="clearfix"></div>
        <!--<div id="ABM">
            <div class="text">Powered by</div>
            <a id="relative" href="http://abmegypt.net" target="_blank">
                <span>
                     <img src="dist/img/abm_gray.png" alt="ABM logo gray" border="0">
                     <img class="img_ABM" src="dist/img/abm.png" alt="ABM logo" border="0">
                     <span id="text_ABM">We Control E-Marketing World</span>
                 </span>
            </a>
        </div>-->
      </div>
    </div><!--/row-->
  </div><!--/container--> 
</div><!--/copyright--> 

<div class="modal fade" tabindex="-1" id="services" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3>Services list & prices</h3>
                <hr>
                <table class="table table-striped table-hover">
                <thead><tr>
                    <th>Code</th>
                    <th>Service description</th>
                    <th>Min. Qty.</th>
                    <th>Price per 1</th>
                </tr></thead>
                <?php
                $result= mysqli_query($conn ,"SELECT * FROM `service` WHERE `status` = 1 ORDER BY `ordering`");
                while ($row = mysqli_fetch_assoc($result)){
                    if (empty($row['price']) || $row['price'] == 0){
                      $price = ($row['panel_price']*$sPercentage)/100;
                    }else {
                      $price = $row['price']/100;
                    }
                    echo '<tr><td>'
                            . '<span class="label label-primary">'.$row['code'].'</span>'
                            . '</td><td>'
                            . $row['description']
                            . '</td><td>'
                            . $row['min_qty']
                            . '</td><td>$'
                            . $price
                            . '</td></tr>';
                }
                ?>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="extras" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3>Extras list & prices</h3>
                <hr>
                <table class="table table-striped table-hover">
                <thead><tr>
                    <th>Code</th>
                    <th>Extra description</th>
                    <th>Price per 1</th>
                </tr></thead>
                <?php
                $result= mysqli_query($conn ,"SELECT * FROM `extras` WHERE `status` = '1'");
                while ($row = mysqli_fetch_assoc($result)){
                    if (empty($row['price']) || $row['price'] == 0){
                      $price = ($row['panel_price']*$ePercentage)/100;
                    }else {
                      $price = $row['price']/100;
                    }
                    echo '<tr><td>'
                            . '<span class="label label-primary">'.$row['code'].'</span>'
                            . '</td><td>'
                            . $row['description']
                            . '</td><td>$'
                            . $price
                            . '</td></tr>';
                }
                ?>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
if (!empty($analytics)){
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo $analytics; ?>', 'auto');
  ga('send', 'pageview');

</script>
<?php } ?>
  </body>
</html> 