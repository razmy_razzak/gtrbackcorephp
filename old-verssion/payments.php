<?php 
require 'includes/header.php';
?>

<script>
$(document).ready(function(){
  $( ".sidebar-menu li a[href^='payments.php']" ).parent().addClass( "active" ); 
});
</script>
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-credit-card"></i> Payments
  </h1>
  <ol class="breadcrumb">
    <li><a href="dashboard.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li class="active">Payments</li>

  </ol>
</section><!-- /.content Header-->

<?php
$SQL = mysql_query("SELECT charges,order from(
  (SELECT SUM(`amount`) FROM `payment` WHERE `uid`='$uid' ) AS `charges`,
  (SELECT SUM(`price`) FROM `order` WHERE `uid`='$uid') AS `orders`) as a
  ");

$charges = mysql_fetch_array(mysql_query("SELECT SUM(`amount`) FROM `payment` WHERE `uid`='$uid' "))[0];
$orders = mysql_fetch_array(mysql_query("SELECT SUM(`price`) FROM `orders` WHERE `uid`='$uid' "))[0];
$balance = $charges - $orders/100;
if (!isset($charges) || $charges == '') {
  $charges = '-';
}else{
  $charges = '$' . $charges;
}
if (!isset($orders) || $orders == '') {
  $orders = '-';
}else{
  $orders = '$' . $orders/100;
}
if (!isset($balance) || $balance == '') {
  $balance = '$0';
}else{
  $balance = '$' . $balance;
}
?>
<section class="content-header">
  <div class="row">
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div href="?active=1" class="info-box no-active">
        <span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Deposits</span>
          <span class="info-box-number"><?php echo $charges; ?></span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div>


    <div class="col-md-4 col-sm-6 col-xs-12">
      <div href="?status=2" class="info-box no-active">
        <span class="info-box-icon bg-aqua"><i class="fa fa-cogs"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Orders</span>
          <span class="info-box-number"><?php echo $orders; ?></span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div>

    <div class="col-md-4 col-sm-6 col-xs-12">
      <div href="?status=3" class="info-box no-active">
        <span class="info-box-icon bg-green"><i class="fa fa-usd"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Current balance</span>
          <span class="info-box-number"><?php echo $balance ?></span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div>
  </div>
</section>

<!-- Content -->
<section class="content">
  <div class="box box-primary">
    <div class="box-body">
      <table class="table table-striped table-hover" >
        <thead>
          <tr>
            <th><?php $general ->toolTip('Date', 'GMT')?></th>
            <th>Method</th>
            <th>Transaction ID</th>
            <th>Amount</th>
          </tr>
        </thead>
        <tbody>
<?php 
$query = mysql_query("SELECT `p`.`id`, `p`.`uid`, `m`.`name` as `method`, `p`.`amount`, `p`.`date` as `date`, `u`.`username` as `username`,txn_id
  FROM `payment` as `p`
  LEFT JOIN `users` as `u` on `p`.`uid` = `u`.`id`
  LEFT JOIN `payment_methods` as `m` on `p`.`method` = `m`.`id`
  WHERE `p`.`uid` = '$uid'
  ORDER BY `p`.`id` DESC
  ");
while ($row = mysql_fetch_array($query)) {

?>
  <tr>
    <td>
      <?php
          if ($row['date'] == '0000-00-00 00:00:00') {
            $general ->toolTipClass('N/A', 'Date is note defined!', 'label label-gray');
          }else {
            $recordDate = strtotime($row['date']);
            if (gmdate("Ymd") == date("Ymd", $recordDate)){
                $time =  date("h:i A", $recordDate);
                $general ->toolTipClass($time , "Today on " . $time, 'label label-gray');
            }
            else {
                $date = date("Y M", $recordDate) . " " . intval(date("d", $recordDate));
                $fullDate = date("Y/m/d h:i A", $recordDate);
                $general ->toolTipClass($date, $fullDate, 'label label-gray');
            }
          }
      ?>
    </td>
    <td><?php echo $row['method'];?></td>
    <td><?php echo $row['txn_id'];?></td>
    <td><?php echo $row['amount'];?></td>

<?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</section>
<script>
  $(function () {
    $('.table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<?php 
require 'includes/footer.php';
?>