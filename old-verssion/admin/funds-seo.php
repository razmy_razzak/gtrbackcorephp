<?php 
require 'includes/header.php';
?>

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#seo']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='add_funds.php']" ).parent().addClass( "active" );
  
});
</script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-info-circle"></i> Add funds
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li class="active">Add funds</li>

  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">
  <div class="box box-primary">
    <div class="box-body">

      <form method="post" action="paypal.php">
        <div class="form-group">
          <label for="title">Amount in USD </label>
          <input type="text" class="form-control" id="title" name="title" value="" placeholder=" ">
        </div>

      
            
        <input type="submit" class="btn btn-primary flat margin-top-20" name="add-funds" value="Add Funds">
      

        </form>
    </div>
  </div>
</section><!-- /.content -->




<?php 
require 'includes/footer.php';
?>