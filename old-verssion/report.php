<?php
require 'includes/header.php';
?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='reports.php']" ).parent().addClass( "active" );
  
});
</script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-bars"></i> Reports
  </h1>
  <ol class="breadcrumb">
    <li><a href="dashboard.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li class="active">Reports</li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">
  <div class="box box-primary">
    <div class="panel-body">
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Created <a data-toggle="tooltip" data-placement="top" title="GMT Date"><sup class="fa fa-info-circle text-gray"></sup></a></th>
            <th>Service</th>
            <th>Qty.</th>
            <th>Charge</th>
            <th>Details</th>
            <th>Status</th>
            <th>Report</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        $('.table').DataTable({
            "columns": [
                {"data": "id"},
                {"data": "date"},
                {"data": "ser"},
                {"data": "qty"},
                {"data": "price"},
                {"data": "details"},
                {"data": "status"},
                {"data": "report"}
            ],
            "ordering": false,
            "lengthMenu": [ 10, 25, 50],
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: 'ajax_orders.php',
                type: 'POST'
            }
        });
    });
</script>

<?php
  include 'includes/footer.php';
?>