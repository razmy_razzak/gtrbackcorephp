<?php 
require 'includes/header.php';
?>

<script>
$(document).ready(function(){
    $( ".sidebar-menu li a[href^='#home']" ).parent().addClass( "active" );
    $( ".sidebar-menu li a[href^='home-template.php']" ).parent().addClass( "active" );
});
</script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-desktop"></i> Choose template
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    <li>Home page</li>
    <li class="active">Template </li>
  </ol>
</section><!-- /.content Header-->

<!-- Content -->
<section class="content">

<?php
// Update
if(isset($_POST['submit'])) {
  // get fields
  $tpl = mysqli_real_escape_string($conn , htmlspecialchars($_POST['tpl']));
  $dir = mysqli_real_escape_string($conn , htmlspecialchars($_POST['dir']));
  $query = mysqli_query($conn , "SELECT * FROM `tpl` WHERE `id`='$tpl'");
  if (mysqli_num_rows($query)>0){
    $query = mysqli_query($conn , "SELECT * FROM `tpl`");
    while($row = mysqli_fetch_array($query)){
      $sql = mysqli_query($conn , "UPDATE `tpl` SET `active`='0'");
    }
    $sql = mysqli_query($conn , "UPDATE `tpl` SET `active`='1' where `id`='$tpl'");
    echo "<div class=\"alert alert-success\" role=\"alert\">Template Successfully Updated!</div>";
  }
  // get fields
  $sql = mysqli_query($conn , "UPDATE `main` SET `dir`='$dir' where `id`='1'");
    echo "<div class=\"alert alert-success\" role=\"alert\">Direction Successfully Updated!</div>";
}

?>

  <div class="box box-primary">
    <div class="box-body">
      <form method="post" action="">

        <div class="form-group">
            <label for="title">Direction of website</label>
<?php
// Get data
$data = mysqli_query($conn , "SELECT dir FROM `main`");
$row = mysqli_fetch_array($data);
?>
  <div class="radio">
    <label><input value="rtl" type="radio" name="dir" <?php if($row['dir']=='rtl'){ echo 'checked'; } ?> > Right To Left </label><br>
    <label><input value="ltr" type="radio" name="dir" <?php if($row['dir']=='ltr'){ echo 'checked'; } ?> > Left To Right</label>
  </div>
</div>

        <div class="form-group">
          <label for="title">Select template</label>
<?php
// Get data
$data = mysqli_query($conn , "SELECT * FROM `tpl`");
while($row = mysqli_fetch_array($data)){
  //echo $row['id'];
?>
<div class="radio">
  <label><input value="<?php echo $row['id'] ?>" type="radio" name="tpl"
  <?php 
  if($row['active']==1){
    echo 'checked';
  }
  ?>
  ><?php echo $row['tpl'] ?></label>
</div>
<?php
}
?>
        </div>
                      
        <input type="submit" class="btn btn-primary flat margin-top-20" name="submit" value="UPDATE">
      </form>
  	</div>
  </div>
</section><!-- /.content -->



<?php 
require 'includes/footer.php';
?>